export enum OrderStatuses
{
	created        = "created",
	open           = "open",         //"placed"
	partial        = "partial",
	partial_missed = "partial_missed",
	filled         = "filled",
	cancelled      = "cancelled",
	rejected       = "rejected",
	errorneous     = "errorneous",
	missed         = "missed"
}
