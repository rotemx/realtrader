export enum Markets
{
	USDT_BTC = "USDT_BTC",
	USDT_ETH = "USDT_ETH",
	USDT_LTC = "USDT_LTC",
	USDT_NEO = "USDT_NEO",
	USDT_BCC = "USDT_BCC",
	
	BTC_ETH  = "BTC_ETH",
	BTC_LTC  = "BTC_LTC",
	BTC_BCC  = "BTC_BCC",
	BTC_XRP  = "BTC_XRP",
	BTC_XVG  = "BTC_XVG",
	BTC_TRX  = "BTC_TRX",
}

export const allMarkets = () => {
	return [
		Markets.USDT_BTC,
		Markets.USDT_ETH,
		Markets.USDT_LTC,
		Markets.USDT_NEO,
		Markets.USDT_BCC,
		
		Markets.BTC_ETH,
		Markets.BTC_LTC,
		Markets.BTC_BCC,
		Markets.BTC_XRP,
		Markets.BTC_XVG,
		Markets.BTC_TRX,
	];
};
