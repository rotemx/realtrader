export enum Coins
{
	BTC  = 'BTC',
	USDT = 'USDT',
	ETH  = 'ETH',
	LTC  = 'LTC',
}
