export enum TransactionStatuses
{
	open      = "open",
	completed = "completed",
	cancelled = "cancelled"
}
