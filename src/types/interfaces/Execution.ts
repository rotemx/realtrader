export interface Execution
{
	amount: number,
	price: number
	fee?: number
	isOpen: boolean
}
