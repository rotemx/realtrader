import { Book, BookSummery } from "./Book";

export interface SingleBookCycle
{
    book: Book,
    timestamp: number,
    cycleId: number
}

export interface SingleBookCycleSummery
{
    book: BookSummery,
    timestamp: number,
    cycleId: number
}