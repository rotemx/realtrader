import {Book} from './Book';

export interface CyclePack
{
	books : Book[]
	cycleId : number
}
