import { Book, BookSummery } from "./Book";

export interface MultipleBooksCycle
{
    books: Book[],
    timestamp: number,
    cycleId: number
}

export interface MultipleBooksCycleSummery
{
    books: BookSummery[],
    timestamp: number,
    cycleId: number
}