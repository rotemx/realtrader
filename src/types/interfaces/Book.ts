export type BookSource = 'ajax' | 'socket' | 'unknown';

export interface BookEntry
{
	price: number,
	amount: number,
}

export interface Book
{
	cycleId?: number,
	market: string,
	fee: number,
	exName: string,
	buy: BookEntry[],
	sell: BookEntry[],
	timestamp: number,
	source: BookSource,
	lastUpdateId?:number
}

export interface BookSummery
{
	cycleId?: number,
	market: string,
	fee: number,
	exName: string,
	bestBuy: BookEntry,
	bestSell: BookEntry,
	timestamp: number,
	source: BookSource,
}
