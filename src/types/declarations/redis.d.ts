import {Callback, Commands, OverloadedSetCommand, RedisClient} from 'redis'
import {EventEmitter} from "events"

declare module "redis"
{
	
	export interface OverloadedSetCommand<T, U, R>
	{
		(key: string, arg1: T, arg2: T, arg3: T, arg4: T, arg5: T, arg6: T): R;
		
		(key: string, arg1: T, arg2: T, arg3: T, arg4: T, arg5: T): R;
		
		(key: string, arg1: T, arg2: T, arg3: T, arg4: T): R;
		
		(key: string, arg1: T, arg2: T, arg3: T): R;
		
		(key: string, arg1: T, arg2: T): R;
		
		(key: string, arg1: T | { [key: string]: T } | T[]): R;
		
		(key: string, ...args: Array<T | Callback<U>>): R;
	}
	
	export interface RedisClient extends Commands<boolean>, EventEmitter
	{
		setAsync(key: string, value: string): Promise<any>;
		setAsync(key: string, value: string, param1?: string, param2?: string, param3?: string, param4?: string, param5?: string): Promise<any>;
		
		getAsync(key: string): Promise<any>;
		
		incrAsync(key: string): Promise<any>;
		
		lrangeAsync(key: string, start: number, stop: number): Promise<any>
		
		zrangeAsync(key: string, start: number, stop: number): Promise<any>
		
		hsetAsync(key: string, field: string, value: string): Promise<any>;
		
		hgetallAsync(key: string,): Promise<any>;
		
		hmsetAsync(key: string, arg1: any): Promise<any>;
		
		flushallAsync(): Promise<boolean>
		
		lpushAsync(key:string, ...numbers:number[]): Promise<boolean>
		
		delAsync(...key:string[]): Promise<boolean>
		
		saddAsync(key:string, ...values:any[]): Promise<boolean>
		
		setnxAsync(key: string, value: string): Promise<any>;
		setnxAsync(key: string, value: string, param1?:string, param2?:string, param3?:string, ): Promise<any>;
	}
	
	export interface Commands<R>
	{
	
	}
	
	export interface Multi extends Commands<Multi>
	{
		execAsync(): Promise<any>;
		
	}
}

