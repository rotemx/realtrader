import {Machine} from './entites/Machine';
import {Cycle} from './entites/Cycle';
import {Markets} from './types/enums/Markets';
import {Coins} from './types/enums/Coins';
import {Order} from './entites/Order';
import {Transaction} from './entites/Transaction';
import {Account} from './entites/Account';
import {playChaChing} from './utils/playChaChing';
import {Log} from './utils/Log';
import {Observable} from 'rxjs';
import {Bittrex} from './exchanges/Bittrex/Bittrex';
import {MINUTE, SECOND} from './GLOBALS';

export class RealTrader
{
	
	static last_checkCycle_time: number;
	
	static loadAllEntites() {
		return Promise.all([
			Machine.loadAll<Machine>(),
			Transaction.loadAll<Transaction>(),
			Order.loadAll<Order>(),
			Account.loadAll<Account>(),
		]);
		
	}
	
	static startCycleMonitor() {
		setInterval(() => {
			const
				now                = Date.now(),
				time_from_last_run = now - RealTrader.last_checkCycle_time,
				time_threshold     = 30 * SECOND;
			
			if (!this.last_checkCycle_time || time_from_last_run > time_threshold)
			{
				Log(`[!!] Oh No! checkCycle hasn't run in ${time_threshold / SECOND} seconds ! *** EXITING PROCESS *** !`, `RealTrader:startCycleMonitor`);
				process.exit();
			}
			
		}, 120000);
	}
	
	constructor(private cycleStream: Observable<Cycle>, public exchangeA, public exchangeB) {}
	
	machines: Machine[]      = [];
	static last_price_ticker = {};
	
	async init() {
		
		await RealTrader.loadAllEntites();
		
		RealTrader.startCycleMonitor();
		
		this.machines = await Machine.loadAll<Machine>();
		let machine: Machine;
		
		if (!this.machines.length)
		{
			Log('Creating new machine in DB', 'RealTrader');
			
			machine = new Machine({
				AB_profit_threshold        : -2,         //Sell in BITTREX
				BA_profit_threshold        : -2,         //Buy  in BITTREX
				Money_coin                 : Coins.USDT,
				Commodity_coin             : Coins.BTC,
				exchangeA_id               : this.exchangeA.id,
				exchangeB_id               : this.exchangeB.id,
				init_Funds                 : {          //init funds are loaded from the exchange now
					accountA: {
						init_Commodity: 0,
						init_Money    : 0,
					},
					accountB: {
						init_Commodity: 0,
						init_Money    : 0,
					},
				},
				local_exchange_id          : Bittrex.id,
				market                     : Markets.USDT_BTC,
				dynamic_balancing_threshold: -99,
				dynamic_balancing_value    : 0,
			});
			
		}
		else
		{
			Log('Loaded OLD machine from DB', 'RealTrader');
			machine = this.machines[0];
		}
		
		await machine.loadActualBalances();
		
		playChaChing();
		machine
			.run(this.cycleStream)
			.catch(err => Log(err, 'machine:run'));
		
		/*
				const
					order = new Order({
						exchange_id   : Binance.id,
						machine_id    : machine.id,
						account_id    : machine.accountA_id,
						amount        : 0.002,
						type          : OrderTypes.sell,
						market        : Markets.USDT_BTC,
						Commodity_coin: Coins.BTC,
						Money_coin    : Coin				price         : 20000
					});
				await order.execute();

		*/
		
		//		 Log('[V] Done.')
		
	}
}



