import {Book} from '../types/interfaces/Book';

export const assertBookData = (book: Book) => {
	let pass = !!(
		!!book &&
		typeof book === 'object' &&
		book.buy &&
		book.sell &&
		Array.isArray(book.buy) &&
		Array.isArray(book.sell) &&
		book.sell.length && book.buy.length &&
		book.sell[0].amount &&
		book.sell[0].price &&
		book.buy[0].amount &&
		book.buy[0].price
	);
	
	if (!pass)
	{
//		console.log('filtered out ' + book && book.exName);
	}
	
	return pass;
};
