import * as short from 'short-uuid';

export function getShortUUID(): string {
	return short().fromUUID(short.uuid());
}
