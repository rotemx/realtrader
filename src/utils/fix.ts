const ten_million = 100000000;

export const fix2 = (num: number): number => {
	
	return (num * ten_million - ((num * ten_million) % 1000000)) / ten_million;
};

export const fix6 = (num: number): number => {
	return (num * ten_million - ((num * ten_million) % 100)) / ten_million;
};

export const fix8 = (num: number): number => {
	return (num * ten_million - ((num * ten_million) % 1)) / ten_million;
};

export const mod1000 = (num: number): number => {
	return num - num % 1000;
};
