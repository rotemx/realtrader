export const JSONify = (json, accuracy=8) => JSON.stringify(json, function (key, val) {
	return val.toFixed ? Number(val.toFixed(accuracy)) : val
}, 2)
