import { BookSummery, Book } from "../types/interfaces/Book";
import { MultipleBooksCycle, MultipleBooksCycleSummery } from "../types/interfaces/MultipleBooksCycle";
import { SingleBookCycle, SingleBookCycleSummery } from "../types/interfaces/SingleBookCycle";

export function summarizeBook(book: Book): BookSummery
{
    return {
        cycleId: book.cycleId,
        market: book.market,
        fee: book.fee,
        exName: book.exName,
        bestBuy: book.buy.length ? book.buy[0] : { amount: 0, price: 0 },
        bestSell: book.sell.length ? book.sell[0] : { amount: 0, price: 0 },
        timestamp: book.timestamp,
        source: book.source,
    }
}

export function summarizeMultipleBooksCycle(cycle: MultipleBooksCycle): MultipleBooksCycleSummery
{
    return {
        cycleId: cycle.cycleId,
        timestamp: cycle.timestamp,
        books: cycle.books.map(summarizeBook),
    }
}
export function summarizeSingleBooksCycle(cycle: SingleBookCycle): SingleBookCycleSummery
{
    return {
        cycleId: cycle.cycleId,
        timestamp: cycle.timestamp,
        book: summarizeBook(cycle.book),
    }
}