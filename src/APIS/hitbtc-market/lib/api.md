# TRADING

"method": "newOrder"
"method": "cancelOrder"
"method": "cancelReplaceOrder"

"method": "getOrders"
"method": "getTradingBalance"

"method": "subscribeReports"
    "method": "activeOrders" //notification
    "method": "report" //notification

# AUTH

"method": "login"

# MARKET

"method": "getCurrency"
"method": "getSymbol"
"method": "getTrades"

"method": "subscribeTicker"
    "method": "ticker" //notification

"method": "subscribeOrderbook"
    "method": "snapshotOrderbook"
    "method": "updateOrderbook"

"method": "subscribeTrades"
    "method": "snapshotTrades"
    "method": "updateTrades"

"method": "subscribeCandles"
    "method": "snapshotCandles",
    "method": "updateCandles"