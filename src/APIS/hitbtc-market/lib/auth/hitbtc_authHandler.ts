import { hitbtc_ApiHandler } from "../common";
import {hitbtc_Credentials} from './authTypes';


export class Hitbtc_authHandler extends hitbtc_ApiHandler {

    login(credentials: hitbtc_Credentials) {

        if (credentials.key && credentials.secret) {
            return this.login_Basic(credentials.key, credentials.secret);
        } else if (credentials.key && credentials.nonce && credentials.signature) {
            return this.login_HS256(credentials.key, credentials.nonce, credentials.signature);
        } else {
            return Promise.reject('must supply either key+secret or key+nonce+signature');
        }
    }

    private login_Basic(key: string, secret: string) {

        return this.sendRequest<any>(
            "login",
            {
                algo: 'BASIC',
                pKey: key,
                sKey: secret,
            },
        );
    }

    private login_HS256(key: string, nonce: string, signature: string) {

        return this.sendRequest<any>(
            "login",
            {
                algo: "HS256",
                pKey: key,
                nonce: nonce,
                signature: signature
            },
        );
    }
}
