export type hitbtc_authType = "BASIC" | "HS256";

export interface hitbtc_Credentials {
    key?: string,
    secret?: string,
    nonce?: string,
    signature?: string,
}
