import {hitbtc_ApiHandler, hitbtc_SubscriptionUpdate} from "../common";
import { Observable} from 'rxjs';
import {hitbtc_OrderBook} from './marketTypes';

export class hitbtc_MarketHandler extends hitbtc_ApiHandler
{
	
	subscribeOrderbook(symbols: string | string[]): Observable<hitbtc_SubscriptionUpdate<hitbtc_OrderBook, hitbtc_OrderBook>> {
		
		symbols              = [...symbols];
		//convert all symbols to subscribe promises
		let subscribeRequest = Observable.fromPromise(
			Promise.all(
				symbols.map(symbol => {
					return this.sendRequest(
						"subscribeOrderbook",
						{
							symbol: symbol,
						},
					);
				}),
			),
		);
		
		let snapshotOrderbook = this._manager.updates$
		                            .filter(x => x.method === "snapshotOrderbook")
		                            .map(x => x.params as hitbtc_OrderBook)
		                            .map(x => ({snapshot: x}));
		
		let updateOrderbook = this._manager.updates$
		                          .filter(x => x.method === "updateOrderbook")
		                          .map(x => x.params as hitbtc_OrderBook)
		                          .map(x => ({update: x}));
		
		return subscribeRequest.mergeMap(x =>
			Observable.merge(snapshotOrderbook, updateOrderbook),
		);
	}
}
