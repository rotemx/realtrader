export interface hitbtc_OrderBookRow {
    price: string,                   //"0.054588",
    size: string,                    //"0.245"
}

export interface hitbtc_OrderBook {
    ask: hitbtc_OrderBookRow[],
    bid: hitbtc_OrderBookRow[],
    symbol: string,                  // "ETHBTC",
    sequence: number,                // 8073827
}
