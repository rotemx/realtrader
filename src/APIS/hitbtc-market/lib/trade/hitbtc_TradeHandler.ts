import { Observable } from 'rxjs';

import {hitbtc_TradeSide, hitbtc_TradeOrder, hitbtc_TradeBalance} from './tradeTypes';
import {hitbtc_ApiHandler, hitbtc_SubscriptionUpdate} from '../common';

//-------------------------------------------------------

export class hitbtc_TradeHandler extends hitbtc_ApiHandler
{
	
	newOrder(symbol: string, side: hitbtc_TradeSide, price: number, amount: number, clientOrderId: string) {
		
		//const clientOrderId = uuid(); //UUID for order
		
		return this.sendRequest<hitbtc_TradeOrder>(
			"newOrder",
			{
				clientOrderId: clientOrderId,
				symbol       : symbol,
				side         : side,
				price        : price,
				quantity     : amount,
			},
		);
	}
	
	cancelOrder(clientOrderId: string) {
		return this.sendRequest<hitbtc_TradeOrder>(
			"cancelOrder",
			{
				clientOrderId: clientOrderId,
			},
		);
	}
	
	cancelReplaceOrder(oldClientOrderId: string, newPrice: number, newAmount: number, newClientOrderId: string) {
		
		return this.sendRequest<hitbtc_TradeOrder>(
			"cancelReplaceOrder",
			{
				clientOrderId  : oldClientOrderId,
				requestClientId: newClientOrderId,
				quantity       : newAmount,
				price          : newPrice,
			},
		);
	}
	
	getOrders() {
		return this.sendRequest<hitbtc_TradeOrder[]>(
			"getOrders",
			{},
		);
	}
	
	getTradingBalance(): Promise<hitbtc_TradeBalance[]> {
		return this.sendRequest<hitbtc_TradeBalance[]>(
			"getTradingBalance",
			{},
		);
	}
	
	subscribeReports(): Observable<hitbtc_SubscriptionUpdate<hitbtc_TradeOrder[], hitbtc_TradeOrder>> {
		
		let subscribeRequest = Observable.fromPromise(
			this.sendRequest(
				"subscribeReports",
				{},
			),
		);
		
		let activeOrders = this._manager.updates$
		                       .filter(x => x.method === "activeOrders")
		                       .map(x => x.params as hitbtc_TradeOrder[])
		                       .map(x => ({snapshot: x}));
		//.flatMap(x => x);
		
		let reports = this._manager.updates$
		                  .filter(x => x.method === "report")
		                  .map(x => x.params as hitbtc_TradeOrder)
		                  .map(x => ({update: x}));
		
		return subscribeRequest.mergeMap(x =>
			Observable.merge(activeOrders, reports),
		);
	}
}
