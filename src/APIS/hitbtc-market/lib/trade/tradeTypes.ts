export type hitbtc_TradeSide = 'buy' | 'sell';
	export type hitbtc_TradeStatus = "new" | "suspended" | "partiallyFilled" | "filled" | "canceled" | "expired";
export type hitbtc_TradeType = "limit" | "market" | "stopLimit" | "stopMarket";
export type hitbtc_TradeTypeTimeInForce =
    "GTC" | // - Good till cancel. GTC order won't close until it is filled. 
    "IOC" | // - An immediate or cancel order is an order to buy or sell that must be executed immediately, and any portion of the order that cannot be immediately filled is cancelled. 
    "FOK" | // - Fill or kill is a type of time-in-force designation used in securities trading that instructs a brokerage to execute a transaction immediately and completely or not at all. 
    "Day" | // - keeps the order active until the end of the trading day in UTC. 
    "GTD";  // - Good till date specified in expireTime.
export type hitbtc_TradeReportType = "status" | "new" | "canceled" | "expired" | "suspended" | "trade" | "replaced";

export interface hitbtc_TradeOrder {
    id: string,                                  // "4346371528",
    clientOrderId: string,                       // "9cbe79cb6f864b71a811402a48d4b5b2",
    symbol: string,                              // "ETHBTC",
    side: hitbtc_TradeSide,                             // "sell",
    status: hitbtc_TradeStatus,                         // "new",
    type: hitbtc_TradeType,                             // "limit",
    timeInForce: hitbtc_TradeTypeTimeInForce,           // "GTC",
    quantity: string,                            // "0.002",
    price: string,                               // "0.083837",
    cumQuantity: string,                         // "0.000",
    createdAt: string,                           // "2017-10-20T12:47:07.942Z",
    updatedAt: string,                           // "2017-10-20T12:50:34.488Z",
    reportType: hitbtc_TradeReportType,                 // "replaced",

    originalRequestClientOrderId?: string,        // "9cbe79cb6f864b71a811402a48d4b5b1"

    tradeQuantity?: string,                      //"0.001",
    tradePrice?: string,                         //"0.053868",
    tradeId?: number,                            //55051694,
    tradeFee?: string,                           //"-0.000000005"
}

export interface hitbtc_TradeBalance {
    currency: string;                             // "BCN",
    available: string;                            // "100.000000000",
    reserved: string;                             // "0"
}
