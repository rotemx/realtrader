export interface hitbtc_ApiMessage {

}

export interface hitbtc_ApiRequest<T_PARAMS> extends hitbtc_ApiMessage {
    id?: number;
    method: string;
    params: T_PARAMS;
}

export interface hitbtc_ApiNotification<T_PARAMS> extends hitbtc_ApiMessage {
    //jsonrpc: string,
    method: string;
    params: T_PARAMS
}

export interface hitbtc_ApiResponse<T_RESPONSE> extends hitbtc_ApiMessage {
    //jsonrpc: string,
    id: number;
    result?: T_RESPONSE;
    error?: {
        code: number,
        message: string,
        description: string
    }
}


export interface hitbtc_SubscriptionUpdate<T_SNAPSHOT, T_UPDATE> {
    snapshot?: T_SNAPSHOT,
    update?: T_UPDATE,
}
