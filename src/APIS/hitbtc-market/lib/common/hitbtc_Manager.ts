import * as WebSocket from 'ws';
import {hitbtc_ApiRequest, hitbtc_ApiResponse, hitbtc_ApiMessage} from "./interfaces";
import {Hitbtc_authHandler, hitbtc_TradeHandler, hitbtc_MarketHandler, hitbtc_Credentials} from '../..';
import {WS_URL} from '../consts';
import {EventEmitter} from 'events';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs';

type ResponseCallback = (err: any, response: hitbtc_ApiResponse<any>) => void

const TIME_RECONNECT_INC = 200;
const TIME_RECONNECT_MAX = 3000;

interface ResponseCallbackWrapper
{
	callback?: ResponseCallback,
	method: string,
	requestId: number,
}

export interface hitbtc_ApiOptions
{
	/** auth credentials - optional */
	auth?: hitbtc_Credentials,
	/** enable verbose logging (default=false)*/
	verbose?: boolean,
	/** enable reconnect on errors and disconnects (default=true)*/
	autoReconnect?: boolean,
}

const defaultOptions: hitbtc_ApiOptions = {
	verbose: false,
	autoReconnect: true,
};

//events
export const event_open = "open";
export type event_open = "open";
export const event_close = "close";
export type event_close = "close";
export const event_error = "error";
export type event_error = "error";
export const event_login = "login";
export type event_login = "login";

export class hitbtc_Manager extends EventEmitter
{
	
	//traders:
	readonly authHandler: Hitbtc_authHandler;
	readonly tradeHandler: hitbtc_TradeHandler;
	readonly marketHandler: hitbtc_MarketHandler;
	
	readonly updates$: Observable<hitbtc_ApiRequest<any>>;
	private updatesSubject: Subject<hitbtc_ApiRequest<any>>;
	
	private _isStarted = false;
	private _connectRetries = 0;
	private _isConnected = false;
	private _options: hitbtc_ApiOptions;
	private _ws: WebSocket;
	private _uuid   = 0;
	
	get nextUUID() { return ++this._uuid; }
	
	//queues:
	private _pendingSends: (() => void)[] = [];
	private _pendingRequests              = new Map<number, ResponseCallbackWrapper>();
	
	constructor(options?: hitbtc_ApiOptions) {
		super();
		
		this._options = {...defaultOptions, ...options};
		
		//prepare updates Observable
		this.updatesSubject = new Subject<hitbtc_ApiRequest<any>>();
		this.updates$       = this.updatesSubject.share();
		
		//all api handlers
		this.authHandler   = new Hitbtc_authHandler(this);
		this.tradeHandler  = new hitbtc_TradeHandler(this);
		this.marketHandler = new hitbtc_MarketHandler(this);
	}
	
	// region events
	on(event: event_open, listener: () => void): this;
	on(event: event_close, listener: (code: number, reason: string) => void): this;
	on(event: event_error, listener: (err: Error) => void): this;
	on(event: event_login, listener: () => void): this;
	//on(event: string | symbol, listener: (...args: any[]) => void): this;
	on(event: string | symbol, listener: (...args: any[]) => void): this {
		return super.on(event, listener);
	}
	
	start() {
		if (!this._isStarted)
		{
			this.startSelf();
		}
	}
	
	stop() {
		if (this._isStarted)
		{
			this.stopSelf();
		}
	}

	sendRequest<T>(method: string, params: T, callback?: ResponseCallback): number {
		const id = this.nextUUID;
		
		const request: hitbtc_ApiRequest<T> = {
			id    : id,
			method: method,
			params: params,
		};
		
		const action = () => {
			
			this.logVerbose(`sending [${id}] ${method}`);
			
			if (callback)
			{
				this._pendingRequests.set(
					id,
					{
						callback : callback,
						method   : method,
						requestId: id,
					},
				);
			}
			
			this._ws.send(JSON.stringify(request), err => {
				if (err)
				{
					this.logVerbose(`error sending [${id}] ${method}`, err);
					let pending = this._pendingRequests.get(id);
					if (pending && pending.callback)
					{
						this._pendingRequests.delete(id);
						pending.callback(err, null);
					}
				}
			});
		};
		
		if (this._isConnected)
		{
			action();
		}
		else
		{
			this._pendingSends.push(action);
		}
		
		return id;
	}

	//-------------------------------------------------------
		
	private startSelf()
	{
		this._isStarted = true;
		this._ws = new WebSocket(WS_URL);
		this._ws.on('open', () => this.onOpen());
		this._ws.on('close', (code, reason) => this.onClose(code, reason));
		this._ws.on('error', (err) => this.onError(err));
		this._ws.on('message', (data) => this.onMessage(data));
	}

	private stopSelf()
	{
		this._isStarted = false;
		this._ws.close();
	}

	//-------------------------------------------------------
	onOpen() {
		this.logVerbose(`socket opened`);
		this.emit(event_open);
		
		this._isConnected           = true;
		this._connectRetries		= 0;
		const action_executePending = () => {
			for (const exec of this._pendingSends)
			{
				exec();
			}
		};
		
		if (this._options.auth)
		{
			this.authHandler
			    .login(this._options.auth)
			    .then(x => {
				    this.logVerbose(`logged in`, x);
				    this.emit(event_login);
				    action_executePending();
			    }).catch(err =>
				console.error("HITBTC error : ", err),
			);
		}
		else
		{
			action_executePending();
		}
	}
	
	onClose(code: number, reason: string) {
		this.logVerbose(`socket closed`, code, reason);
		this.emit(event_close, code, reason);
		
		this._ws.removeAllListeners();
		this._isConnected = false;
		
		//try to reconnect
		if (this._options.autoReconnect)
		{
			let waitBase = this._connectRetries * TIME_RECONNECT_INC;
			let waitTime = Math.min(waitBase, TIME_RECONNECT_MAX);
			this.logVerbose(`will reconnect in ${(waitTime / 1000).toFixed(3)} seconds`);
			waitTime += 100 * Math.random();

			setTimeout(() =>
			{
				if (this._isStarted && !this._isConnected)
				{
					this.logVerbose(`reconnecting (try #${this._connectRetries})`);
					this.startSelf();
				}
			}, waitTime);
			
			this._connectRetries++;
		}		
	}
	
	onError(err: any) {
		let info = err.message || err.code || err.errno;
		console.warn(`socket error`,  info);
		
		if (this.listenerCount(event_error))
		{
			this.emit(event_error, err);
		}
	}
	
	onMessage(dataBuffer: WebSocket.Data) {
		let data: hitbtc_ApiMessage;
		
		try
		{
			data = JSON.parse(dataBuffer.toString());
			
			if (data['method'])
			{
				// 'method' indicates this is an update
				this.handleUpdate(data as hitbtc_ApiRequest<any>);
			}
			else if (data['id'])
			{
				// 'id' indicates this is response
				this.handleResponse(data as hitbtc_ApiResponse<any>);
			}
			else
			{
				// hmm this is new...
				this.warningVerbose('unknown message...!', data);
			}
			
		} catch (err)
		{
			console.error(err);
		}
	}
	
	//-------------------------------------------------------
	
	handleUpdate(update: hitbtc_ApiRequest<any>) {
		this.logVerbose(`update ${update.method}`);
		this.updatesSubject.next(update);
	}
	
	handleResponse(response: hitbtc_ApiResponse<any>) {
		let id        = response.id;
		const pending = this._pendingRequests.get(id);
		
		if (pending)
		{
			this._pendingRequests.delete(id);
			let err = response.error;
			if (err)
			{
				this.errorVerbose(`error response for [${id}] ${pending.method}`, err);
				pending.callback(err, response);
			}
			else
			{
				this.logVerbose(`response for [${id}] ${pending.method}`);
				pending.callback(null, response);
			}
		}
	}
	
	private logVerbose(...args: any[]) {
		if (this._options.verbose)
		{
			console.log('** HITBTC API', ...args);
		}
	}
	
	private errorVerbose(...args: any[]) {
		if (this._options.verbose)
		{
			console.error('** HITBTC API', ...args);
		}
	}
	
	private warningVerbose(...args: any[]) {
		if (this._options.verbose)
		{
			console.warn('** HITBTC API', ...args);
		}
	}
	
}
