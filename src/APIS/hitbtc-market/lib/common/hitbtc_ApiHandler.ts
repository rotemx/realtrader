import { hitbtc_ApiRequest } from "./interfaces";
import { hitbtc_Manager } from "./hitbtc_Manager";

export abstract class hitbtc_ApiHandler {

    protected readonly _manager: hitbtc_Manager;

    constructor(manager: hitbtc_Manager) {
        this._manager = manager;
    }

    protected sendRequest<RESULT_T>(method: string, params: any) {
        return new Promise((resolve, reject) => {
            this._manager.sendRequest(
                method,
                params,
                (err, response) => {
                    if (err) {
                        reject(err);
                        return;
                    }
                    resolve(response.result);
                }
            );
        }).then(result => {
            return result as RESULT_T;
        });
    }
}
