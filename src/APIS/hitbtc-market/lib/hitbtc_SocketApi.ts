import { hitbtc_Manager, hitbtc_ApiOptions, event_open, event_close, event_error, event_login } from "./common";
import { EventEmitter } from "events";

export class hitbtc_SocketApi extends EventEmitter {

    get trade() { return this._manager.tradeHandler; }
    get market() { return this._manager.marketHandler; }

    private _manager: hitbtc_Manager;

    constructor(options?: hitbtc_ApiOptions) {
        super();
        this._manager = new hitbtc_Manager(options);
    }

    // region events
    on(event: event_open, listener: () => void): this;
    on(event: event_close, listener: (code: number, reason: string) => void): this;
    on(event: event_error, listener: (err: Error) => void): this;
    on(event: event_login, listener: () => void): this;
    //on(event: string | symbol, listener: (...args: any[]) => void): this;
    on(event: any, listener: (...args: any[]) => void): this {
        this._manager.on(event, listener);
        return this;
    }


    start() {
        this._manager.start();
    }

    stop() {
        this._manager.stop();
    }
}
