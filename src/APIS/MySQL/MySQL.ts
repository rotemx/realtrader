import * as Sequelize from 'sequelize';

const sqlHostname = '127.0.0.1';

export class MYSQL
{
	
	//	static async createDB(name: string) {
	//
	//		let res = await sequelize.query(`CREATE DATABASE \`${name}\``)
	//		                         .catch(err => Log(err));
	//		console.log(res);
	//	}
	
	/*
		static async dropDB(name: string) {
			
			let res = await sequelize.query(`DROP DATABASE \`${name}\``)
					 .catch(err => logErr);
			console.log(res);
		}
	*/
	
	static sequelize: Sequelize.Sequelize;
	
	static async init(dbName) {
		this.sequelize = new Sequelize(dbName, 'rotemx', 'BTCIsTheKing!', {
			host   : sqlHostname,
			dialect: 'mysql',
			logging: false,
			pool   : {
				max    : 50,
				min    : 0,
				acquire: 30000,
				idle   : 10000,
			},
			
		});
		
		return this.sequelize
		           .authenticate()
		           .catch(err => {
			           console.error('Unable to connect to the database:', err);
		           });
	}
	
	static get Record() {
		return this.sequelize.define('record', {
				cycleId: Sequelize.DOUBLE,
				time   : Sequelize.STRING,
				
				A_buy_price : Sequelize.DOUBLE,
				A_buy_amount: Sequelize.DOUBLE,
				
				A_sell_price : Sequelize.DOUBLE,
				A_sell_amount: Sequelize.DOUBLE,
				
				B_buy_price : Sequelize.DOUBLE,
				B_buy_amount: Sequelize.DOUBLE,
				
				B_sell_price : Sequelize.DOUBLE,
				B_sell_amount: Sequelize.DOUBLE,
				
				AB_Delta: Sequelize.DOUBLE,
				BA_Delta: Sequelize.DOUBLE,
			},
			{
				indexes: [
					{
						unique: true,
						fields: ['cycleId'],
					},
				],
			});
	};
}


