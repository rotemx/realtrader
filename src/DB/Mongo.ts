import {Log} from '../utils/Log';
import {Cycle} from '../entites/Cycle';
import {Book} from '../types/interfaces/Book';
import {EXCHANGE_CONFIG} from '../exchanges/EXCHANGE_CONFIG';
import {MongoClient, Db} from 'mongodb';

const
	username       = EXCHANGE_CONFIG.Mongo.username,
	pass           = EXCHANGE_CONFIG.Mongo.password,
	hostname       = 'localhost',
	masterHostname = EXCHANGE_CONFIG.Mongo.master_server,
	url            = `mongodb://${username}:${pass}@${hostname}:27017/?authMechanism=DEFAULT`,
	masterUrl      = `mongodb://${username}:${pass}@${masterHostname}:27017/?authMechanism=DEFAULT`;

export class Mongo
{
	static client: MongoClient;
	static db: Db;
	static masterClient: MongoClient;
	
	static init(): Promise<any> {
		return MongoClient.connect(url)
		                  .then((client) => {
			                  [this.client, this.db] = [client, client.db('RealTrader')];
			                  Log(`Connected successfully to mongo DB at ${hostname}\n`, 'Mongo/init');
		                  })
		                  .catch(err => {
			                  Log(err);
			                  return Promise.reject(err);
		                  });
	}
	
	static async getMasterClient(): Promise<MongoClient> {
		return this.masterClient || await MongoClient.connect(masterUrl)
		                                             .then((client: MongoClient) => {
			                                             this.masterClient = client;
			                                             Log(`Connected successfully to MASTER mongo DB at ${masterUrl}\n`, 'Mongo/getMasterClient');
			                                             return client;
		                                             })
		                                             .catch(err => {
			                                             Log(err);
			                                             return Promise.reject(err);
		                                             });
	}
	
	static updateDocument(query, data, collection_name: string): Promise<any> {
		if (!query || !collection_name) return Promise.reject('Mongo/updateDocument: no item provided.');
		return this.db.collection(collection_name).updateOne(query, {$set: data});
	}
	
	static addDocument(item, collection_name: string): Promise<any> {
		
		if (!item) return Promise.reject('Mongo/saveItem: no item provided.');
		return this.db.collection(collection_name).insertOne(item);
	}
	
	static removeDocument(item, collection_name: string): Promise<any> {
		
		if (!item) return Promise.reject('Mongo/removeItem: no item provided.');
		return this.db.collection(collection_name).deleteOne({_id: item._id});
	}
	
	static async close() {
		if (this.client)
		{
			this.client.close();
		}
		if (this.masterClient)
		{
			this.masterClient.close();
			
		}
	}
	
	static getAllDocuments(collection: string) {
		if (!collection) return Promise.reject('Mongo/getAllDocuments: no collection name provided.');
		if (!this.db.collection(collection))
		{
			Log(`db.collection ${collection} is undefined !`, 'WTF');
			return;
		}
		Log(`getAllDocuments find`);
		return this.db.collection(collection)
		           .find({});
		
	}
	
	static async saveBook(book: Book, db_suffix: string = '') {
		if (!book || !(typeof book === 'object') || !book.cycleId)
		{
			Log('Error - wrong value provided', 'DB.saveBook');
			return;
		}
		
		let
			masterClient   = await Mongo.getMasterClient(),
			db             = masterClient.db(`${db_suffix}_${book.exName}`),
			allCollections = await db.collections(),
			collection     = await allCollections.find(c => c.collectionName === book.market);
		
		/*
				if (collection && await collection.findOne({cycleId: book.cycleId}))
				{
					return;
				}
		*/
		
		if (collection)
		{
			let IndexExists = await collection.indexExists('cycleId');
			if (!IndexExists)
			{
				//									await collection.createIndex('cycleId',{unique: true})
			}
		}
		collection = collection || db.collection(book.market);
		
		//insert
		let
			document = {
				book     : book,
				timestamp: book.timestamp,
				cycleId  : book.cycleId,
			},
			
			prom     = new Promise((resolve, reject) => {
				collection.insertOne(document, function (err, result) {
					if (err)
					{
						Log(err);
						reject(err);
					}
					Log(`Saved ${book.exName} book. cycleId ${book.cycleId} ${book.market}`, 'Mongo/savebook', 'mongo', false);
					
					resolve(true);
				});
			});
		
		prom.catch(Log);
		return prom;
	}
	
	static saveCycle(cycle: Cycle) {
		if (!cycle || !(typeof cycle === 'object') || !cycle.bookA || !cycle.bookB || !cycle.cycleId)
		{
			Log('Error - wrong value provided', 'DB.saveCycle');
		}
		
		const
			bookA = cycle.bookA,
			bookB = cycle.bookB;
		
		return new Promise((resolve, reject) => {
			
			[bookA, bookB].forEach(async book => {
				const
					db         = (await this.getMasterClient()).db(book.exName),
					collection = db.collection(book.market),
					data       = {
						book     : book,
						timestamp: book.timestamp,
						cycleId  : cycle.cycleId,
					};
				
				collection.insertOne(data, function (err, result) {
					if (err)
					{
						Log(err);
						reject(err);
					}
					Log(`Saved ${book.exName} book. cycleId ${cycle.cycleId} ${book.market}`);
					resolve(true);
				});
			});
		});
	}
}
