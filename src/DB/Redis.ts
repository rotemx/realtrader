import * as bluebird from 'bluebird';
import * as redis from 'redis';
import {RedisClient} from 'redis';
import {Log} from '../utils/Log';
import {EXCHANGE_CONFIG} from '../exchanges/EXCHANGE_CONFIG';

bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);

export class Redis
{
	private static client: RedisClient;
	
	static async init(host: string = '127.0.0.1') {
		const options = {
			password: EXCHANGE_CONFIG.Redis.password,
			host    : host || '127.0.0.1'
		};
		
		this.client = redis.createClient(options);
		this.client.on("error", function (err) {
			console.log("Error " + err);
		});
	}
	
	static deleteEntity(entity): Promise<any[]> {
		return this.client
		           .multi()
		           .del(`${entity.key}`)
		           .zrem(`all-${entity.collection_name}`, entity.id)
		           .execAsync();
	}
	
	static async saveEntity(collection_name: string, instance): Promise<any[]> {
		
		let filtered = {};
		Object.keys(instance)
		      .forEach(key => {
			      if (Array.isArray(instance[key]))
			      {
				      filtered[key] = JSON.stringify(instance[key]);
			      }
			      else if (!['object', 'function'].includes(typeof instance[key]))
			      {
				      filtered[key] = instance[key];
			      }
		      });
		
		return this.client
		           .multi()
		           .hmset(
			           instance.key,
			           filtered
		           )
		           .zadd(instance.list_key, instance.id, instance.id)
		           .execAsync();
		
	}
	
	static async loadEntities<T>(Model): Promise<T[]> {
		
		const
			ids: number[] = await this.client.zrangeAsync(Model.list_key, 0, -1),
			results       = [];
		
		for (let id of ids)
		{
			results.push(this.loadEntity(Model, id));
		}
		return Promise.all(results);
	}
	
	static async loadEntity(Model, id: number) {
		
		const
			data = await this.client.hgetallAsync(`${Model.collection_name}:${id}`);
		
		if (!data) return null;
		const
			model  = new Model(),
			parsed = {};
		
		Object.keys(data)
		      .forEach(key => {
			      try
			      {
				      parsed[key] = JSON.parse(data[key]);
			      }
			      catch (err)
			      {
				      //			      	Log(err, 'Redis parse');
				      parsed[key] = data[key];
			      }
		      });
		
		Object.assign(model, parsed);
		
		return model;
	}
	
	static set(key, value): Promise<any> {
		return this.client.setAsync(key, value).catch(err => {Log(err, 'Redis/set');});
	}
	
	static containsUndefined(data): boolean {
		let contains = false;
		Object.keys(data).forEach(key => {
			if (typeof data[key] === 'undefined')
			{
				contains = true;
			}
		});
		return true;
	}
	
	static setHashFields(key, data): Promise<any> {
		
		const filtered = {};
		
		Object.keys(data)
		      .forEach(key => filtered[key] = Array.isArray(data[key]) ? JSON.stringify(data[key]) : data[key]);
		
		//		this.containsUndefined(filtered);
		return this.client.hmsetAsync(key, filtered);
	}
	
	static async setSETValue(key, data: any[]): Promise<any> {
		if (!Array.isArray(data))
		{
			Log('ERROR - IT IS NO ARRAY !!!!', 'setSETValue');
		}
		await Redis.client.delAsync(key);
		if (data.length)
		{
			return Redis.client.saddAsync(key, [...data]);
		}
		return true;
	}
	
	static async get(key): Promise<any> {
		return this.client.getAsync(key).catch(Log);
	}
	
	static async getSet(key: string, start: number, end: number = -1): Promise<any> {
		return this.client.zrangeAsync(key, start, end).catch(Log);
	}
	
	static async incr(key): Promise<any> {
		return this.client.incrAsync(key).catch(Log);
	}
	
	static end() {
		Log(`Redis connection closed at ${new Date()}`);
		this.client.quit();
	}
	
	static async setLock(key: string) {
		
		const
			lock_key = `${key}-lock`,
			isLocked = await this.client.getAsync(lock_key);
		
		if (isLocked)
		{
			Log(`LOCKED ! machine key ${key}. skipping cycle.`);
			return false;
		}
		else
		{
			//			Log(`NOT LOCKED.... machine key ${key}`);
			
			const
				uuid = require('uuid/v4')();
			//			Log(`Locking uuid : ${uuid}`);
			const
				isOk = await this.client.setAsync(lock_key, uuid, 'nx', 'px', '3000');
			
			if (isOk)
			{
				return async () => {
					const
						lockedUuid = await this.client.getAsync(lock_key);
					
					if (lockedUuid === uuid)
					{
						Log(`Unlocked success. ${uuid}`);
						return await this.client.delAsync(lock_key);
					}
					
					return !lockedUuid;
				};
			}
		}
	}
	
	static flushAll() {
		return this.client.flushallAsync();
	}
}
