import {Entity} from './abstract/Entity';
import {Account} from './Account';
import {Cycle} from './Cycle';
import {Markets} from '../types/enums/Markets';
import {Transaction} from './Transaction';
import * as Trade from '../modules/Trade';
import {Exchange} from '../exchanges/abstract/Exchange';
import {Coins} from '../types/enums/Coins';
import {Mongo} from '../DB/Mongo';
import {Order} from './Order';
import {Log} from '../utils/Log';
import {GLOBALS} from '../GLOBALS';
import {Observable} from 'rxjs';
import {fix8} from '../utils/fix';

let
	isLocked: boolean = false;

interface IMachine
{
	market: Markets
	
	AB_profit_threshold: number
	BA_profit_threshold: number
	
	//	init_Money: number
	//	init_Commodity: number
	
	init_Funds: {
		accountA: {
			init_Money: number
			init_Commodity: number
		}
		accountB: {
			init_Money: number
			init_Commodity: number
		}
	}
	
	Money_coin: Coins
	Commodity_coin: Coins
	
	dynamic_balancing_threshold: number
	dynamic_balancing_value: number
	
	exchangeA_id: number
	exchangeB_id: number
	local_exchange_id: number
	
}

export class Machine extends Entity<Machine> implements IMachine
{
	//region model
	static Model = Machine;
	       Model = Machine;
	
	static list_key = `all-machines`;
	       list_key = `all-machines`;
	
	static collection_name = 'machines';
	       collection_name = 'machines';
	
	static instances: Machine[] = [];
	//endregion
	
	//region members
	
	readonly dynamic_balancing_threshold;
	readonly dynamic_balancing_value;
	
	readonly AB_profit_threshold: number;
	readonly BA_profit_threshold: number;
	
	readonly csv_file_name?: string;
	
	readonly TOTAL_ACTUAL_profit_$: number = 0;
	readonly started_time: number;
	
	readonly init_Money: number;
	readonly init_Commodity: number;
	
	readonly TOTAL_tx_count: number = 0;
	//endregion
	
	//region enums
	
	readonly market: Markets;
	
	readonly Money_coin: Coins;
	readonly Commodity_coin: Coins;
	
	//endregion
	
	//region relations
	
	//region exchanges
	readonly cycle_count: number = 0;
	
	readonly exchangeA_id: number;
	
	get exchangeA(): Exchange {
		return Exchange.running_instances[this.exchangeA_id].instance;
	}
	
	readonly exchangeB_id: number;
	
	get exchangeB(): Exchange {
		return Exchange.running_instances[this.exchangeB_id].instance;
	}
	
	readonly local_exchange_id: number;
	
	//endregion
	
	//region transactions
	get transactions(): Transaction[] {
		return Transaction.getAllFromCache<Transaction>()
		                  .filter(tx => tx.machine_id === this.id);
	}
	
	//endregion
	
	// region orders
	get orders(): Order[] {
		return Order.getAllFromCache<Order>()
		            .filter(order => order.machine_id === this.id);
	}
	
	//endregion
	
	//region accounts
	init_Funds: {
		accountA: {
			init_Money: number
			init_Commodity: number
		}
		accountB: {
			init_Money: number
			init_Commodity: number
		}
	};
	
	readonly accountA_id: number;
	
	get accountA(): Account {
		return Account.getById(this.accountA_id);
	}
	
	readonly accountB_id: number;
	
	get accountB(): Account {
		return Account.getById(this.accountB_id);
	}
	
	//endregion
	
	//endregion
	
	constructor(initValue?: IMachine) {
		super('machines', !initValue);
		this.Model           = Machine;
		this.Model.instances = this.Model.instances || [];
		
		if (!initValue) //Loaded from DB
		{
			return;
		}
		
		// initiated with data
		this.exchangeA_id = initValue.exchangeA_id;
		this.exchangeB_id = initValue.exchangeB_id;
		
		Object.assign(this, initValue);
		
		this.csv_file_name = `${this.AB_profit_threshold}_${this.BA_profit_threshold}_${this.market}.csv`;
		this.started_time  = Date.now();
		this.accountA_id   = new Account({exchange_id: initValue.exchangeA_id, init_Money: initValue.init_Funds.accountA.init_Money, init_Commodity: initValue.init_Funds.accountA.init_Commodity, Money_coin: initValue.Money_coin, Commodity_coin: initValue.Commodity_coin, machine_id: this.id})
			.id;
		
		this.accountB_id = new Account({exchange_id: initValue.exchangeB_id, init_Money: initValue.init_Funds.accountB.init_Money, init_Commodity: initValue.init_Funds.accountB.init_Commodity, Money_coin: initValue.Money_coin, Commodity_coin: initValue.Commodity_coin, machine_id: this.id})
			.id;
		
		this.save();
	}
	
	readonly lastTicker = {};
	
	async run(cycleStream: Observable<Cycle>) {
		
		let counter = 0;
		this.set({
			started_time: Date.now(),
		});
		
		Log(` [>>>>>>] Running machine: ${JSON.stringify(this, null, 2)}
[-] Globals: ${JSON.stringify(GLOBALS, null, 2)}
`,
		);
		cycleStream.subscribe(async cycle => {
			if (isLocked)
			{
				Log('...isLocked. returning [<]');
				return;
			}
			const time_delta = Math.abs(cycle.bookB.timestamp - cycle.bookA.timestamp);
			if (time_delta > GLOBALS.MAX_BOOK_TIME_DELTA_MS)
			{
				//						Log(`Time Delta ${time_delta} too big. returning.`, 'machine/run', 'MAIN', false);
				//					return;
			}
			
			if (counter === 0)
			{
				Log(`   === [$$$] Staring holdings:
					Holdings_$:             ${fix8(cycle.bookA.sell[0].price * this.accountA.balance_C + this.accountA.balance_$)}
					Holdings_C:             ${fix8(this.accountA.balance_$ / cycle.bookA.sell[0].price + this.accountA.balance_C)}
					`);
			}
			if (!(++counter % 2000))
			{
				
				Log(`   [$$$] Current holdings:
					Holdings_$:             ${fix8(cycle.bookA.sell[0].price * this.accountA.balance_C + this.accountA.balance_$)}
					Holdings_C:             ${fix8(this.accountA.balance_$ / cycle.bookA.sell[0].price + this.accountA.balance_C)}
					`);
				counter = 1;
			}
			
			isLocked = true;
			this.set({
				cycle_count: this.cycle_count + 1,
			});
			const transactions: Transaction[] = Trade.checkCycle(this, cycle);
			if (transactions.length)
			{
				Log(`   [$$$] Holdings before transaction:
					Holdings_$:             ${fix8(cycle.bookA.sell[0].price * this.accountA.balance_C + this.accountA.balance_$)}
					Holdings_C:             ${fix8(this.accountA.balance_$ / cycle.bookA.sell[0].price + this.accountA.balance_C)}
					`);
				
				Log(`Received ${transactions.length} txs.`, `machine/run cycleId ${cycle.cycleId}`);
				Promise.all(transactions.map(tx => tx.execute()))
				
				       .then(() => {
					       isLocked = false;
					       //					       this.loadActualBalances()
					       //					           .then(() => {
					       //					           });
				       })
				       .catch(err => {
					       isLocked = false;
					       //					       this.loadActualBalances()
					       //					           .then(() => {
					       //					           });
					       Log(err, 'machine/tx/execute');
				       });
				Mongo.saveCycle(cycle);
			}
			else
			{
				isLocked = false;
			}
			if (!(++counter % 400))
			{
				this.loadActualBalances();
			}
			
		});
		return this;
	}
	
	async loadActualBalances() {
		const catcher = (ex, err) => {
			console.log(`setActualBalance CATCHERRR ${ex}`);
			Log(err, `${ex}: machine/setActualBalance ERROR:`);
		};
		
		let local_account = this.accountA.exchange_id === this.local_exchange_id ? this.accountA : this.accountB;
		
		await local_account.setActualBalance()
		                   .catch(catcher.bind(this, local_account.exchange.exName));
		return this;
	}
}
