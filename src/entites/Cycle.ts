import {Book} from '../types/interfaces/Book';
import {Entity} from './abstract/Entity';

interface ICycle
{
	id?: number
	timestamp?: number
	bookA: Book
	bookB: Book
	bookC?: Book
	cycleId: number
	time_delta: number
	sourceA?: 'ajax' | 'socket' | 'unknown'
	sourceB?: 'ajax' | 'socket' | 'unknown'
}

export class Cycle extends Entity<Cycle> implements ICycle
{
	
	//region model
	
	static Model = Cycle;
	       Model = Cycle;
	
	static collection_name = 'cycles';
	       collection_name = 'cycles';
	
	static list_key = `all-cycles`;
	       list_key = `all-cycles`;
	
	static instances: Cycle[] = [];
	
	//endregion
	
	//region Members
	timestamp?: number;
	bookA: Book;
	bookB: Book;
	cycleId: number;
	time_delta: number;
	sourceA: 'ajax' | 'socket' | 'unknown';
	sourceB: 'ajax' | 'socket' | 'unknown';
	
	//endregion
	
	constructor(initValue?: ICycle) {
		super('cycles', !initValue);
		this.Model           = Cycle;
		this.Model.instances = this.Model.instances || [];
		
		if (!initValue) return;
		Object.assign(this, initValue);
	}
}





