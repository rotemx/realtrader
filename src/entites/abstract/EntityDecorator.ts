export function EntityDecorator(options?: { collection_name: string, Model:any }) {
	return <T extends { new (...args: any[]): {} }>(orig: T) => class extends orig
	{
		static collection_name: string = (options && options.collection_name) ? options.collection_name : orig['name'].toLowerCase()
		static ids_list_key: string    = `all-${(options && options.collection_name) ? options.collection_name : orig['name'].toLowerCase()}`
		static Model = options.Model
	}
}
