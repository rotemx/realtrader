import {Redis} from '../../DB/Redis';
import {Log} from '../../utils/Log';
import {State} from '../../modules/State';

export abstract class Entity<T>
{
	//region model
	
	Model: any;
	static Model: any;
	
	collection_name: string;
	static collection_name: string;
	
	list_key: string;
	static list_key: string;
	
	id: number;
	key: string;
	
	//endregion
	static instances: any[] = [];
	
	constructor(collection_name: string, fromDB: boolean) {
		if (!fromDB)
		{
			if (collection_name !== 'cycles')
			{
				this.id = State.nextId;
			}
			this.key      = `${collection_name}:${this.id}`;
			this.list_key = `all-${collection_name}`;
		}
	}
	
	static getById<T>(id: number): T {
		const cached = this.instances.find(inst => inst.id === id);
		if (cached)
		{
			return cached;
		}
		else
		{
			Log(`Unable to find ${this.list_key} id ${id} in the cache.`, 'getbyId');
		}
		/*
				return Redis.loadEntity(this.Model, id)
							.then(loaded => {
								if (loaded)
								{
									this.instances.push(loaded);
								}
								else
								{
									Log(`${this.collection_name} not found id ${id}`);
								}
								return loaded;
							});
		*/
	}
	
	static getAllFromCache<M extends Entity<M>>(Model?): M[] {
		if (!this.instances)
		{
			Log(`this.instances ${this.collection_name}, ${this.list_key} is undefined!`, 'WTF');
			return []
		}
		return this.instances;
	}
	
	static loadAll<M extends Entity<M>>(Model?): Promise<M[]> {
		return Redis.loadEntities<M>(Model || this.Model)
		            .then(models => {
			            this.instances = models;
			            return models;
		            });
	}
	
	protected static saveToCache<N extends Entity<N>>(model: N) {
		const
			cached = this.instances.find(i =>
				i.id === model.id
			);
		
		if (cached)
		{
			this.instances[this.instances.indexOf(cached)] = model;
		}
		else
		{
			this.instances.push(model);
		}
	}
	
	/*
		remove(): Promise<any> {
			return Redis.deleteEntity(this);
		}
	*/
	
	save(): Promise<any[]> {
		this.Model.saveToCache.call(this.Model, this);
		return Redis.saveEntity(this.collection_name, this);
	}
	
	set(data: Partial<T>): Promise<any> {
		
		Object.assign(this, data);
		
		for (let key of Object.keys(data))
		{
			if (!Array.isArray(data[key]) && typeof data[key] === 'object')
			{
				Log(`it is object, no no.`, 'entity/set');
			}
			if (Array.isArray(data[key]))
			{
				data[key] = JSON.stringify(data[key]);
			}
		}
		return Redis.setHashFields(this.key, data);
	}
}

