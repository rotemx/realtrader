import {Order} from './Order';
import {Exchange} from '../exchanges/abstract/Exchange';
import {Entity} from './abstract/Entity';
import {Coins} from '../types/enums/Coins';
import {Transaction} from './Transaction';
import {Machine} from './Machine';
import {Log} from '../utils/Log';
import {fix8} from '../utils/fix';

interface IAccount
{
	
	init_Money: number
	init_Commodity: number
	
	Money_coin: Coins
	Commodity_coin: Coins
	
	exchange_id: number
	machine_id: number
}

export class Account extends Entity<Account> implements IAccount
{
	//region model
	static Model = Account;
	       Model = Account;
	
	static collection_name = 'accounts';
	       collection_name = 'accounts';
	
	static list_key = `all-accounts`;
	       list_key = `all-accounts`;
	
	static instances: Account[] = [];
	
	//endregion
	
	//region relations
	
	//region orders
	get orders(): Order[] {
		return Order.getAllFromCache<Order>()
		            .filter(order => (order.account_id === this.id));
		
	}
	
	//endregion
	
	//region exchange
	
	readonly exchange_id: number;
	
	get exchange(): Exchange {
		return Exchange.running_instances[this.exchange_id].instance;
	}
	
	//endregion
	
	//region machine
	
	machine_id: number;
	
	get machine(): Machine {
		return Machine.getById(this.machine_id);
	}
	
	//endregion
	
	//region Transactions
	get transactions(): Transaction[] {
		return Transaction.getAllFromCache<Transaction>()
		                  .filter(tx => (tx.buy_account_id === this.id || tx.sell_account_id === this.id));
	}
	
	//endregion
	//endregion
	
	//region members
	readonly init_Money: number;
	readonly init_Commodity: number;
	
	readonly Money_coin: Coins;
	readonly Commodity_coin: Coins;
	
	readonly balance_$: number;
	
	readonly balance_C: number;
	
	readonly TOTAL_money_out: number = 0;
	readonly TOTAL_money_in: number  = 0;
	
	readonly TOTAL_commodity_in: number  = 0;
	readonly TOTAL_commodity_out: number = 0;
	
	readonly TOTAL_buy_tx_count: number  = 0;
	readonly TOTAL_sell_tx_count: number = 0;
	
	readonly min_balance_$: number;
	readonly min_balance_C: number;
	
	readonly last_tx_time: number;
	
	readonly curr_open_orders_count: number = 0;
	
	//endregion
	
	constructor(initValue?: IAccount) {
		super('accounts', !initValue);
		this.Model           = Account;
		this.Model.instances = this.Model.instances || [];
		
		if (!initValue) return;
		
		Object.assign(this, initValue);
		
		this.last_tx_time = -1;
		this.init_Money   = this.balance_$ = this.min_balance_$ = initValue.init_Money;
		this.init_Commodity = this.balance_C = this.min_balance_C = initValue.init_Commodity;
		this.save();
	}
	
	async setActualBalance() {
		const
			balance_$                  = fix8((await this.exchange.getBalances())[this.Money_coin].available),
			balance_C                  = fix8((await this.exchange.getBalances())[this.Commodity_coin].available),
			balances: Partial<Account> = {
				balance_$    : balance_$,
				min_balance_$: balance_$,
				balance_C    : balance_C,
				min_balance_C: balance_C,
			};
		
		this.set(balances);
		
		Log(`
[$$] ${this.exchange.exName} ${this.Money_coin}:     ${this.balance_$} ${this.Commodity_coin}: ${this.balance_C}`, '', 'balances');
	}
	
	getMoneyBalance(): number {
		return this.exchange.getMoneyBalance(this.balance_$);
	}
	
	getCommodityBalance(): number {
		return this.exchange.getCommodityBalance(this.balance_C);
	}
	
	getMaxSellAmount(): number {
		return this.exchange.getMaxSellAmount(this.balance_C);
	}
	
	getMaxBuyAmount(buy_price: number): number {
		return this.exchange.getMaxBuyAmount(this.balance_$, buy_price);
	}
	
}
