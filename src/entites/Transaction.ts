import {Order} from './Order';
import {Entity} from './abstract/Entity';
import {Machine} from './Machine';
import {Account} from './Account';
import {TransactionStatuses} from '../types/enums/TransactionStatuses';
import {OrderStatuses} from '../types/enums/OrderStatuses';
import {fix8} from '../utils/fix';

interface ITransaction
{
	//region members
	
	time: number
	amount: number
	
	//	BUY PRICE
	EXP_buy_price: number;
	SAFE_buy_price: number;
	
	//	SELL PRICE
	EXP_sell_price: number;
	SAFE_sell_price: number;
	
	//	COST
	EXP_cost_for_profit_calc_$: number;
	SAFE_cost_for_profit_calc_$: number;
	
	//	INCOME
	EXP_income_for_profit_calc_$: number;
	SAFE_income_for_profit_calc_$: number;
	
	//	PROFIT PER COMMODITY
	EXP_profit_per_Commodity_unit: number;
	SAFE_profit_per_Commodity_unit: number;
	
	//	PROFIT
	EXP_profit: number;
	SAFE_profit: number;
	
	//	TIME
	previous_tx_time: number
	time_delta: number
	
	//endregion
	
	//region relations
	cycle_id: number
	
	machine_id: number
	
	buy_account_id: number
	sell_account_id: number
	
	sell_order_id: number
	buy_order_id: number
	
	//endregion
}

export class Transaction extends Entity<Transaction> implements ITransaction
{
	//region model
	static Model = Transaction;
	       Model = Transaction;
	
	static collection_name = 'transactions';
	       collection_name = 'transactions';
	
	static list_key = `all-transactions`;
	
	static instances: Transaction[] = [];
	
	//endregion
	
	//region members
	
	//	BUY PRICE
	readonly EXP_buy_price: number;
	readonly SAFE_buy_price: number;
	readonly ACTUAL_buy_price: number;
	
	//	SELL PRICE
	readonly EXP_sell_price: number;
	readonly SAFE_sell_price: number;
	readonly ACTUAL_sell_price: number;
	
	//	PROFIT PER COMMODITY
	readonly EXP_profit_per_Commodity_unit: number;
	readonly SAFE_profit_per_Commodity_unit: number;
	readonly ACTUAL_profit_per_1_Commodity: number;
	
	//	PROFIT
	readonly EXP_profit: number;
	readonly SAFE_profit: number;
	readonly ACTUAL_profit_$: number;
	
	//	INCOME
	readonly EXP_income_$: number;
	readonly SAFE_income_$: number;
	readonly ACTUAL_income_$: number;
	
	//	INCOME
	readonly EXP_income_for_profit_calc_$: number;
	readonly SAFE_income_for_profit_calc_$: number;
	readonly ACTUAL_income_for_profit_calc_$: number;
	
	//	COST
	readonly EXP_cost_$: number;
	readonly SAFE_cost_$: number;
	readonly ACTUAL_cost_$: number;
	
	readonly EXP_cost_for_profit_calc_$: number;
	readonly SAFE_cost_for_profit_calc_$: number;
	readonly ACTUAL_cost_for_profit_calc_$: number;
	
	//region not so important
	readonly time: number;
	readonly time_delta: number;
	readonly previous_tx_time: number;
	//endregion
	
	readonly amount: number;
	readonly cycle_id: number;
	//endregion
	
	//region relations
	
	//region orders
	readonly buy_order_id: number;
	
	get buy_order(): Order {
		return Order.getById(this.buy_order_id);
	}
	
	readonly sell_order_id: number;
	
	get sell_order(): Order {
		return Order.getById(this.sell_order_id);
	}
	
	//endregion
	
	//region machine
	
	readonly machine_id: number;
	
	get machine(): Machine {
		return Machine.getById(this.machine_id);
	}
	
	//endregion
	
	//region accounts
	readonly buy_account_id: number;
	
	get buy_account(): Account {
		return Account.getById(this.buy_account_id);
	}
	
	readonly sell_account_id: number;
	
	get sell_account(): Account {
		return Account.getById(this.sell_account_id);
	}
	
	//endregion
	
	//endregion
	
	//region enums
	readonly status: TransactionStatuses = TransactionStatuses.open;
	
	//endregion
	
	constructor(initValue?: ITransaction) {
		super('transactions', !initValue);
		this.Model           = Transaction;
		this.Model.instances = this.Model.instances || [];
		
		if (!initValue) return;
		
		this.status = TransactionStatuses.open;
		
		Object.assign(this, initValue);
		this.save();
	}
	
	async execute() {
		
		let local_order = this.buy_order.exchange_id === this.machine.local_exchange_id ? this.buy_order : this.sell_order;
		
		return local_order.execute();
	}
	
	tryFinish() {
		const
			buy_order  = this.buy_order,
			sell_order = this.sell_order,
			machine    = this.machine;
		
		if (sell_order.status === OrderStatuses.filled
			&& buy_order.status === OrderStatuses.filled
			&& this.status !== TransactionStatuses.completed)
		{
			const actual_profit_$ = fix8(this.sell_order.ACTUAL_income_for_profit_calc_$ - this.buy_order.ACTUAL_cost_for_profit_calc_$);
			
			this.set({
				status                       : TransactionStatuses.completed,
				ACTUAL_buy_price             : this.buy_order.ACTUAL_price,
				ACTUAL_sell_price            : this.sell_order.ACTUAL_price,
				ACTUAL_income_$              : this.sell_order.ACTUAL_income_for_profit_calc_$,
				ACTUAL_cost_$                : this.buy_order.ACTUAL_cost_for_profit_calc_$,
				ACTUAL_profit_$              : actual_profit_$,
				ACTUAL_profit_per_1_Commodity: fix8(actual_profit_$ / this.amount),
			});
			
			machine.set({
				TOTAL_ACTUAL_profit_$: fix8(machine.TOTAL_ACTUAL_profit_$ + actual_profit_$),
				TOTAL_tx_count       : machine.TOTAL_tx_count + 1,
			});
			//			Log(this, `[V] Transaction complete`);
			
		}
	}
}



