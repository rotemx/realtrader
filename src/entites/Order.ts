import {Entity} from './abstract/Entity';
import {Account} from './Account';
import {OrderTypes} from '../types/enums/OrderTypes';
import {OrderStatuses} from '../types/enums/OrderStatuses';
import {Coins} from '../types/enums/Coins';
import {Markets} from '../types/enums/Markets';
import {Exchange} from '../exchanges/abstract/Exchange';
import {Transaction} from './Transaction';
import {Machine} from './Machine';
import {Log} from '../utils/Log';
import {API_CancelOrder_Reply} from '../exchanges/types/API_CancelOrder_Reply';
import {fix8} from '../utils/fix';
import {Execution} from '../types/interfaces/Execution';
import {Exchanges} from '../types/enums/Exchanges';

export interface IOrder
{
	//	GENERAL
	readonly amount: number,
	readonly type: OrderTypes,
	readonly market: Markets
	readonly Money_coin: Coins,
	readonly Commodity_coin: Coins
	
	//	PRICE
	readonly EXP_price: number,
	readonly SAFE_price: number
	
	//	FEE
	readonly EXP_fee: number,
	readonly SAFE_fee: number
	
	//	COST
	readonly EXP_cost,
	readonly SAFE_cost,
	
	readonly EXP_cost_for_balance_$: number
	readonly SAFE_cost_for_balance_$: number
	
	readonly EXP_cost_for_profit_calc_$: number
	readonly SAFE_cost_for_profit_calc_$: number
	
	//	INCOME
	readonly EXP_income: number,
	readonly SAFE_income: number;
	
	readonly EXP_income_for_balance_$: number,
	readonly SAFE_income_for_balance_$: number;
	
	readonly EXP_income_for_profit_calc_$: number
	readonly SAFE_income_for_profit_calc_$: number
	
	//	RELATIONS
	readonly orderId?: string
	readonly account_id: number,
	readonly transaction_id?: number
	readonly machine_id: number
	readonly exchange_id: number
}

export class Order extends Entity<Order> implements IOrder
{
	//region model
	static Model = Order;
	       Model = Order;
	
	static collection_name = 'orders';
	       collection_name = 'orders';
	
	static list_key = `all-orders`;
	
	static instances: Order[] = [];
	
	//endregion
	
	//region members
	
	//	GENERAL
	readonly amount: number;
	readonly orderId?: string;
	readonly clientOrderId?: string;
	
	//	PRICE
	readonly EXP_price: number;
	readonly SAFE_price: number;
	readonly ACTUAL_price: number = 0;
	
	//	COST
	readonly EXP_cost: number;                          //
	readonly SAFE_cost: number;
	readonly ACTUAL_cost: number = 0;
	
	readonly EXP_cost_for_balance_$: number;
	readonly SAFE_cost_for_balance_$: number;
	
	readonly EXP_cost_for_profit_calc_$: number;
	readonly SAFE_cost_for_profit_calc_$: number;
	readonly ACTUAL_cost_for_profit_calc_$: number = 0;
	readonly ACTUAL_cost_for_balance_$: number     = 0;
	
	//	INCOME
	readonly EXP_income: number;
	readonly SAFE_income: number;
	readonly ACTUAL_income: number = 0;
	
	readonly EXP_income_for_balance_$: number;
	readonly SAFE_income_for_balance_$: number;
	readonly ACTUAL_income_after_fee: number;
	
	readonly EXP_income_for_profit_calc_$: number    = 0;
	readonly SAFE_income_for_profit_calc_$: number   = 0;
	readonly ACTUAL_income_for_profit_calc_$: number = 0;
	
	//	FEE
	readonly EXP_fee: number;
	readonly SAFE_fee: number;
	readonly ACTUAL_fee: number = 0;
	
	//	TIME
	readonly created_time: number    = 0;
	readonly created_time_F: Date;
	readonly placed_time: number     = 0;
	readonly filled_time: number     = 0;
	readonly errorneous_time: number = 0;
	readonly cancelled_time: number  = 0;
	
	//	Executions
	readonly executions: Execution[] = [];
	readonly filled_amount: number   = 0;
	readonly remaining_amount: number;
	
	//endregion
	
	//region enums
	readonly status: OrderStatuses;
	readonly type: OrderTypes;
	readonly Money_coin: Coins;
	readonly Commodity_coin: Coins;
	readonly market: Markets;
	//endregion
	
	//region relations
	
	//region account
	
	readonly account_id: number;
	
	get account(): Account {
		return Account.getById<Account>(this.account_id);
	}
	
	//endregion
	
	//region transaction
	readonly transaction_id: number;
	
	get transaction(): Transaction {
		return Transaction.getById(this.transaction_id);
	}
	
	//endregion
	
	//region machine
	readonly machine_id: number;
	
	get machine(): Machine {
		return Machine.getById(this.machine_id);
	}
	
	//endregion
	
	//region exchange
	readonly exchange_id: number;
	
	get exchange(): Exchange {
		return Exchange.running_instances[this.exchange_id].instance;
	}
	
	//endregion
	//endregion
	
	constructor(initValue?: IOrder) {
		super('orders', !initValue);
		this.Model           = Order;
		this.Model.instances = this.Model.instances || [];
		
		if (!initValue) return;
		
		Object.assign(this, initValue);
		
		this.status           = OrderStatuses.created;
		this.created_time     = Date.now();
		this.created_time_F   = new Date(this.created_time);
		this.remaining_amount = this.amount;
		
		this.account.set({curr_open_orders_count: this.account.curr_open_orders_count + 1});
		
		this.save();
	}
	
	//region methods
	
	static getByOrderId(id: string): Order {
		return Order.getAllFromCache<Order>()
		            .find(o => o.orderId == id);
	}
	
	async execute(): Promise<Order> {
		const log_info = `${this.exchange.exName}/execute ${this.id}: ${this.type} ${this.amount} ${this.Commodity_coin} @ ${this.EXP_price} ${this.Money_coin}`;
		
		//region Logs
		
		if (this.exchange.exName === Exchanges.binance)
		{
			return this;
		}
		if (this.type === OrderTypes.buy && this.machine.local_exchange_id === this.exchange_id)
		{
			Log(`
[$] Balances before execution :
				BUY in ${this.transaction.buy_order.exchange.exName}:
					${this.transaction.buy_order.account.balance_$} ${this.account.Money_coin}
					${this.transaction.buy_order.account.balance_C} ${this.account.Commodity_coin}
					`);
		}
		
		if (this.type === OrderTypes.sell && this.machine.local_exchange_id === this.exchange_id)
		{
			Log(`
[$] Balances before execution :
				SELL in ${this.transaction.sell_order.exchange.exName}:
					${this.transaction.sell_order.account.balance_$} ${this.account.Money_coin}
					${this.transaction.sell_order.account.balance_C} ${this.account.Commodity_coin}
				`, `Order/execute id ${this.id}: ${this.type} ${this.amount} @ ${this.EXP_price}`);
		}
		//endregion
		
		if (this.type === OrderTypes.buy)
		{
			const new_balance_$ = fix8(this.account.balance_$ - this.SAFE_cost_for_balance_$);
			if (new_balance_$ < 0)
			{
				Log(`(buy in ${this.exchange.exName}) balance ${new_balance_$} is negative`, log_info);
			}
			Log(`> Decrementing ${this.exchange.exName} balance. old:${this.account.balance_$} new: ${new_balance_$}`, log_info);
			this.account.set({
				balance_$    : new_balance_$,
				min_balance_$: Math.min(this.account.min_balance_$, new_balance_$),
			});
		}
		if (this.type === OrderTypes.sell)
		{
			const new_balance_C = fix8(this.account.balance_C - this.transaction.amount);
			if (new_balance_C < 0)
			{
				Log(`(sell in ${this.exchange.exName}) balance ${new_balance_C} is negative`, `Order/execute WTF`);
			}
			
			Log(`> Decrementing  ${this.exchange.exName} balance. old:${this.account.balance_C} new: ${new_balance_C}`, log_info);
			this.account.set({
				balance_C    : new_balance_C,
				min_balance_C: Math.min(this.account.min_balance_C, new_balance_C),
			});
		}
		
		return this.exchange
		           .execOrder(this)
		           .catch(async () => {
			           if (this.type === OrderTypes.buy)
			           {
				           const new_balance = fix8(this.account.balance_$ + this.SAFE_cost_for_balance_$);
				           Log(`exec order API failed. RESTORING ${this.exchange.exName} balance. new value: ${new_balance}`, log_info);
				           this.account.set({
					           balance_$: new_balance,
				           });
			           }
			           if (this.type === OrderTypes.sell)
			           {
				           const new_balance = fix8(this.account.balance_C + this.transaction.amount);
				           Log(`exec order API failed. RESTORING ${this.exchange.exName} balance. new value: ${new_balance}`, log_info);
				           this.account.set({
					           balance_C: new_balance,
				           });
			           }
			           this.setStatus(OrderStatuses.errorneous);
			           return this;
		           });
	}
	
	aggregateExecutions(execution: Execution) {
		const log_info = `${this.exchange.exName}/aggregateExecutions ${this.id}: ${this.type} ${this.amount} ${this.Commodity_coin} @ ${this.EXP_price} ${this.Money_coin}`;
		
		if (!execution || typeof execution.price === 'undefined' || typeof execution.amount === 'undefined' || typeof execution.isOpen === 'undefined')
		{
			Log(execution, `aggregateExecutions ERROR`, log_info);
			return;
		}
		
		Log(execution, `aggregateExecutions execution. ${this.type} order id ${this.id} in ${this.exchange.exName}. current filled: ${this.filled_amount}/${this.amount}:`);
		this.executions.push(execution);
		this.save();
		
		const account = this.account;
		if (this.type === OrderTypes.buy)
		{
			const
				agg_cost_$              = this.executions.reduce((pre, curr) => pre + +curr.price * +curr.amount, 0),  //not including fees in BUY (bittrex)
				agg_amount_C            = this.executions.reduce((pre, curr) => pre + +curr.amount, 0),
				agg_fee                 = this.executions.reduce((pre, curr) => pre + +curr.fee, 0),
				actual_price_per_unit_$ = agg_amount_C ? fix8(+agg_cost_$ / +agg_amount_C) : 0,
				NEW_balance_C           = fix8(account.balance_C + execution.amount);
			
			this.set({
				ACTUAL_price                 : actual_price_per_unit_$,
				ACTUAL_cost                  : fix8(agg_cost_$),
				ACTUAL_income                : fix8(agg_amount_C),
				ACTUAL_fee                   : fix8(agg_fee),
				ACTUAL_cost_for_profit_calc_$: fix8(agg_cost_$ * (1 + this.exchange.fee)),
				ACTUAL_cost_for_balance_$    : fix8(agg_cost_$ * (1 + (this.exchange.isFeeSameCurrency ? this.exchange.fee : 0))),
				filled_amount                : fix8(agg_amount_C),
				remaining_amount             : fix8(this.amount - agg_amount_C),
			});
			
			account.set({
				balance_C         : NEW_balance_C,
				TOTAL_commodity_in: fix8(account.TOTAL_commodity_in + execution.amount),
				TOTAL_money_out   : fix8(account.TOTAL_money_out + execution.price * execution.amount),
			});
			
			if (!execution.isOpen) // money REDEMPTION if order is closed
			{
				let isFilled = Math.abs(this.amount - agg_amount_C) < 0.00000001; //equals ZERO
				//				Log(`order ${this.id} isFilled : ${isFilled}. agg_amount_C : ${agg_amount_C}. this.amount: ${this.amount}`);
				Log(`Balance money redemption from ${this.type} order id ${this.id} to ${this.exchange.exName}: ${this.account.balance_$} ${this.account.Money_coin} to ${this.account.balance_$ + this.SAFE_cost_for_balance_$ - this.ACTUAL_cost_for_balance_$}`, log_info);
				this.account.set({balance_$: fix8(this.account.balance_$ + this.SAFE_cost_for_balance_$ - this.ACTUAL_cost_for_balance_$)});
				
				switch (true)
				{
					case isFilled:
						this.setStatus(OrderStatuses.filled);
						break;
					case agg_amount_C === 0:
						this.setStatus(OrderStatuses.missed);
						break;
					case !isFilled && agg_amount_C > 0:
						this.setStatus(OrderStatuses.partial_missed);
						break;
				}
				
			}
		}
		
		if (this.type === OrderTypes.sell)
		{
			const
				agg_income_$            = this.executions.reduce((pre, curr) => pre + +curr.price * +curr.amount, 0),   //NOT including fees in SELL in bittrex
				agg_amount_C            = this.executions.reduce((pre, curr) => pre + +curr.amount, 0),
				agg_fee                 = this.executions.reduce((pre, curr) => pre + +curr.fee, 0),
				actual_price_per_unit_$ = agg_amount_C ? fix8(+agg_income_$ / +agg_amount_C) : 0,
				NEW_balance_$           = fix8(account.balance_$ + execution.amount * execution.price - (this.exchange.isFeeSameCurrency ? execution.fee : 0));
			
			this.set({
				ACTUAL_price                   : fix8(actual_price_per_unit_$),
				ACTUAL_cost                    : fix8(agg_amount_C),
				ACTUAL_income                  : fix8(agg_income_$),
				ACTUAL_fee                     : fix8(agg_fee),
				ACTUAL_income_for_profit_calc_$: fix8(agg_income_$ * (1 - this.exchange.fee)),
				filled_amount                  : fix8(agg_amount_C),
				remaining_amount               : fix8(this.amount - agg_amount_C),
			});
			
			account.set({
				balance_$          : NEW_balance_$,
				TOTAL_money_in     : fix8(account.TOTAL_money_in + execution.amount * execution.price),
				TOTAL_commodity_out: fix8(account.TOTAL_commodity_out + execution.amount),
			});
			
			if (Number.isNaN(this.ACTUAL_price))
			{
				Log(`this.ACTUAL_price is NaN!`, `order ${this.id}`);
			}
			
			if (!execution.isOpen)
			{
				Log(`Balance commodity redemption from ${this.type} order id ${this.id} to ${this.exchange.exName}: ${this.account.balance_C} ${this.account.Commodity_coin} to ${this.account.balance_C + this.amount - this.filled_amount}`, log_info);
				this.account.set({
					balance_C: fix8(this.account.balance_C + this.amount - this.filled_amount),
				});
				
				let isFilled = Math.abs(this.amount - agg_amount_C) < 0.00000001;
				//				Log(`order ${this.id} isFilled : ${isFilled}. agg_amount_C : ${agg_amount_C}. this.amount: ${this.amount}`);
				switch (true)
				{
					case isFilled:
						this.setStatus(OrderStatuses.filled);
						break;
					case agg_amount_C === 0:
						this.setStatus(OrderStatuses.missed);
						break;
					case !isFilled && agg_amount_C > 0:
						this.setStatus(OrderStatuses.partial_missed);
						break;
				}
			}
		}
		return this.account.setActualBalance();
		
	}
	
	setStatus(status: OrderStatuses) {
		Log(status, `setStatus ${this.type} ${this.exchange.exName} id ${this.id}`);
		if (this.status === status)
		{
			Log(`The status ${status} for order ${this.id} is already is is.`);
			return;
		}
		
		this.set({
			status: status,
		});
		
		switch (status)
		{
			case OrderStatuses.filled:
				
				this.set({
					filled_time: Date.now(),
				});
				
				const account = this.account;
				if (this.type === OrderTypes.buy)
				{
					account.set({
						TOTAL_buy_tx_count: account.TOTAL_buy_tx_count + 1,
					});
				}
				else    //SELL order
				{
					account.set({
						TOTAL_sell_tx_count: account.TOTAL_sell_tx_count + 1,
					});
				}
				
				this.account.set({
					curr_open_orders_count: this.account.curr_open_orders_count - 1,
				});
				break;
			case OrderStatuses.partial:
				break;
			case OrderStatuses.cancelled:
				this.set({cancelled_time: Date.now()});
				break;
			case OrderStatuses.open:
				break;
			case OrderStatuses.created:
				break;
			case OrderStatuses.errorneous:
				this.set({
					errorneous_time: Date.now(),
				});
				
				break;
		}
	}
	
	async cancel(): Promise<any> {
		const
			res: API_CancelOrder_Reply = await this.exchange.cancelOrder(this)
			                                       .catch(err => {
				                                       Log(err);
				                                       return Promise.reject(err);
			                                       });
		if (res)
		{
			this.setStatus(OrderStatuses.cancelled);
			return this;
		}
		return false;
	}
	
	//endregion
}
