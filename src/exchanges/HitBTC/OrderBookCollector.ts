import {Hitbtc} from "./Hitbtc";
import {Book, BookEntry} from "../../types/interfaces/Book";
import {Markets} from "../../types/enums/Markets";
import {hitbtc_SocketApi, hitbtc_ApiOptions, hitbtc_OrderBookRow, hitbtc_OrderBook} from "../../APIS/hitbtc-market";
import {Subscription} from "rxjs";
import {Exchanges} from "../../types/enums/Exchanges";
import {Log} from "../../utils/Log";

import { Observable } from 'rxjs';
import { Subject } from 'rxjs';

interface Sequenced_Book extends Book
{
	sequence: number;
}

export class OrderBookCollector
{
	
	api: hitbtc_SocketApi;
	subs: Subscription[] = [];
	
	marketsMap = new Map<Markets, Sequenced_Book>();
	private booksSubject: Subject<Map<Markets, Sequenced_Book>>;
	
	constructor(private exchange: Hitbtc, options?: hitbtc_ApiOptions) {
		this.api          = new hitbtc_SocketApi(options);
		this.booksSubject = new Subject<Map<Markets, Sequenced_Book>>();
	}
	
	start() {
		let marketStrs = this.exchange.activeMarkets.map(x => this.exchange.marketToString(x));
		this.subs.push(
			this.api.market.subscribeOrderbook(marketStrs).subscribe(
				data => {
					if (data.snapshot)
					{
						this.handleSnapshot(data.snapshot);
					}
					if (data.update)
					{
						this.handleUpdate(data.update);
					}
					
					this.booksSubject.next(this.marketsMap);
				},
				err => console.error(err),
			),
		);
		
		this.api.start();
	}
	
	stop() {
		this.api.stop();
		
		this.subs.forEach(s => s.unsubscribe());
		this.subs = [];
	}
	
	getBook(market: Markets) {
		return this.marketsMap.get(market);
	}
	
	getBook$(market: Markets): Observable<Book> {
		return this.booksSubject
		           .map(m => m.get(market))
		           .filter(x => !!x).distinct();
	}
	
	//-------------------------------------------------------
	// helpers
	
	private handleSnapshot(data: hitbtc_OrderBook) {
		let market               = this.exchange.stringToMarket(data.symbol);
		let asks                 = data.ask || [];
		let bids                 = data.bid || [];
		let book: Sequenced_Book = {
			sequence : data.sequence,
			buy      : asks.map(apiItemToEntry),
			sell     : bids.map(apiItemToEntry),
			exName   : Exchanges.hitbtc,
			timestamp: Date.now(),
			market   : market,
			fee      : this.exchange.fee,
			source   : 'socket',
		};
		this.marketsMap.set(market, book);
	}
	
	private handleUpdate(data: hitbtc_OrderBook) {
		
		let market_str = data.symbol;
		let market     = this.exchange.stringToMarket(market_str);
		let book       = this.marketsMap.get(market);
		
		if (!book) return;
		
		if (book.sequence > data.sequence)
		{
			Log('update #sequence in no more than what is is already?!', 'handleUpdate:WTF');
		}
		
		let newBuys: BookEntry[]  = data.ask.map(apiItemToEntry);
		let newSells: BookEntry[] = data.bid.map(apiItemToEntry);
		
		newBuys.forEach(row => {
			let idx = book.buy.findIndex(x => x.price == row.price);
			if (idx > -1)
			{
				if (row.amount == 0)
				{
					book.buy.splice(idx, 1);
				}
				else
				{
					book.buy[idx] = row;
				}
			}
			else
			{
				book.buy.push(row);
			}
		});
		
		newSells.forEach(row => {
			let idx = book.sell.findIndex(x => x.price == row.price);
			if (idx > -1)
			{
				if (row.amount == 0)
				{
					book.sell.splice(idx, 1);
				}
				else
				{
					book.sell[idx] = row;
				}
			}
			else
			{
				book.sell.push(row);
			}
		});
		
		book.buy       = book.buy.sort((a, b) => a.price > b.price ? 1 : -1);
		book.sell      = book.sell.sort((a, b) => a.price < b.price ? 1 : -1);
		book.timestamp = Date.now();
	}
}

function apiItemToEntry(item: hitbtc_OrderBookRow): BookEntry {
	return {
		price : parseFloat(item.price),
		amount: parseFloat(item.size),
	};
	
}
