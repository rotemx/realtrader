import {Exchanges} from '../../types/enums/Exchanges';
import {Exchange} from '../abstract/Exchange';
import HitBTC from 'hitbtc-api';
import {EXCHANGE_CONFIG} from '../EXCHANGE_CONFIG';
import {API_CancelOrder_Reply} from '../types/API_CancelOrder_Reply';

import {Order} from '../../entites/Order';
import {Book, BookEntry} from '../../types/interfaces/Book';
import {Markets} from '../../types/enums/Markets';
import {API_Open_Order} from '../types/API_Open_Order';
import {Log} from '../../utils/Log';
import {Redis} from '../../DB/Redis';
import {hitbtc_SocketApi} from '../../APIS/hitbtc-market';
import {API_Get_Balances_Reply} from '../types/API_Get_Balances_Reply';
import {OrderStatuses} from '../../types/enums/OrderStatuses';
import {hitbtc_TradeOrder} from '../../APIS/hitbtc-market/lib/trade';
import {OrderTypes} from '../../types/enums/OrderTypes';
import {OrderBookCollector} from './OrderBookCollector';
import {getShortUUID} from '../../utils/getShortUUID';
import {Observable} from 'rxjs';

let [key, secret] = [EXCHANGE_CONFIG.HitBTC.key, EXCHANGE_CONFIG.HitBTC.secret];

const
	INTERVAL   = 100,
	restClient = new HitBTC({
		key,
		secret,
		isDemo: false,
	});

export class Hitbtc extends Exchange
{
	socketApi: hitbtc_SocketApi;
	
	//region Model
	static key = `exchanges:4`;
	       key = `exchanges:4`;
	
	static id = 4;
	       id = 4;
	
	static fee = 0.001;
	       fee = 0.001;
	
	static Model = Hitbtc;
	       Model = Hitbtc;
	
	static collection_name = 'exchanges';
	       collection_name = 'exchanges';
	
	static list_key = `all-exchanges`;
	       list_key = `all-exchanges`;
	
	static exName = Exchanges.hitbtc;
	       exName = Exchanges.hitbtc;
	
	static instances: Hitbtc[];
	private orderBookCollector: OrderBookCollector;
	
	buy_power_ratio: number = 1 - this.fee;
	
	//endregion
	
	constructor(isNewInstance?: boolean) {
		super(Exchanges.hitbtc, isNewInstance);
		this.Model           = Hitbtc;
		this.Model.instances = this.Model.instances || [];
		
		this.socketApi = new hitbtc_SocketApi({
			//			verbose: true,
			auth: {
				key   : EXCHANGE_CONFIG.HitBTC.key,
				secret: EXCHANGE_CONFIG.HitBTC.secret,
			},
		});
		this.socketApi.start();
	}
	
	static async load(): Promise<Hitbtc> {
		
		return Redis.loadEntity(Hitbtc, this.id)
		            .then(async loadedInstance => {
			            let ex: Hitbtc;
			
			            if (loadedInstance)
			            {
				            ex = loadedInstance;
			            }
			            else
			            {
				            ex           = new Hitbtc(true); //null for newly created exchange instance
				            ex.isRunning = true;
				            ex.save();
			            }
			            Exchange.running_instances[this.id] = {exName: ex.exName, instance: ex, id: ex.id};
			            await ex.initGetBookSnapshot();
			            return ex;
		            });
		
	}
	
	getBookAjax$(market: Markets = Markets.USDT_BTC): Observable<Book> {
		let counter      = 0;
		let lastReceived = 0;
		return Observable.interval(INTERVAL)
		                 .mergeMap(() => {
			                 counter++;
			                 const counterSend = counter;
			                 return this.getBookAjax(market)
			                            .filter(res => !!res)
			                            .map(data => ({data, counter: counterSend}))
			                            .catch(error => {
				                            return Observable.of(null);
			                            });
		                 })
		                 .filter(res => !!res)
		                 .filter(res => res.counter >= lastReceived)
		                 .do(res => lastReceived = res.counter)
		                 .map(res => res.data)
		                 .shareReplay(1);
	}
	
	getBookAjax(market: Markets, cycleId?: number): Observable<Book> {
		
		const mapItem = (res: any) => {
			if (!res || !res.bids || !res.asks)
			{
				Log('No res', `HitBTC getBookAjax/mapItem/${market_str}`);
				return null;
			}
			
			const
				buyRecords: BookEntry[] = res.asks
				                             .slice(0, this.book_length)
				                             .map((obj) => ({price: obj.price, amount: obj.volume})), //HIGHER
				sellRecords             = res.bids
				                             .slice(0, this.book_length)
				                             .map((obj) => ({price: obj.price, amount: obj.volume})), //LOWER
				book: Book              = {
					market   : market,
					fee      : Hitbtc.fee,
					exName   : Hitbtc.exName,
					timestamp: cycleId || Date.now(),
					cycleId  : cycleId || Date.now(),
					buy      : buyRecords,
					sell     : sellRecords,
					source   : 'ajax',
				};
			
			if (book.buy && book.buy.length && book.sell && book.sell.length && book.buy[0].price < book.sell[0].price)
			{
				Log(`WTF ! HitBTC_buy ${book.buy[0].price} < HitBTC_sell ${book.sell[0].price}`, 'HitBTC WTF!');
				return null;
			}
			return book;
		};
		
		const market_str = this.marketToString(market);
		
		return new Observable(obs => {
			restClient.getOrderBook(market_str)
			          .then(res => {
				          const mapItemResult = mapItem(res);
				          if (!mapItemResult)
				          {
					          obs.error('no res');
					          return;
				          }
				          obs.next(mapItemResult);
				          obs.complete();
			          })
			          .catch(err => {
				          Log(err, `hitBTC/getBookAjax/${market_str}`);
				          obs.error(err);
			          });
			return () => {};
			
		});
		
	}
	
	get activeMarkets() {
		return [
			Markets.USDT_NEO,
			Markets.USDT_BTC,
			Markets.USDT_ETH,
			Markets.USDT_LTC,
			Markets.USDT_BCC,
			
			Markets.BTC_ETH,
			Markets.BTC_LTC,
			Markets.BTC_BCC,
			Markets.BTC_XRP,
			Markets.BTC_XVG,
			Markets.BTC_TRX,
		];
	}
	
	marketToString(market: Markets): string {
		return {
			[Markets.USDT_BTC]: 'BTCUSD',
			[Markets.USDT_NEO]: 'NEOUSD',
			[Markets.USDT_ETH]: 'ETHUSD',
			[Markets.USDT_LTC]: 'LTCUSD',
			[Markets.USDT_BCC]: 'BCHUSD',
			
			[Markets.BTC_ETH]: 'ETHBTC',
			[Markets.BTC_LTC]: 'LTCBTC',
			[Markets.BTC_BCC]: 'BCHBTC',
			[Markets.BTC_XRP]: 'XRPBTC',
			[Markets.BTC_XVG]: 'XVGBTC',
			[Markets.BTC_TRX]: 'TRXBTC',
		}[market];
	}
	
	stringToMarket(market_str: string): Markets {
		return {
			'BTCUSD': Markets.USDT_BTC,
			'ETHUSD': Markets.USDT_ETH,
			'LTCUSD': Markets.USDT_LTC,
			'NEOUSD': Markets.USDT_NEO,
			'BCHUSD': Markets.USDT_BCC,
			
			'ETHBTC': Markets.BTC_ETH,
			'LTCBTC': Markets.BTC_LTC,
			'BCHBTC': Markets.BTC_BCC,
			'XRPBTC': Markets.BTC_XRP,
			'XVGBTC': Markets.BTC_XVG,
			'TRXBTC': Markets.BTC_TRX,
		}[market_str];
	}
	
	initGetBookSnapshot() {
		if (!this.orderBookCollector)
		{
			this.orderBookCollector = new OrderBookCollector(this);
			this.orderBookCollector.start();
		}
		return Promise.resolve();
	}
	
	getBookSnapshot(market: Markets, cycleId: number): Promise<Book> {
		
		return this.getBookAjax(market, cycleId).toPromise();
		
		// -- removing book collector via book collector. socket gets stuck 
		// let book = this.orderBookCollector.getBook(market);
		// if (book)
		// {
		// 	book.cycleId = cycleId;
		// 	if (book.buy[0].price <= book.sell[0].price)
		// 	{
		// 		Log(`WTF ! HITBTC SOCKET : ${book.market} buy ${book.buy[0].price} < sell ${book.sell[0].price}`, 'HitBTC getbookAjax$ WTF!');
		// 		return Promise.resolve(null);
		// 	}
		// 	return Promise.resolve(book);
		// }
		// else
		// {
		// 	return Promise.resolve(null);
		// }
	}
	
	//region NOT IMPLEMENTED
	
	protected async _run(): Promise<any> {
		this.socketApi.trade.subscribeReports().subscribe(report => {
			if (report.snapshot)
			{
				//todo: handle snapshot
			}
			if (report.update)
			{
				this.onExecutionUpdate(report.update);
			}
		});
	}
	
	protected _stop(): void {
	
	}
	
	protected _getOpenOrders(market: Markets): Promise<API_Open_Order[]> {return null;}
	
	getBalances(): Promise<API_Get_Balances_Reply> {
		
		let prom = this.socketApi.trade.getTradingBalance()
		               .then(tradingBalances => {
			
			               let reply: API_Get_Balances_Reply = {};
			
			               for (let tradingBalance of tradingBalances)
			               {
				               let api_currency = tradingBalance.currency;
				               if (api_currency === 'USD')
				               {
					               api_currency = 'USDT';
				               }
				
				               reply[api_currency] = {
					               total    : parseFloat(tradingBalance.available) + parseFloat(tradingBalance.reserved),
					               available: parseFloat(tradingBalance.available),
					               reserved : parseFloat(tradingBalance.reserved),
				               };
			               }
			               return reply;
		               });
		
		prom.catch(err => {
			console.log('CATVHCHHH');
			
			Log(err, 'HitBTC/getBalance');
		});
		return prom;
	}
	
	protected _execOrder(order: Order): Promise<Order> {
		Log(`[>] Executing ${order.type} order ${order.id}: amount ${order.amount} at EXP price ${order.EXP_price} (SAFE ${order.SAFE_price})`, 'hitBTC:_execOrder');
		
		let clientOrderId = getShortUUID();
		
		order.set({clientOrderId});
		
		let prom = this.socketApi.trade.newOrder(
			this.marketToString(order.market),
			order.type === OrderTypes.buy ? 'buy' : 'sell',
			order.SAFE_price,
			order.amount,
			clientOrderId,
		).then(x => {
				Log(x, `HitBTC/_execOrder res`, 'hitbtc');
				order.set({
					orderId    : String(x.id),
					placed_time: Date.now(),
				});
				return order;
			},
		);
		
		prom.catch(err => {
			Log(err, 'HitBTC/_execOrder error');
		});
		
		return prom;
	}
	
	protected _cancelOrder(order: Order): Promise<API_CancelOrder_Reply> {
		return this.socketApi.trade.cancelOrder(order.clientOrderId).then(x => {
			return {
				timestamp: Date.now(),
			};
		});
	}
	
	getBook$(market: Markets): Observable<Book> {
		return this.orderBookCollector.getBook$(market);
	}
	
	getMoneyBalance(balance: number): number {
		return balance;
	}
	
	getCommodityBalance(balance: number): number {
		return balance;
	}
	
	getMaxSellAmount(balance: number): number {return null;}
	
	getMaxBuyAmount(balance_$: number, buy_price): number {return null;}
	
	//endregion
	
	onExecutionUpdate(data: hitbtc_TradeOrder) {
		Log(data, 'hitBTC/onExecutionUpdate', 'hitBTC_onExecutionUpdate');
		
		//const {X: order_status, p: price, q: quantity, z: filledQty, n: fee, S: side, o: orderType, i: orderId, c: clientOrderId, l: last_exec_amount, L: last_exec_price} = data;
		const {
			      id           : orderId,
			      clientOrderId: clientOrderId,
			      status       : order_status,
			      tradePrice   : last_exec_price,
			      tradeQuantity: last_exec_amount,
			      tradeFee     : fee,
		      } = data;
		
		let order = this.orders.find(o =>
			o.orderId == orderId) || this.orders.find(o => o.clientOrderId == clientOrderId,
		);
		
		if (!order)
		{
			Log(`order ${orderId}/${clientOrderId} not found !`, 'HitBTC/onExecutionUpdate');
			//			Log(this.orders, 'this.orders');
			Log(data, 'data');
			return;
		}
		
		order.aggregateExecutions({
			price : +last_exec_price,
			amount: +last_exec_amount,
			fee   : +fee,
			isOpen: ["filled", "canceled", "expired", "suspended"].includes(status), //todo: does not support IOC partial_filled
		});
		
		switch (order_status)
		{
			
			case "suspended":
				Log(data, "hitbtc:onExecutionUpdate:case:REJECTED", 'hitbtc');
				order.setStatus(OrderStatuses.rejected);
				return;
			case "canceled":
				Log(data, "hitbtc:onExecutionUpdate:case:CANCELED", 'hitbtc');
				order.setStatus(OrderStatuses.cancelled);
				return;
			
			case "new":
				Log(data, "hitbtc:onExecutionUpdate:case:NEW", 'hitbtc');
				break;
			case "expired":
				Log(data, "hitbtc:onExecutionUpdate:case:EXPIRED", 'hitbtc');
				break;
			
			case "partiallyFilled":
				Log(data, "HITBTC Order status PARTIALLY_FILLED!", 'hitbtc');
				if (order.status !== OrderStatuses.filled)
				{
					order.setStatus(OrderStatuses.partial);
				}
				break;
			
			case "filled":
				Log(data, "HITBTC Order status FILLED!", 'hitbtc');
				//				order.setStatus(OrderStatuses.filled); //should be marked on aggregateExecutions. todo:check if working
				break;
			
			default:
				Log(data, `HITBTC Order status ${order.id} in ${this.exName} WTF?!! NO ORDER STATUS ?!?! WTF!!`, 'hitbtc');
				break;
		}
		
		order.transaction.tryFinish();
	}
}
