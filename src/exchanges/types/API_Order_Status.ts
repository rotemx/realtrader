export interface API_Order_Status
{
	remaining_amount: number
	filled_amount: number
	isOpen: boolean
	actual_price: number
	total_cost: number
	fee_paid: number
}


