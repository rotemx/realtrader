export interface API_Get_Balance_Reply
{
	total: number
	available: number
	reserved: number
}

export interface API_Get_Balances_Reply
{
	[coin: string]: API_Get_Balance_Reply
}
