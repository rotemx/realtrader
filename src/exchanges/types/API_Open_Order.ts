export interface API_Open_Order
{
	isClosed: boolean
	total_cost: number
	price_per_unit: number
	total_amount: number
	remaining_amount: number
	filled_amount: number
	orderId: string,
	fee_paid : number
}


