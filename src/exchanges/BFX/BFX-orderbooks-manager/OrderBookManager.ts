import {BFX} from '../BFX';
import {Markets} from '../../../types/enums/Markets';
import {Log} from '../../../utils/Log';

const
	WS     = require('ws'),
	_      = require('lodash'),
	async  = require('async'),
	fs     = require('fs'),
	moment = require('moment'),
	CRC    = require('crc-32'),
	conf   = {
		wshost: 'wss://api.bitfinex.com/ws/2',
	};

export class BFXOrderBookManager
{
	static BOOK: { bids: any, asks: any, psnap: any, mcnt: number } = {asks: {}, bids: {}, psnap: {}, mcnt: 0};
	static connected                                                = false;
	static connecting                                               = false;
	static cli;
	static seq                                                      = null;
	static market: Markets;
	
	static onClose() {
		this.seq        = null;
		this.connecting = false;
		this.connected  = false;
		Log('BFX WS closed');
	}
	
	static onOpen() {
		this.connecting = false;
		this.connected  = true;
		this.cli.send(JSON.stringify({event: 'conf', flags: 65536 + 131072}));
		this.cli.send(JSON.stringify({event: 'subscribe', channel: 'book', pair: BFX.marketToString(this.market), prec: 'P0', len: 25}));
		Log('BFX WS open');
	}
	
	static onMessage(msg) {
		msg = JSON.parse(msg);
		
		if (msg.event) return;
		if (msg[1] === 'hb')
		{
			this.seq = +msg[2];
			return;
		}
		else if (msg[1] === 'cs')
		{
			this.seq = +msg[3];
			
			const
				checksum  = msg[2],
				csdata    = [],
				bids_keys = this.BOOK.psnap['bids'],
				asks_keys = this.BOOK.psnap['asks'];
			
			for (let i = 0; i < 25; i++)
			{
				if (bids_keys[i])
				{
					const
						price = bids_keys[i],
						pp    = this.BOOK.bids[price];
					csdata.push(pp.price, pp.amount);
				}
				if (asks_keys[i])
				{
					const
						price = asks_keys[i],
						pp    = this.BOOK.asks[price];
					csdata.push(pp.price, -pp.amount);
				}
			}
			
			const
				cs_str  = csdata.join(':'),
				cs_calc = CRC.str(cs_str);
			
			//			fs.appendFileSync(logfile, '[' + moment().format('YYYY-MM-DDTHH:mm:ss.SSS') + '] ' + pair + ' | ' + JSON.stringify(['cs_string=' + cs_str, 'cs_calc=' + cs_calc, 'server_checksum=' + checksum]) + '\n');
			if (cs_calc !== checksum)
			{
				Log('BFX CHECKSUM_FAILED', 'BFX OrderBookManager');
				process.exit(-1);
			}
			return;
		}
		
		//		fs.appendFileSync(logfile, '[' + moment().format('YYYY-MM-DDTHH:mm:ss.SSS') + '] ' + pair + ' | ' + JSON.stringify(msg) + '\n');
		
		if (this.BOOK.mcnt === 0)
		{
			_.each(msg[1], (pp) => {
				pp         = {price: pp[0], cnt: pp[1], amount: pp[2]};
				const side = pp.amount >= 0 ? 'bids' : 'asks';
				pp.amount  = Math.abs(pp.amount);
				//				if (this.BOOK[side][pp.price])
				//				{
				//					fs.appendFileSync(logfile, '[' + moment().format() + '] ' + pair + ' | ' + JSON.stringify(pp) + ' BOOK snap existing bid override\n');
				//				}
				this.BOOK[side][pp.price] = pp;
			});
		}
		
		else
		{
			const cseq = +msg[2];
			msg        = msg[1];
			
			if (!this.seq)
			{
				this.seq = cseq - 1;
			}
			
			if (cseq - this.seq !== 1)
			{
				Log(this.seq, 'BFX OrderBookManager OUT OF SEQUENCE');
				process.exit();
			}
			
			this.seq = cseq;
			
			let pp = {price: msg[0], cnt: msg[1], amount: msg[2]};
			
			if (!pp.cnt)
			{
				let found = true;
				
				if (pp.amount > 0)
				{
					if (this.BOOK['bids'][pp.price])
					{
						delete this.BOOK['bids'][pp.price];
					}
					else
					{
						found = false;
					}
				}
				else if (pp.amount < 0)
				{
					if (this.BOOK['asks'][pp.price])
					{
						delete this.BOOK['asks'][pp.price];
					}
					else
					{
						found = false;
					}
				}
				
				if (!found)
				{
					//					fs.appendFileSync(logfile, '[' + moment().format() + '] ' + pair + ' | ' + JSON.stringify(pp) + ' BOOK delete fail side not found\n');
				}
			}
			else
			{
				let side                  = pp.amount >= 0 ? 'bids' : 'asks';
				pp.amount                 = Math.abs(pp.amount);
				this.BOOK[side][pp.price] = pp;
			}
		}
		
		_.each(['bids', 'asks'], (side) => {
			let
				sbook   = this.BOOK[side],
				bprices = Object.keys(sbook);
			
			this.BOOK.psnap[side] = bprices.sort(function (a, b) {
				if (side === 'bids')
				{
					return +a >= +b ? -1 : 1;
				}
				else
				{
					return +a <= +b ? -1 : 1;
				}
			});
		});
		
		this.BOOK.mcnt++;
		this.checkCross(msg);
		//		console.log('=================================');
		//		console.log('2: ' + this.BOOK.asks[Object.keys(this.BOOK.asks)[0]].price + '\t\t\t' + this.BOOK.asks[Object.keys(this.BOOK.asks)[0]].amount)
	}
	
	static run(market: Markets) {
		
		return new Promise((resolve, reject) => {
			if (this.connecting || this.connected)
			{
				if (this.connected)
				{
					resolve(true);
				}
				return;
			}
			setInterval(() => {
//				console.log('setInterval');
				if (this.connected)
				{
					return;
				}
				this.market     = market;
				this.connecting = true;
				this.cli        = new WS(conf.wshost, {});
				
				this.cli.on('open', () => {
					resolve(true);
					this.onOpen();
				});
				
				this.cli.on('close', this.onClose.bind(this));
				this.cli.on('message', this.onMessage.bind(this));
				
			}, 3500);
			
		});
		
	}
	
	static checkCross(msg) {
		let
			bid = this.BOOK.psnap.bids[0],
			ask = this.BOOK.psnap.asks[0];
		
		if (bid >= ask)
		{
			Log('bid(' + bid + ')>=ask(' + ask + ')', 'BFX OrderBookManager WTF BID > ASK');
		}
	}
}

(async () => {
//	BFXOrderBookManager.run(Markets.USDT_BTC);
})();
