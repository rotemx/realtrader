import {Markets} from '../../types/enums/Markets';
import {Book, BookEntry} from '../../types/interfaces/Book';
import {EXCHANGE_CONFIG} from '../EXCHANGE_CONFIG';
import * as http from 'superagent';
import {Exchanges} from '../../types/enums/Exchanges';
import {Log} from '../../utils/Log';
import {assertBookData} from '../../utils/AssertBookData';
//import {BFXOrderBookManager} from './BFX-orderbooks-manager/OrderBookManager';

const
	bitfinex_api_node = require('bitfinex-api-node'),
	bfx               = new bitfinex_api_node({
		apiKey   : EXCHANGE_CONFIG.BFX.key,
		apiSecret: EXCHANGE_CONFIG.BFX.secret,
		
		ws: {
			autoReconnect: true,
			seqAudit     : true,
			packetWDDelay: 10 * 1000,
		},
	}),
	ws                = bfx.ws(2, {transform: true});

export class BFX
{
	
	static fee = 0.002;
	       fee = 0.002;
	
	static exName = Exchanges.BFX;
	       exName = Exchanges.BFX;
	
	static getBookAjax(market: Markets, cycleId?: number): Promise<Book> {
		
		let
			url     = `https://api.bitfinex.com/v2/book/${this.marketToString(market)}/P0`,
			request = http.get(url);
		
		request.catch(err => Log(err.text, 'BFX/Ajax'));
		
		return request.then((res: any) => {
			if (!res || !res.body || !res.body.length)
			{
				return null;
			}
			
			let
				sellEntries: BookEntry[] = res.body.slice(0, 24).map(a => ({price: a[0], amount: a[2]})),
				buyEntries: BookEntry[]  = res.body.slice(25, 49).map(a => ({price: a[0], amount: a[2]})),
				
				book: Book               = {
					market   : market,
					fee      : BFX.fee,
					exName   : BFX.exName,
					timestamp: cycleId || Date.now(),
					cycleId  : cycleId || Date.now(),
					buy      : buyEntries.slice(0, 10),
					sell     : sellEntries.slice(0, 10),
					source   : 'ajax',
				};
			if (book.buy && book.buy.length && book.sell && book.sell.length && book.buy[0].price < book.sell[0].price)
			{
				Log(`WTF ! BFX buy ${book.buy[0].price} < BFX_sell ${book.sell[0].price}`, 'BFX getAjaxBook WTF!');
				return null;
			}
			
			if (!assertBookData(book))
			{
				return null;
			}
			return book;
		});
	}
	
	static getBook(market: Markets) {
		
//		const
//			b          = BFXOrderBookManager.BOOK;
//			book: Book = {
//				buy:b.asks,
//			};
//
		return Promise.resolve(null);
	}
	
	static stringToMarket(market_str: string): Markets {
		return {
			'tBTCUSD': Markets.USDT_BTC,
			//			'USDT-ETH': Markets.USDT_ETH,
			//			'USDT-LTC': Markets.USDT_LTC,
			//			'USDT-NEO': Markets.USDT_NEO,
			//			'USDT-BCC': Markets.USDT_BCC,
			//
			//			'BTC-ETH': Markets.BTC_ETH,
			//			'BTC-LTC': Markets.BTC_LTC,
			//			'BTC-BCC': Markets.BTC_BCC,
			//			'BTC-XRP': Markets.BTC_XRP,
			//			'BTC-XVG': Markets.BTC_XVG,
			//			'BTC-TRX': Markets.BTC_TRX,
		}[market_str];
		
	}
	
	static marketToString(market: Markets): string {
		return {
			[Markets.USDT_BTC]: 'tBTCUSD',
			//			[Markets.USDT_ETH]: 'USDT-ETH',
			//			[Markets.USDT_LTC]: 'USDT-LTC',
			//			[Markets.USDT_NEO]: 'USDT-NEO',
			//			[Markets.USDT_BCC]: 'USDT-BCC',
			//
			//			[Markets.BTC_ETH]: 'BTC-ETH',
			//			[Markets.BTC_LTC]: 'BTC-LTC',
			//			[Markets.BTC_BCC]: 'BTC-BCC',
			//			[Markets.BTC_XRP]: 'BTC-XRP',
			//			[Markets.BTC_XVG]: 'BTC-XVG',
			//			[Markets.BTC_TRX]: 'BTC-TRX',
		}[market];
	}
	
}

/*
(async () => {
/!*
	ws.open();
	
	ws.on('open', () => {
		const symbol = BFX.marketToString(Markets.USDT_BTC);
		
		ws.subscribeOrderBook(symbol, "P0", 25);
		
		ws.onOrderBook({symbol: symbol, prec: 'P0', len: '25'}, (res) => {
			//			console.log('---------------------------------------------------------------------');
			if (res.asks && res.asks[0])
			{
				console.log(`1: ${res.asks[0][0]}\t\t\t ${res.asks[0][1]}`);
//				console.log('1: ' + res.asks && res.asks[0] && res.asks[0][0] + '\t\t\t' + res.asks && res.asks[0] && res.asks[0][2]);
				
			}
			//			console.log('---------------------------------------------------------------------');
		});
		
	});	//	setInterval(async () => {
*!/
	
	//		BFX.getBookAjax(Markets.USDT_BTC).then(bfx_book => {
	
	//			console.log(bfx_book.sell[0].price);
	//		}).catch(err => Log(err, 'BFX/Test'));
	//	}, 700);
})();
*/
