export const BITTREX_DEMO_BOOK = {
	"book"     : {
		"market"   : "USDT_BTC",
		"fee"      : 0.0005,
		"timestamp": 1522239913701.0,
		"exName"   : "binance",
		"buy"      : [
			{
				"price" : 8038.66,
				"amount": 0.034738
			},
			{
				"price" : 8038.67,
				"amount": 0.037
			},
			{
				"price" : 8038.68,
				"amount": 0.827802
			},
			{
				"price" : 8038.8,
				"amount": 0.306635
			},
			{
				"price" : 8039,
				"amount": 0.053466
			},
			{
				"price" : 8039.95,
				"amount": 1.10817
			},
			{
				"price" : 8039.96,
				"amount": 7.686646
			},
			{
				"price" : 8039.98,
				"amount": 0.016386
			},
			{
				"price" : 8040,
				"amount": 0.843716
			},
			{
				"price" : 8041,
				"amount": 0.121749
			},
			{
				"price" : 8041.96,
				"amount": 0.309396
			},
			{
				"price" : 8041.97,
				"amount": 0.019694
			},
			{
				"price" : 8042,
				"amount": 0.020208
			},
			{
				"price" : 8042.88,
				"amount": 1
			},
			{
				"price" : 8043.28,
				"amount": 0.02
			},
			{
				"price" : 8044.6,
				"amount": 3
			},
			{
				"price" : 8044.61,
				"amount": 0.003488
			},
			{
				"price" : 8044.91,
				"amount": 0.195786
			},
			{
				"price" : 8044.99,
				"amount": 0.143768
			},
			{
				"price" : 8045,
				"amount": 0.314426
			}
		],
		"sell"     : [
			{
				"price" : 8033.1,
				"amount": 0.525088
			},
			{
				"price" : 8033,
				"amount": 7.441786
			},
			{
				"price" : 8031,
				"amount": 0.103072
			},
			{
				"price" : 8030.01,
				"amount": 1.273894
			},
			{
				"price" : 8030,
				"amount": 0.227779
			},
			{
				"price" : 8029,
				"amount": 0.027126
			},
			{
				"price" : 8028,
				"amount": 0.392558
			},
			{
				"price" : 8026.1,
				"amount": 0.739165
			},
			{
				"price" : 8026,
				"amount": 0.073564
			},
			{
				"price" : 8025,
				"amount": 1.031572
			},
			{
				"price" : 8023.04,
				"amount": 0.001744
			},
			{
				"price" : 8022.94,
				"amount": 0.00687
			},
			{
				"price" : 8022.85,
				"amount": 0.003295
			},
			{
				"price" : 8022.27,
				"amount": 0.003337
			},
			{
				"price" : 8022.2,
				"amount": 0.012768
			},
			{
				"price" : 8022.01,
				"amount": 0.07426
			},
			{
				"price" : 8022,
				"amount": 0.14572
			},
			{
				"price" : 8021.99,
				"amount": 0.020363
			},
			{
				"price" : 8021.9,
				"amount": 0.245622
			},
			{
				"price" : 8021.57,
				"amount": 0.00327
			}
		]
	},
	"timestamp": 1522239913701.0,
	"cycleId"  : 1522239913828.0
};
