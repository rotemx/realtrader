export interface BITTREX_API_CancelOrder_Reply
{
	"success": boolean
	"message": string
	"result": null
}
