
import {Coins} from '../../../types/enums/Coins';

interface BITTREX_API_Get_Balance_Entry{
	"Currency": string,
	"Balance": number,
	"Available": number,
	"Pending": number,
	"CryptoAddress": string
	
}

export interface BITTREX_API_Get_Balances_Reply
{
	"success": boolean,
	"message": string,
	"result": BITTREX_API_Get_Balance_Entry[]
}

/*
{
	"success": true,
	"message": "",
	"result": [
	{
		"Currency": "ARK",
		"Balance": 0,
		"Available": 0,
		"Pending": 0,
		"CryptoAddress": null
	},
	{
		"Currency": "BCC",
		"Balance": 0,
		"Available": 0,
		"Pending": 0,
		"CryptoAddress": null
	},
	{
		"Currency": "BTC",
		"Balance": 0.49324787,
		"Available": 0.49324787,
		"Pending": 0,
		"CryptoAddress": "1CGia4wQCvw7W9z1fNPph5G4sx3a4iwKbN"
	},
	{
		"Currency": "ENG",
		"Balance": 0,
		"Available": 0,
		"Pending": 0,
		"CryptoAddress": null
	},
	{
		"Currency": "MCO",
		"Balance": 0,
		"Available": 0,
		"Pending": 0,
		"CryptoAddress": null
	},
	{
		"Currency": "OMG",
		"Balance": 0,
		"Available": 0,
		"Pending": 0,
		"CryptoAddress": null
	},
	{
		"Currency": "POWR",
		"Balance": 0,
		"Available": 0,
		"Pending": 0,
		"CryptoAddress": null
	},
	{
		"Currency": "RDD",
		"Balance": 0,
		"Available": 0,
		"Pending": 0,
		"CryptoAddress": null
	},
	{
		"Currency": "USDT",
		"Balance": 38.42003019,
		"Available": 38.42003019,
		"Pending": 0,
		"CryptoAddress": null
	},
	{
		"Currency": "XLM",
		"Balance": 0,
		"Available": 0,
		"Pending": 0,
		"CryptoAddress": null
	},
	{
		"Currency": "XRP",
		"Balance": 0,
		"Available": 0,
		"Pending": 0,
		"CryptoAddress": "56698806"
	},
	{
		"Currency": "XVG",
		"Balance": 0,
		"Available": 0,
		"Pending": 0,
		"CryptoAddress": null
	}
]
}*/
