
import {Coins} from '../../../types/enums/Coins';

export interface BITTREX_API_Get_Balance_Reply
{
	"success": boolean,
	"message": string,
	"result": {
		"Currency": Coins,
		"Balance": number,
		"Available": number,
		"Pending": number,
		"CryptoAddress": string
	}
}
/*
{
	"success": true,
	"message": "",
	"result": {
	"Currency": "BTC",
		"Balance": 0.49324787,
		"Available": 0.49324787,
		"Pending": 0,
		"CryptoAddress": "1CGia4wQCvw7W9z1fNPph5G4sx3a4iwKbN"
}
*/
