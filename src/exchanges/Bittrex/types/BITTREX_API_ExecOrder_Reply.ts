import {Markets} from '../../../types/enums/Markets'
import {Coins} from '../../../types/enums/Coins'

export interface BITTREX_API_ExecOrder_Reply
{
	"success": boolean,
	"message": string,
	"result": {
		OrderId: string,
		MarketName: Markets,
		MarketCurrency: Coins.BTC,
		BuyOrSell: 'Sell' | 'Buy',
		OrderType: "LIMIT",
		Quantity: number,
		Rate: number
	}
}

