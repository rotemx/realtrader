import {Markets} from '../../../types/enums/Markets'

export interface BITTREX_API_OpenOrder
{
	Uuid: string,
	OrderUuid: string,
	Exchange: Markets,
	OrderType: 'LIMIT_SELL' | 'LIMIT_BUY',
	Quantity: number,
	QuantityRemaining: number,
	Limit: number,
	CommissionPaid: number,
	Price: number,          //total cost
	PricePerUnit: number,
	Opened: number,
	Closed: null,
	CancelInitiated: boolean,
	ImmediateOrCancel: boolean,
	IsConditional: boolean,
	Condition: string,
	ConditionTarget: number
}

export interface BITTREX_API_GetOpenOrdersReply
{
	success: boolean,
	message: string,
	result: BITTREX_API_OpenOrder[]
	
}
