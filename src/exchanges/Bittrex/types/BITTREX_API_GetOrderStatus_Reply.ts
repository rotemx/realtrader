export interface BITTREX_API_GetOrderStatus_Reply
{
	message: string,
	success: boolean,
	result: {
		"AccountId": string,
		"OrderUuid": string,
		"Exchange": "USDT-BTC" | "USDT-ETH",
		"Type": "LIMIT_SELL" | "LIMIT_BUY",
		"Quantity": number,
		"QuantityRemaining": number,
		"Limit": number,
		"Reserved": number,
		"ReserveRemaining": number,
		"CommissionReserved": number,
		"CommissionReserveRemaining": number,
		"CommissionPaid": number,
		"Price": number,
		"PricePerUnit": number,
		"Opened": string,
		"Closed": string,
		"IsOpen": boolean,
		"Sentinel": string,
		"CancelInitiated": boolean,
		"ImmediateOrCancel": boolean,
		"IsConditional": boolean,
		"Condition": string,
		"ConditionTarget": number
		
	}
}

/*
{
	"AccountId": null,
	"OrderUuid": "caa581d6-214b-4f0d-9b9b-c71f010808d2",
	"Exchange": "USDT-BTC",
	"Type": "LIMIT_SELL",
	"Quantity": 0.001,
	"QuantityRemaining": 0.001,
	"Limit": 20000,
	"Reserved": 0.001,
	"ReserveRemaining": 0.001,
	"CommissionReserved": 0,
	"CommissionReserveRemaining": 0,
	"CommissionPaid": 0,
	"Price": 0,
	"PricePerUnit": null,
	"Opened": "2018-03-14T12:54:04.277",
	"Closed": null,
	"IsOpen": true,
	"Sentinel": "5aa9cdf7-1d3c-45d8-8eab-136aa11769a7",
	"CancelInitiated": false,
	"ImmediateOrCancel": false,
	"IsConditional": false,
	"Condition": "NONE",
	"ConditionTarget": null
}*/
