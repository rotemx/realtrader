import {Observable} from 'rxjs';

import * as http from 'superagent';
import {Log} from '../../utils/Log';
import {Book, BookEntry} from '../../types/interfaces/Book';
import {IOrder, Order} from '../../entites/Order';
import {Exchange} from '../abstract/Exchange';
import {Exchanges} from '../../types/enums/Exchanges';
import {Markets} from '../../types/enums/Markets';
import {Redis} from '../../DB/Redis';
import {OrderTypes} from '../../types/enums/OrderTypes';
import * as node_bittrex_api from 'node.bittrex.api';
import {BITTREX_API_GetOpenOrdersReply} from './types/BITTREX_API_GetOpenOrders_Reply';
import {BITTREX_API_ExecOrder_Reply} from './types/BITTREX_API_ExecOrder_Reply';
import {API_Open_Order} from '../types/API_Open_Order';
import {BITTREX_API_GetOrderStatus_Reply} from './types/BITTREX_API_GetOrderStatus_Reply';
import {API_Order_Status} from '../types/API_Order_Status';
import {API_CancelOrder_Reply} from '../types/API_CancelOrder_Reply';
import {BITTREX_API_CancelOrder_Reply} from './types/BITTREX_API_CancelOrder_Reply';
import {OrderStatuses} from '../../types/enums/OrderStatuses';
import {EXCHANGE_CONFIG} from '../EXCHANGE_CONFIG';
import * as MarketManager from '../../../lib/bittrex-market';
import {API_Get_Balances_Reply} from '../types/API_Get_Balances_Reply';
import {BITTREX_API_Get_Balances_Reply} from './types/BITTREX_API_Get_Balances_Reply';
import {fix8} from '../../utils/fix';
import {Execution} from '../../types/interfaces/Execution';
import {assertBookData} from '../../utils/AssertBookData';
import {Subscriber} from 'rxjs/Subscriber';
import {SECOND, MINUTE} from '../../GLOBALS';
import Timer = NodeJS.Timer;

const
	public_api_url = 'https://bittrex.com/api/v1.1/public',
	market_api_url = 'https://bittrex.com/api/v1.1/market',
	INTERVAL       = 1100;

node_bittrex_api.options({
	apikey   : EXCHANGE_CONFIG.bittrex.key,
	apisecret: EXCHANGE_CONFIG.bittrex.secret,
	cleartext: false,
	verbose  : false,
});

export class Bittrex extends Exchange
{
	//region members
	static key = `exchanges:1`;
	       key = `exchanges:1`;
	
	static id = 1;
	       id = 1;
	
	static fee = 0.0025;
	       fee = 0.0025;
	
	static Model = Bittrex;
	       Model = Bittrex;
	
	static collection_name = 'exchanges';
	       collection_name = 'exchanges';
	
	static list_key = `all-exchanges`;
	       list_key = `all-exchanges`;
	
	static exName = Exchanges.bittrex;
	       exName = Exchanges.bittrex;
	
	static instances: Bittrex[] = [];
	
	set_interval: Timer;
	buy_power_ratio: number = 1 - this.fee;
	
	//endregion
	
	constructor(isNewInstance?: boolean) {
		super(Exchanges.bittrex, isNewInstance);
		this.Model           = Bittrex;
		this.Model.instances = this.Model.instances || [];
	}
	
	is_obs_running   = false;
	obs_running_flag = false;
	
	last_obs_update_time;
	
	getBook$(market: Markets): Observable<Book> {
		
		const market_str = this.marketToString(market);
		
		return new Observable<Book>(obs => {
			try
			{
				Log('[V] opening socket', 'bittrex,getBook$');
				
				let minutes = 30;
				
				setInterval(() => {
					Log(`[!] resetting Bittrex socket every ${minutes} minutes.`, `Bittrex:getBook$`, 'balances');
					this.marketManager.reset();
					this.marketManager = new MarketManager(false);
					this.registerSocket(market_str, obs);
				}, minutes * MINUTE);
				
				this.registerSocket(market_str, obs);
			}
			catch (err)
			{
				Log('[!] BITTREX SOCKET ERRRRRR ', 'bittrex/getBook$');
				Log(err);
			}
		}).retry();
		
	}
	
	registerSocket(market_str: string, obs: Subscriber<Book>) {
		this.marketManager.market(market_str, (err, socket) => {
			if (err)
			{
				Log(err, '[!] bittrex:registerSocket:marketManager');
				return obs.error('Obs has erred');
			}
			
			socket.on('orderbookUpdated', () => {
				if (!this.obs_running_flag)
				{
					Log('[>] Bittrex Obs is running');
					this.obs_running_flag = true;
				}
				
				const
					now                   = Date.now(),
					time_from_last_update = now - this.last_obs_update_time;
				
				if (time_from_last_update > 20 * SECOND)
				{
					Log(`Orderbook Socket update received after ${time_from_last_update / SECOND} seconds.`, `[W] Bittrex:getBook$:onMarketUpdate`);
				}
				
				this.last_obs_update_time = now;
				this.is_obs_running       = true;
				
				const
					buyRecords: BookEntry[]  = socket.asks
					                                 .slice(0, this.book_length)
					                                 .map((obj) => ({price: obj[0], amount: obj[1]})), //HIGHER
					sellRecords: BookEntry[] = socket.bids
					                                 .slice(0, this.book_length)
					                                 .map((obj) => ({price: obj[0], amount: obj[1]})), //LOWER
					
					book: Book               = {
						market   : this.stringToMarket(market_str),
						fee      : Bittrex.fee,
						exName   : Bittrex.exName,
						timestamp: now,
						buy      : buyRecords,
						sell     : sellRecords,
						source   : 'socket',
					};
				
				//						console.log(`>> ${sellRecords[0].price} ${buyRecords[0].price}`);
				
				if (book.buy[0].price <= book.sell[0].price)
				{
					Log(`[!] WTF ! BITTREX SOCKET buy ${book.buy[0].price} < sell ${book.sell[0].price}`, 'Bittrex getbookAjax$ WTF!');
					Log('[X] Oops.... Obs is bad', 'bittrex,getBook$');
					this.marketManager.reset();
					``;
					this.marketManager = new MarketManager(false);
					obs.error('Obs is bad');
					return;
				}
				
				obs.next(book);
			});
			
		});
		
		setTimeout(() => {
			if (!this.is_obs_running)
			{
				Log('[X] Oops.... Obs is not running', 'bittrex,getBook$');
				this.marketManager.reset();
				``;
				this.marketManager = new MarketManager(false);
				obs.error('Obs is not running');
			}
		}, 10 * SECOND);
		
	}
	
	marketManager;
	
	deductExecutionFromOpenOrder(open_order: { filled_amount: number, total_cost: number, fee_paid: number, isOpen: boolean } = {filled_amount: 0, total_cost: 0, fee_paid: 0, isOpen: true}, order: Order): Execution {
		const zero_response = {
			amount: 0,
			price : 0,
			fee   : 0,
			isOpen: open_order.isOpen,
		};
		
		const
			ACTUAL_cost_or_income = (order.type === OrderTypes.buy ? order.ACTUAL_cost : order.ACTUAL_income),
			log_info              = `${order.id} ${order.type} ${order.amount}@ Bittrex`;
		
		if (
			open_order.filled_amount < order.filled_amount
			|| order.filled_amount > 0 && ACTUAL_cost_or_income === 0
			|| order.filled_amount === 0 && ACTUAL_cost_or_income > 0
			|| open_order.filled_amount === 0 && ACTUAL_cost_or_income > 0
		)
		{
			Log(`WTF WHAT IS IT SHIT ??!?!  ?! Y like this?! `, `deductExecutionFromOpenOrder ${log_info}`);
			Log(open_order, ` deduct / open_order ${log_info}`, 'bittrex');
			//			Log(order, 'deduct / order');
			return zero_response;
		}
		
		if (Math.abs(open_order.filled_amount - order.filled_amount) >= 0.00000001)
		{
			const
				delta_cost              = fix8(open_order.total_cost - ACTUAL_cost_or_income),
				delta_amount            = fix8(open_order.filled_amount - order.filled_amount),
				delta_price             = fix8(delta_cost / delta_amount),
				delta_fee               = fix8(open_order.fee_paid - order.ACTUAL_fee),
				return_value: Execution = {
					amount: delta_amount,
					price : delta_price,
					fee   : delta_fee,
					isOpen: open_order.isOpen,
				};
			
			Log(open_order, 'deduct / open_order ', 'bittrex');
			Log(order, 'deduct / order', 'bittrex');
			Log(return_value, 'deduct / return_value', 'bittrex');
			return return_value;
		}
		return zero_response;
	}
	
	protected async _run() {
		
		//region marketManager
		try
		{
			this.marketManager = new MarketManager(false);
		} catch (e)
		{
			console.log(e);
		}
		//endregion
		
		this.set_interval = setInterval(async () => {
			const active_orders = this.orders.filter(or => or.status === OrderStatuses.partial || or.status === OrderStatuses.open);
			
			active_orders
				.forEach(async active_order => {
					let status = await this._getOrderStatus(active_order.orderId);
					if (status.filled_amount && active_order.status === OrderStatuses.open)
					{
						active_order.setStatus(OrderStatuses.partial);
					}
					if (!status.isOpen)
					{
						if (status.filled_amount === active_order.amount)
						{
							active_order.setStatus(OrderStatuses.filled);
						}
						if (status.filled_amount === 0)
						{
							active_order.setStatus(OrderStatuses.missed);
							
						}
						if (status.filled_amount > 0 && status.filled_amount < active_order.amount)
						{
							active_order.setStatus(OrderStatuses.partial);
						}
					}
					
					let execution = this.deductExecutionFromOpenOrder({
						filled_amount: status.filled_amount,
						total_cost   : status.total_cost,
						fee_paid     : status.fee_paid,
						isOpen       : status.isOpen,
					}, active_order);
					
					if (!execution)
					{
						Log(active_order, '[!] WTF bittrex/run execution is undefined ?! active_order is:');
						Log(status, '[!] WTF bittrex/run execution is undefined ?! status is:');
					}
					active_order.aggregateExecutions(execution);
					active_order.transaction.tryFinish();
				});
		}, 2000);
	}
	
	protected _stop() {
		clearInterval(this.set_interval);
	}
	
	get activeMarkets() {
		return [
			Markets.USDT_BTC,
			Markets.USDT_ETH,
			Markets.USDT_LTC,
			Markets.USDT_NEO,
			Markets.USDT_BCC,
			
			Markets.BTC_ETH,
			Markets.BTC_LTC,
			Markets.BTC_BCC,
			Markets.BTC_XRP,
			Markets.BTC_XVG,
			Markets.BTC_TRX,
		];
	}
	
	marketToString(market: Markets): string {
		return {
			[Markets.USDT_BTC]: 'USDT-BTC',
			[Markets.USDT_ETH]: 'USDT-ETH',
			[Markets.USDT_LTC]: 'USDT-LTC',
			[Markets.USDT_NEO]: 'USDT-NEO',
			[Markets.USDT_BCC]: 'USDT-BCC',
			
			[Markets.BTC_ETH]: 'BTC-ETH',
			[Markets.BTC_LTC]: 'BTC-LTC',
			[Markets.BTC_BCC]: 'BTC-BCC',
			[Markets.BTC_XRP]: 'BTC-XRP',
			[Markets.BTC_XVG]: 'BTC-XVG',
			[Markets.BTC_TRX]: 'BTC-TRX',
		}[market];
	}
	
	stringToMarket(market_str: string): Markets {
		return {
			'USDT-BTC': Markets.USDT_BTC,
			'USDT-ETH': Markets.USDT_ETH,
			'USDT-LTC': Markets.USDT_LTC,
			'USDT-NEO': Markets.USDT_NEO,
			'USDT-BCC': Markets.USDT_BCC,
			
			'BTC-ETH': Markets.BTC_ETH,
			'BTC-LTC': Markets.BTC_LTC,
			'BTC-BCC': Markets.BTC_BCC,
			'BTC-XRP': Markets.BTC_XRP,
			'BTC-XVG': Markets.BTC_XVG,
			'BTC-TRX': Markets.BTC_TRX,
		}[market_str];
	}
	
	static async load(): Promise<Bittrex> {
		return Redis.loadEntity(Bittrex, this.id)
		            .then(loadedInstance => {
			            let ex: Bittrex;
			
			            if (loadedInstance)
			            {
				            ex = loadedInstance;
			            }
			            else
			            {
				            ex           = new Bittrex(true); //null for newly created exchange instance
				            ex.isRunning = true;
				            ex.save();
			            }
			            Exchange.running_instances[this.id] = {exName: ex.exName, instance: ex, id: ex.id};
			            return ex;
		            });
	}
	
	getBookAjax$(market: Markets = Markets.USDT_BTC): Observable<Book> {
		
		let counter      = 0;
		let lastReceived = 0;
		return Observable
			.interval(INTERVAL)
			.mergeMap(() => {
				counter++;
				const counterSend = counter;
				return this.getBookAjax(market)
				           .map(data => ({data, counter: counterSend}))
				           .catch(error => {
					           return Observable.of(null);
				           });
			}).filter(res => !!res)
			.filter(res => res.counter >= lastReceived)
			.do(res => lastReceived = res.counter)
			.map(res => res.data)
			.shareReplay(1);
	}
	
	getBookAjax(market: Markets, cycleId?: number): Observable<Book> {
		let market_str = this.marketToString(market);
		
		const request = http
			.get(`${public_api_url}/getorderbook`)
			.set('Content-Type', 'application/json')
			.query({'market': market_str})
			.query({'type': 'both'})
			.catch(err => Log(err.text, 'bittrex/getBookAjax'));
		
		if (!request)
		{
			return;
		}
		
		return Observable.fromPromise(request)
		                 .map((res: any) => {
			                 if (!res || !res.body || !res.body.result || !res.body.result.buy || !res.body.result.sell)
			                 {
				                 return null;
			                 }
			
			                 const
				                 data                     = res.body.result,
				                 buyRecords: BookEntry[]  = data.sell.slice(0, this.book_length).map((obj) => ({price: obj.Rate, amount: obj.Quantity})), //HIGHER
				                 sellRecords: BookEntry[] = data.buy.slice(0, this.book_length).map((obj) => ({price: obj.Rate, amount: obj.Quantity})), //LOWER
				
				                 book: Book               = {
					                 market   : market,
					                 fee      : Bittrex.fee,
					                 exName   : Bittrex.exName,
					                 timestamp: cycleId || Date.now(),
					                 cycleId  : cycleId || Date.now(),
					                 buy      : buyRecords,
					                 sell     : sellRecords,
					                 source   : 'ajax',
				                 };
			
			                 if (book.buy && book.buy.length && book.sell && book.sell.length && book.buy[0].price < book.sell[0].price)
			                 {
				                 Log(`WTF ! bittrex buy ${book.buy[0].price} < bittrex_sell ${book.sell[0].price}`, 'Bittrex getAjaxBook WTF!');
				                 return null;
			                 }
			
			                 if (!assertBookData(book))
			                 {
				                 return null;
			                 }
			
			                 return book;
		                 })
		                 .shareReplay(1)
		                 .catch((err, caught) => Observable.throw(err));
	}
	
	protected _getOpenOrders(market?: Markets): Promise<API_Open_Order[]> {
		
		let prom = new Promise((resolve, reject) => {
			const markets = market ? {market: this.marketToString(market)} : {};
			
			node_bittrex_api.getopenorders(markets, function (res: BITTREX_API_GetOpenOrdersReply, err) {
				if (err || !res || !res.success)
				{
					return reject(err || 'Bittrex get open orders failed ');
				}
				Log(res, 'bittrex/_getOpenOrders res', 'bittrex');
				const map: API_Open_Order[] = res.result.map(open_order => {
						let api_open_order: API_Open_Order = {
							total_amount    : open_order.Quantity,
							remaining_amount: open_order.QuantityRemaining,
							filled_amount   : open_order.Quantity - open_order.QuantityRemaining,
							total_cost      : open_order.Price,
							price_per_unit  : open_order.PricePerUnit,
							isClosed        : open_order.Closed,
							orderId         : open_order.OrderUuid,
							fee_paid        : open_order.CommissionPaid,
						};
						return api_open_order;
					},
				);
				resolve(map);
			});
		});
		
		prom.catch((err) => {
			Log(err, 'bittrex/getOpenOrders');
		});
		
		return prom as Promise<API_Open_Order[]>;
	}
	
	protected _cancelOrder(order: IOrder): Promise<API_CancelOrder_Reply> {
		return new Promise<API_CancelOrder_Reply>((resolve, reject) => {
			
			node_bittrex_api.cancel({uuid: order.orderId}, function (res: BITTREX_API_CancelOrder_Reply, err) {
				if (err || !res || res.success)
				{
					reject(err);
					return;
				}
				
				resolve({
					timestamp: Date.now(),
				});
			});
		});
	}
	
	protected _execOrder(order: Order): Promise<Order> {
		
		const
			me    = this,
			apiFn = order.type === OrderTypes.buy ? node_bittrex_api.tradebuy : node_bittrex_api.tradesell;
		
		let prom = new Promise<Order>((resolve, reject) => {
			
			Log(`[>] Executing ${order.type} order ${order.id}: amount ${order.amount} at EXP price ${order.EXP_price} (SAFE ${order.SAFE_price})`, 'Bittrex');
			
			order.set({
				placed_time: Date.now(),
			});
			
			apiFn({
				MarketName   : this.marketToString(order.market),
				OrderType    : 'LIMIT',
				Quantity     : order.amount,
				Rate         : order.SAFE_price,
				TimeInEffect : 'IMMEDIATE_OR_CANCEL',                              // supported options are 'IMMEDIATE_OR_CANCEL', 'GOOD_TIL_CANCELLED', 'FILL_OR_KILL'
				ConditionType: 'NONE',                                      // supported options are 'NONE', 'GREATER_THAN', 'LESS_THAN'
				Target       : 0,                                           // used in conjunction with ConditionType
			}, function (res: BITTREX_API_ExecOrder_Reply, err) {
				if (err || !res || !res.success)
				{
					reject(err || "Bittrex exec order failed !");
					return;
				}
				
				const id = res.result.OrderId;
				Log(res, `Bittrex/execOrder id ${order.id}: ${order.type} ${order.amount}@${order.EXP_price} exec res:`);
				
				order.set({
					     orderId: res.result.OrderId,
				     })
				     .then(() => {
					     return me._getOrderStatus(id)
					              .then(async order_status => {
						              Log(order_status, `BITTREX execOrder/_getOrderStatus after exec: order_status id ${order.id}: ${order.amount}@${order.EXP_price} (safe ${order.SAFE_price}: `);
						              let status: OrderStatuses;
						
						              switch (true)
						              {
							              case order_status.isOpen && order_status.filled_amount === 0:
								              status = OrderStatuses.open;
								              break;
							              case order_status.filled_amount > 0 && order_status.filled_amount < order.amount && order_status.isOpen:
								              status = OrderStatuses.partial; //partial_open
								              break;
							              case order_status.filled_amount === order.amount && !order_status.isOpen:
								              status = OrderStatuses.filled;
								              break;
							              case order_status.filled_amount > order.amount:
								              Log(`order_status.filled_amount ${order_status.filled_amount} > order.amount ${order.amount} ?!`, `WTF bittrex ${order.id}`);
								              break;
							
							              case !order_status.isOpen && order_status.filled_amount > 0 && order_status.filled_amount < order.amount:
								              status = OrderStatuses.partial_missed;
								              break;
							
							              case !order_status.isOpen && order_status.filled_amount === 0:
								              //								              Log(`order_status.filled_amount ${order_status.filled_amount} is zero and order.isOpen is ${order_status.isOpen}. setting status "missed".`, `bittrex ${order.id} ${order.amount}@${order.SAFE_price}`);
								              status = OrderStatuses.missed;
								              break;
						              }
						
						              if (status !== OrderStatuses.open)
						              {
							              await order.aggregateExecutions({
								              price : order_status.actual_price,
								              amount: order_status.filled_amount,
								              fee   : order_status.fee_paid,
								              isOpen: order_status.isOpen,
							              });
							              order.transaction.tryFinish();
						              }
						              order.setStatus(status);
						              resolve(order);
					              });
				     });
			});
		});
		
		prom.catch(err => {
			Log(err, `Bittrex/execorder id ${order.id}: ${order.type} ${order.amount}@${order.EXP_price}`);
		});
		
		return prom;
	}
	
	protected _getOrderStatus(id: string): Promise<API_Order_Status> {
		return new Promise<API_Order_Status>((resolve, reject) => {
			node_bittrex_api.getorder({uuid: id}, (res: BITTREX_API_GetOrderStatus_Reply, err) => {
				if (err || !res || !res.success)
				{
					reject(err);
					return;
				}
				Log(res.result, `bittrex/_getOrderStatus ${id} res`, `bittrex`);
				
				const order_status: API_Order_Status = {
					remaining_amount: res.result.QuantityRemaining,
					filled_amount   : res.result.Quantity - res.result.QuantityRemaining,
					isOpen          : res.result.IsOpen,
					actual_price    : res.result.PricePerUnit,
					total_cost      : res.result.Price,
					fee_paid        : res.result.CommissionPaid,
				};
				resolve(order_status);
			});
		});
		
	}
	
	async getBalances(): Promise<API_Get_Balances_Reply> {
		
		let prom = new Promise<API_Get_Balances_Reply>((resolve, reject) => {
			node_bittrex_api.getbalances(function (data: BITTREX_API_Get_Balances_Reply, err) {
				if (err)
				{
					reject(err);
					return;
				}
				if (data && data.success && data.result)
				{
					let reply: API_Get_Balances_Reply = {};
					
					data.result.forEach(entry => {
						reply[entry['Currency']] = {
							total    : entry.Balance,
							available: entry.Available,
							reserved : fix8(entry.Balance - entry.Available),
						};
					});
					resolve(reply);
				}
				reject('wrong data reply');
			});
		});
		prom.catch(err => {
			Log(err, 'Bittrex/getBalances');
		});
		return prom;
	}
	
	/*
		async getBalance(coin: string): Promise<API_Get_Balance_Reply> {
			
			let prom = new Promise<API_Get_Balance_Reply>((resolve, reject) => {
				node_bittrex_api.getbalance({currency: coin}, function (data: BITTREX_API_Get_Balance_Reply, err) {
					if (err)
					{
						reject(err);
					}
					else if (data && data.success && data.result)
					{
						let reply: API_Get_Balance_Reply = {
							available: data.result.Available,
							balance  : data.result.Balance,
							reserved : data.result.Pending
						};
						 resolve(reply);
					}
				});
			});
			
			prom.catch(err => {
				Log(err, 'Bittrex/getBalance');
				return Promise.reject(err);
			});
			
			return prom;
		}
	*/
	
	getBookSnapshot(market: Markets, cycleId: number) {
		return this.getBookAjax(market, cycleId).toPromise();
	}
	
	getMoneyBalance(balance: number): number {
		return balance;
	}
	
	getCommodityBalance(balance: number): number {
		return balance;
	}
	
	getMaxSellAmount(balance_C: number): number {
		return balance_C;
	}
	
	getMaxBuyAmount(balance_$: number, buy_price: number): number {
		return fix8(balance_$ * buy_price * (1 - this.fee));
	}
	
}
























