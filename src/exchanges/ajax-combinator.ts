import {Bittrex} from './Bittrex/Bittrex';
import {Binance} from './Binance/Binance';
import {Markets} from '../types/enums/Markets';
import {Cycle} from '../entites/Cycle';
import {Observable} from 'rxjs/Observable';
import {Book} from '../types/interfaces/Book';
import {assertBookData} from '../utils/AssertBookData';
import {Subscriber} from 'rxjs/Subscriber';
import {BFX} from './BFX/BFX';
import {Mongo} from '../DB/Mongo';
import {State} from '../modules/State';
//import {BFXOrderBookManager} from './BFX/BFX-orderbooks-manager/OrderBookManager';

export class AjaxCombinator
{
	
	static BINN_last_updateId: number = 0;
	
	static BINN_last_update_time: number = 0;
	static BITT_last_update_time: number = 0;
	static BFX_last_update_time: number  = 0;
	
	static BINN_book: Book;
	static BITT_book: Book;
	static BFX_book: Book;
	
	static getCombinedStream$(bittrex: Bittrex, binance: Binance, market: Markets, bfx?: typeof BFX): Observable<Cycle> {
		
		return new Observable<Cycle>(obs => {
			
			setInterval(async () => {
				let
					start     = Date.now(),
					BINN_prom = binance.getBookAjax(market).toPromise(),
					BITT_prom = bittrex.getBookAjax(market).toPromise();
				
				if (bfx)
				{
//					await BFXOrderBookManager.run(market);
					let BFX_prom = bfx.getBook(market);
					BFX_prom
						.then((book: Book) => {
							const
								BFX_time           = Date.now() - start,
								BFX_time_threshold = 1000;
							
							if (BFX_time < BFX_time_threshold && assertBookData(book))
							{
								this.BFX_last_update_time = Date.now();
								this.BFX_book             = book;
								//							this.publishCycle(obs)
							}
							
						});
				}
				
				BINN_prom
					.then((book: Book) => {
						const
							binance_time        = Date.now() - start,
							BINN_time_threshold = 700;
						
						if (
							book.lastUpdateId > this.BINN_last_updateId
							&& binance_time < BINN_time_threshold &&
							assertBookData(book))
						{
							this.BINN_last_updateId    = book.lastUpdateId;
							this.BINN_book             = book;
							this.BINN_last_update_time = Date.now();
							this.publishCycle(obs);
						}
						else
						{
							//							process.stdout.write('.');
							
							//							if (binance_time > BINN_time_threshold)
							{
								//								console.log(`Binance book arrived too late - ${binance_time} ms`);
							}
							//							if (book.lastUpdateId > this.BINN_last_updateId)
							//							{
							//								console.log(`Binance book arrived after the newer one! last ${this.BINN_last_updateId } current ${book.lastUpdateId}`);
							//							}
						}
					});
				
				BITT_prom
					.then((book: Book) => {
						const
							bittrex_time        = Date.now() - start,
							BITT_time_threshold = 6000;
						
						if (bittrex_time < BITT_time_threshold
							&& assertBookData(book)
						)
						{
							this.BITT_book             = book;
							this.BITT_last_update_time = Date.now();
							this.publishCycle(obs);
						}
					});
			}, 100);
		});
	}
	
	static publishCycle(obs: Subscriber<Cycle>) {
		const
			now               = Date.now(),
			latency_threshold = 1000;
		
		if (
			this.BINN_book
			&& now - this.BINN_last_update_time < latency_threshold
			&& this.BITT_book
			&& now - this.BITT_last_update_time < latency_threshold
			&& this.BFX_book
			&& now - this.BFX_last_update_time < latency_threshold
		)
		{
			let
				cycle = new Cycle({
					time_delta: -1,
					bookA     : this.BITT_book,
					bookB     : this.BINN_book,
					bookC     : this.BFX_book || null,
					cycleId   : Math.max(this.BINN_last_update_time, this.BITT_last_update_time),
					timestamp : Math.min(this.BINN_last_update_time, this.BITT_last_update_time),
					sourceA   : 'ajax',
					sourceB   : 'ajax',
				});
			
			obs.next(cycle);
		}
		else
		{
			//						process.stdout.write('.');
			//						console.log(`
			//											now - this.BINN_last_update_time ${now - this.BINN_last_update_time}
			//											now - this.BITT_last_update_time ${now - this.BITT_last_update_time}
			//											not fresh enough !`);
		}
	}
	
	static async init() {
		await State.init();
		await Mongo.init();
		
		const
			binance: Binance = await Binance.load(),
			bittrex: Bittrex = await Bittrex.load();
		
		AjaxCombinator.getCombinedStream$(bittrex, binance, Markets.USDT_BTC, BFX)
		              .subscribe(cycle => {
			              process.stdout.write('.');
		              });
		
		setInterval(() => {
			const
				db_suffix = 'CN200',
				cycleId   = Date.now();
			
			if (this.BINN_book && this.BITT_book && this.BFX_book)
			{
				Mongo.saveBook(Object.assign(this.BINN_book, {cycleId}), db_suffix);
				Mongo.saveBook(Object.assign(this.BITT_book, {cycleId}), db_suffix);
				Mongo.saveBook(Object.assign(this.BFX_book, {cycleId}), db_suffix);
				process.stdout.write('V');
			}
			
		}, 200);
		
	}
}

(async () => {
	AjaxCombinator.init();
})();
