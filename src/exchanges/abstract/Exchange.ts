import { Observable } from 'rxjs';
import {Book} from '../../types/interfaces/Book';
import {OrderStatuses} from '../../types/enums/OrderStatuses';
import {Markets} from '../../types/enums/Markets';
import {Entity} from '../../entites/abstract/Entity';
import {IOrder, Order} from '../../entites/Order';
import {Exchanges} from '../../types/enums/Exchanges';
import {API_CancelOrder_Reply} from '../types/API_CancelOrder_Reply';
import {API_Open_Order} from '../types/API_Open_Order';

export abstract class Exchange extends Entity<Exchange>
{
	//region model
	
	static collection_name = 'exchanges';
	       collection_name = 'exchanges';
	
	static list_key = `all-exchanges`;
	       list_key = `all-exchanges`;
	
	//endregion
	
	//region members
	static fee;
	       fee;
	       exName: Exchanges;
	       isRunning: boolean = false;
	
	static running_instances: { [id: number]: { exName: string, instance: Exchange, id: number } } = {};
	
	book_length = 10;
	
	buy_power_ratio: number = 1;
	
	static isFeeSameCurrency = true;
	       isFeeSameCurrency = true;
	
	//endregion
	
	//region orders
	
	get orders(): Order[] {
		return Order.getAllFromCache<Order>()
		            .filter(order => order.exchange_id === this.id);
	}
	
	//endregion
	
	constructor(exName: Exchanges, isNewInstance?: boolean) {
		super('exchanges', true);
		Exchange.running_instances = Exchange.running_instances || {};
		if (isNewInstance)
		{
			this.exName = exName;
		}
	}
	
	allMarkets(): Markets[] {
		return [
			Markets.USDT_BTC,
			Markets.USDT_ETH,
			Markets.USDT_LTC,
			Markets.USDT_NEO,
			Markets.USDT_BCC,
			
			Markets.BTC_ETH,
			Markets.BTC_LTC,
			Markets.BTC_BCC,
			Markets.BTC_XRP,
			Markets.BTC_XVG,
			Markets.BTC_TRX,
		];
	}
	
	async run() {
		await this._run();
		this.isRunning = true;
		this.save();
	}
	
	async stop() {
		this._stop();
		this.isRunning = false;
		this.save();
	}
	
	getOpenOrders(market: Markets): Promise<API_Open_Order[]> {
		return this._getOpenOrders(market);
	}
	
	async execOrder(order: Order): Promise<Order> {
		
		await this._execOrder(order);
		if (order.status === OrderStatuses.filled)
		{
			await order.transaction.tryFinish();
		}
		return order;
	}
	
	cancelAllOrders() {}
	
	cancelOrder(order: Order): Promise<API_CancelOrder_Reply> {
		return this._cancelOrder(order);
	}
	
	// // ABSTRACT
	
	protected abstract async _run(): Promise<any>
	
	protected abstract _stop(): void
	
	protected abstract _getOpenOrders(market: Markets): Promise<API_Open_Order[]>
	
	protected abstract _execOrder(order: Order): Promise<Order>
	
	protected abstract _cancelOrder(order: IOrder): Promise<API_CancelOrder_Reply>
	
	abstract get activeMarkets(): Markets[];
	
	abstract marketToString(market: Markets): string
	
	abstract stringToMarket(market_str: string): Markets
	
	abstract getBook$(market: Markets): Observable<Book>
	
	initGetBookSnapshot(): Promise<void> {return Promise.resolve();}
	
	abstract getBookSnapshot(market: Markets, cycleId: number): Promise<Book>
	
	abstract getBookAjax(market: Markets, cycleId: number): Observable<Book>
	
	abstract getBalances(): Promise<any>
	
	abstract getMoneyBalance(balance: number): number
	
	abstract getCommodityBalance(balance: number): number
	
	abstract getMaxSellAmount(balance: number): number
	
	abstract getMaxBuyAmount(balance_$: number, buy_price): number
	
}


	



