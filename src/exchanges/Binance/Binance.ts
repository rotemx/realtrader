import {Observable} from 'rxjs';
import {Book, BookEntry} from '../../types/interfaces/Book';
import {Log} from '../../utils/Log';
import {Order} from '../../entites/Order';
import {Exchange} from '../abstract/Exchange';
import {Exchanges} from '../../types/enums/Exchanges';
import {Markets} from '../../types/enums/Markets';
import {Redis} from '../../DB/Redis';
import {BINANCE_API_ExecOrderReply} from './types/BINANCE_API_ExecOrderReply';
import {API_CancelOrder_Reply} from '../types/API_CancelOrder_Reply';
import {BINANCE_API_CancelOrder_Reply} from './types/BINANCE_API_CancelOrder_Reply';
import {API_Open_Order} from '../types/API_Open_Order';
import {BINANCE_API_OpenOrder} from './types/BINANCE_API_OpenOrder';
import * as node_binance_api from 'node-binance-api';
import {EXCHANGE_CONFIG} from '../EXCHANGE_CONFIG';
import {OrderStatuses} from '../../types/enums/OrderStatuses';
import {API_Get_Balances_Reply} from '../types/API_Get_Balances_Reply';
import {fix8} from '../../utils/fix';
import {getShortUUID} from '../../utils/getShortUUID';
import {assertBookData} from '../../utils/AssertBookData';

const INTERVAL = 50;

node_binance_api.options({
	'APIKEY'     : EXCHANGE_CONFIG.binance.key,
	'APISECRET'  : EXCHANGE_CONFIG.binance.secret,
	useServerTime: true,
	reconnect    : true,
	verbose      : true,
	log          : log => {
		Log(log);
	},
});

export class Binance extends Exchange
{
	
	//region Model
	static key = `exchanges:0`;
	       key = `exchanges:0`;
	
	static id = 0;
	       id = 0;
	
	static fee = 0.0005;
	       fee = 0.0005;
	
	static Model = Binance;
	       Model = Binance;
	
	static collection_name = 'exchanges';
	       collection_name = 'exchanges';
	
	static list_key = `all-exchanges`;
	       list_key = `all-exchanges`;
	
	static exName = Exchanges.binance;
	       exName = Exchanges.binance;
	
	static instances: Binance[] = [];
	
	static isFeeSameCurrency = false;
	       isFeeSameCurrency = false;
	
	//endregion
	
	constructor(isNewInstance?: boolean) {
		super(Exchanges.binance, isNewInstance);
		this.Model           = Binance;
		this.Model.instances = this.Model.instances || [];
	}
	
	onExecutionUpdate(data) {
		Log(data, 'binance/onExecutionUpdate', 'binance_onExecutionUpdate');
		
		const
			{X: order_status, p: price, q: quantity, z: filledQty, n: fee, S: side, o: orderType, i: orderId, c: clientOrderId, l: last_exec_amount, L: last_exec_price} = data;
		
		let order = this.orders.find(o => o.orderId == orderId) || this.orders.find(o => o.clientOrderId == clientOrderId);
		if (!order)
		{
			Log(`order ${orderId}/${clientOrderId} not found !`, 'binance/onExecutionUpdate', 'binance');
			Log(this.orders, 'this.orders', 'binance');
			Log(data, 'data', 'binance');
			return;
		}
		
		order.aggregateExecutions({
			price : +last_exec_price,
			amount: +last_exec_amount,
			fee   : +fee,
			isOpen: !["REJECTED", "EXPIRED", "FILLED"].includes(order_status), //todo: does not support IOC partial_filled
		});
		
		switch (order_status)
		{
			case "REJECTED":
				Log(data, "binance:onExecutionUpdate:case:REJECTED", 'binance');
				order.setStatus(OrderStatuses.rejected);
				return;
			case "order_status === 'FILLED'":
				Log(data, "binance:onExecutionUpdate:case:CANCELED", 'binance');
				order.setStatus(OrderStatuses.cancelled);
				return;
			
			case "NEW":
				Log(data, "binance:onExecutionUpdate:case:NEW", 'binance');
				break;
			case "EXPIRED":
				Log(data, "binance:onExecutionUpdate:case:EXPIRED", 'binance');
				break;
			
			case "PARTIALLY_FILLED":
				Log(data, "BINANCE Order status PARTIALLY_FILLED!", 'binance');
				if (order.status !== OrderStatuses.filled)
				{
					order.setStatus(OrderStatuses.partial);
				}
				break;
			
			case "FILLED":
				Log(data, "BINANCE Order status FILLED!", 'binance');
				order.setStatus(OrderStatuses.filled);
				break;
			
			default:
				Log(data, `BINANCE Order status ${order.id} in ${this.exName} WTF?!! NO ORDER STATUS ?!?! WTF!!`, 'binance');
				break;
		}
		
		order.transaction.tryFinish();
	}
	
	protected async _run() {
		try
		{
			this.balances = await this.getBalances();
		} catch (e)
		{
			Log(e, 'BINANCE:getBalances');
		}
		
		//		node_binance_api.websockets.userData(() => {}, this.onExecutionUpdate.bind(this));
	}
	
	protected _stop() {
	}
	
	get activeMarkets() {
		return [
			Markets.USDT_BTC,
			Markets.USDT_ETH,
			Markets.USDT_LTC,
			Markets.USDT_NEO,
			Markets.USDT_BCC,
			Markets.BTC_ETH,
			Markets.BTC_LTC,
			Markets.BTC_BCC,
			Markets.BTC_XRP,
			Markets.BTC_XVG,
			Markets.BTC_TRX,
		];
	}
	
	marketToString(market: Markets): string {
		return {
			[Markets.USDT_BTC]: 'BTCUSDT',
			[Markets.USDT_ETH]: 'ETHUSDT',
			[Markets.USDT_LTC]: 'LTCUSDT',
			[Markets.USDT_NEO]: 'NEOUSDT',
			[Markets.USDT_BCC]: 'BCCUSDT',
			
			[Markets.BTC_ETH]: 'ETHBTC',
			[Markets.BTC_LTC]: 'LTCBTC',
			[Markets.BTC_BCC]: 'BCCBTC',
			[Markets.BTC_XRP]: 'XRPBTC',
			[Markets.BTC_XVG]: 'XVGBTC',
			[Markets.BTC_TRX]: 'TRXBTC',
		}[market];
	}
	
	stringToMarket(market_str: string): Markets {
		return {
			'BTCUSDT': Markets.USDT_BTC,
			'NEOUSDT': Markets.USDT_NEO,
			'ETHUSDT': Markets.USDT_ETH,
			'LTCUSDT': Markets.USDT_LTC,
			'BCCUSDT': Markets.USDT_BCC,
			
			'ETHBTC': Markets.BTC_ETH,
			'LTCBTC': Markets.BTC_LTC,
			'BCCBTC': Markets.BTC_BCC,
			'XRPBTC': Markets.BTC_XRP,
			'XVGBTC': Markets.BTC_XVG,
			'TRXBTC': Markets.BTC_TRX,
			
		}[market_str];
	}
	
	static async load(): Promise<Binance> {
		return Redis.loadEntity(Binance, this.id)
		            .then(instance => {
			            let ex: Binance;
			
			            if (instance)
			            {
				            ex = instance;
			            }
			            else
			            {
				            ex           = new Binance(true); //null for newly created exchange instance
				            ex.isRunning = true;
				            ex.save();
				
			            }
			            Exchange.running_instances[this.id] = {exName: ex.exName, instance: ex, id: ex.id};
			            return ex;
		            });
	}
	
	getBook$(market: Markets): Observable<Book> {
		
		const market_str = this.marketToString(market);
		
		return new Observable<Book>(obs => {
			let conn = node_binance_api.websockets.depthCache(market_str, (symbolr, depth) => {
				if (!depth || !depth.asks || !depth.bids || !Object.keys(depth.bids).length || !Object.keys(depth.asks).length)
				{
					Log('ERR Binance NO data ');
					return;
				}
				
				const
					{e: eventType, E: eventTime, s: symbol, u: updateId, b: bidDepth, a: askDepth} = depth,
					buys                                                                           = node_binance_api.sortAsks(depth.asks),
					sells                                                                          = node_binance_api.sortBids(depth.bids),
					
					book: Book                                                                     = {
						market   : market,
						fee      : Binance.fee,
						exName   : Binance.exName,
						timestamp: Date.now(),
						buy      : Object.keys(buys).map((price) => ({price: parseFloat(price), amount: buys[price]})).sort((a, b) => a.price < b.price ? -1 : 1),
						sell     : Object.keys(sells).map((price) => ({price: parseFloat(price), amount: sells[price]})).sort((a, b) => a.price > b.price ? -1 : 1),
						source   : 'socket',
					};
				
				if (book.buy[0].price <= book.sell[0].price)
				{
					Log(`WTF ! binance buy ${book.buy[0].price} < binance_sell ${book.sell[0].price}`, 'Binance WTF!');
					return;
				}
				
				obs.next(book);
			});
			
			return () => {
				//todo unsubscribe from websocket
			};
		}).shareReplay(1);
	}
	
	getBookAjax$(market: Markets = Markets.USDT_BTC): Observable<Book> {
		return Observable
			.interval(INTERVAL)
			.mergeMap(() => {
				return this.getBookAjax(market)
				           .catch(error => {
					           return Observable.of(null);
				           });
			})
			.filter(res => !!res)
			.shareReplay(1);
	}
	
	getBookAjax(market: Markets, cycleId?: number): Observable<Book> {
		
		const
			market_str = this.marketToString(market);
		return new Observable(obs => {
			node_binance_api.depth(market_str, (error, depth, symbol) => {
				
				if (error || !depth || !depth.bids || !depth.asks)
				{
					obs.error(error || 'No Binance Depth Data Received');
					return;
				}
				
				const
					buyEntry: BookEntry[]  = Object.keys(depth.asks)
					                               .slice(0, this.book_length)
					                               .map(key => ({price: parseFloat(key), amount: depth.asks[key]})),
					sellEntry: BookEntry[] = Object.keys(depth.bids)
					                               .slice(0, this.book_length)
					                               .map(key => ({price: parseFloat(key), amount: depth.bids[key]})),
					
					book: Book             = {
						market      : market,
						fee         : Binance.fee,
						timestamp   : cycleId || Date.now(),
						cycleId     : cycleId || Date.now(),
						exName      : Binance.exName,
						buy         : buyEntry,
						sell        : sellEntry,
						source      : 'ajax',
						lastUpdateId: depth.lastUpdateId,
					};
				
				if (!assertBookData(book))
				{
					return
				}
				
				if (book.buy[0].price <= book.sell[0].price) //WTF check
				{
					Log(`WTF ! binance buy ${book.buy[0].price} < binance_sell ${book.sell[0].price}`, 'Binance AJAX WTF!');
					obs.complete();
					return;
				}
				
				obs.next(book);
				obs.complete();
			});
			return () => {};
		});
	}
	
	getBookSnapshot(market: Markets, cycleId: number) {
		return this.getBookAjax(market, cycleId).toPromise();
	}
	
	protected _execOrder(order: Order): Promise<Order> {
		
		let prom = new Promise<Order>(((resolve, reject) => {
			Log(`[>] Executing ${order.type} order ${order.id}: amount ${order.amount} at EXP price ${order.EXP_price} (SAFE ${order.SAFE_price})`, 'Binance');
			
			const clientOrderId = getShortUUID();
			
			order.set({clientOrderId});
			
			node_binance_api[order.type](this.marketToString(order.market), order.amount, order.SAFE_price, {type: 'LIMIT', newClientOrderId: clientOrderId}, (error, res: BINANCE_API_ExecOrderReply) => {
				if (error)
				{
					Log(error, 'Binance/_execOrder error');
					reject(error);
					return;
				}
				Log(res, `Binance/_execOrder res`, 'binance');
				order.set({
					orderId    : String(res.orderId),
					placed_time: Date.now(),
				});
				resolve(order);
			});
		}));
		prom.catch(err => {
			Log(err.body, 'binance/_execOrder error body');
		});
		return prom;
	}
	
	protected _getOpenOrders(market: Markets): Promise<API_Open_Order[]> {
		
		return new Promise<any>((resolve, reject) => {
			
			node_binance_api.openOrders(this.marketToString(market), (error, open_orders: BINANCE_API_OpenOrder[], symbol) => {
				if (error)
				{
					return reject(error);
				}
				let api_order_orders: API_Open_Order[] = open_orders.map(o => Binance.binanceOrderToAPIOpenOrder(o));
				resolve(api_order_orders);
			});
		});
	}
	
	private static binanceOrderToAPIOpenOrder(bin_order: BINANCE_API_OpenOrder): API_Open_Order {
		
		const
			total_price                    = +bin_order.price * +bin_order.origQty,
			remaining_amount               = +bin_order.origQty - +bin_order.executedQty,
			api_open_order: API_Open_Order = {
				remaining_amount: remaining_amount,
				orderId         : String(bin_order.orderId),
				isClosed        : bin_order.executedQty === bin_order.origQty,
				price_per_unit  : +bin_order.price,
				total_cost      : total_price,
				total_amount    : +bin_order.origQty,
				filled_amount   : +bin_order.origQty - +bin_order.executedQty,
				fee_paid        : 0,  //not available in binance
			};
		return api_open_order;
	}
	
	protected _cancelOrder(order: Order): Promise<API_CancelOrder_Reply> {
		
		return new Promise<API_CancelOrder_Reply>((resolve, reject) => {
			node_binance_api.cancel(this.marketToString(order.market), order.orderId, (error, res: BINANCE_API_CancelOrder_Reply, symbol) => {
				if (error) reject(error);
				
				resolve({
					timestamp: Date.now(),
				});
			});
		});
	}
	
	balances: API_Get_Balances_Reply = {};
	
	getBalances(): Promise<API_Get_Balances_Reply> {
		
		const prom = new Promise<API_Get_Balances_Reply>((resolve, reject) => {
			node_binance_api.balance((error, balances) => {
				if (error)
				{
					reject(error);
					return;
				}
				let reply: API_Get_Balances_Reply = {};
				
				Object.keys(balances).forEach(key => {
					reply[key] = {
						total    : parseFloat(balances[key].available) + parseFloat(balances[key].onOrder),
						available: parseFloat(balances[key].available),
						reserved : parseFloat(balances[key].onOrder),
					};
				});
				resolve(reply);
			});
		});
		
		prom.catch(err => {
			Log(err, 'Binance/getBalance');
		});
		return prom;
	}
	
	async getOrderStatus(id: string) {
		
		node_binance_api.orderStatus(this.marketToString(Markets.USDT_BTC), id, (err, status, symbol) => {
			Log(status, 'order status');
			
		});
		return null;
	}
	
	getMoneyBalance(balance: number): number {
		return balance;
	}
	
	getCommodityBalance(balance: number): number {
		return balance;
	}
	
	getMaxSellAmount(balance_C: number): number {
		if (this.balances['BNB'].available < balance_C * 0.0005)
		{
			Log(`Not enough BNB to pay SELL fees in Binance!`, 'Binance');
		}
		return balance_C;
	}
	
	getMaxBuyAmount(balance_$: number, buy_price: number): number {
		if (this.balances['BNB'].available < balance_$ * 0.0005)
		{
			Log(`Not enough BNB to pay BUY fees in Binance!`, 'Binance');
		}
		
		return fix8(balance_$ * buy_price);
	}
	
}



























