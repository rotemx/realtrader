export interface BINANCE_API_OpenOrder
{
	"symbol": "BTCUSDT" | "BTCETH"
	"orderId": number
	"clientOrderId": string
	"price": string
	"origQty": string
	"executedQty": string
	"status": "NEW" | "CANCELED" | "REPLACED" | "REJECTED" | "TRADE" | "EXPIRED"  | "FILLED"
	"timeInForce": "GTC" | "IOC" | "FOK"
	"type": "LIMIT" | "MARKET"  ///?? not sure about market
	"side": "SELL" | "BUY"
	"stopPrice": string,
	"icebergQty": string,
	"time": number,
	"isWorking": boolean
	
}

/*
{
	"symbol": "BTCUSDT",
	"orderId": 69113028,
	"clientOrderId": "ML3KYsLgfGZ2n4N4MpPEeQ",
	"price": "20000.00000000",
	"origQty": "0.00200000",
	"executedQty": "0.00000000",
	"status": "NEW",
	"timeInForce": "GTC",
	"type": "LIMIT",
	"side": "SELL",
	"stopPrice": "0.00000000",
	"icebergQty": "0.00000000",
	"time": 1521128274675,
	"isWorking": true
}*/

