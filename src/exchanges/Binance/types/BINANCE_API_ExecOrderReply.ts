export interface BINANCE_API_ExecOrderReply
{
	symbol: string,
	orderId: number,
	clientOrderId: string,
	transactTime: number,
	price: number,
	origQty: number,
	executedQty: number,
	status:  "NEW" | "FILLED" | "PARTIALLY_FILLED" | "CANCELED" | "PENDING_CANCEL" | "REJECTED" | "EXPIRED",
	timeInForce: "GTC" | "IOC" | "FOK",
	type: "LIMIT" |"MARKET" | "STOP_LOSS" | "STOP_LOSS_LIMIT" | "TAKE_PROFIT" | "TAKE_PROFIT_LIMIT" | "LIMIT_MAKER",
	side: "SELL" | "BUY"
}

/*
{
	"symbol": "BTCUSDT",
	"orderId": 66768358,
	"clientOrderId": "DvbdcW3ETzuc33xuUtD6oc",
	"transactTime": 1520894952231,
	"price": "2000.00000000",
	"origQty": "0.01000000",
	"executedQty": "0.01000000",
	"status": "FILLED",
	"timeInForce": "GTC",
	"type": "LIMIT",
	"side": "SELL"

}
}*/
