const example = {
	"e": "executionReport",
	"E": 1521122865400,
	"s": "BTCUSDT",
	"c": "web_2b4987ab023641aa9ecaf7ac6ccbe79c",
	"S": "SELL",
	"o": "LIMIT",
	"f": "GTC",
	"q": "0.00200000",
	"p": "8300.98000000",
	"P": "0.00000000",
	"F": "0.00000000",
	"g": -1,
	"C": "null",
	"x": "NEW",
	"X": "NEW",
	"r": "NONE",
	"i": 69042581,
	"l": "0.00000000",
	"z": "0.00000000",
	"L": "0.00000000",
	"n": "0",
	"N": null,
	"T": 1521122865397,
	"t": -1,
	"I": 165604921,
	"w": true,
	"m": false,
	"M": false,
	"O": -1,
	"Z": "-0.00000001"
};

//Execution Update :
/*
{
	"e": "executionReport",        // Event type
	"E": 1499405658658,            // Event time
	"s": "ETHBTC",                 // Symbol
	"c": "mUvoqJxFIILMdfAW5iGSOW", // Client order ID
	"S": "BUY",                    // Side
	"o": "LIMIT",                  // Order type
	"f": "GTC",                    // Time in force
	"q": "1.00000000",             // Order quantity
	"p": "0.10264410",             // Order price
	"P": "0.00000000",             // Stop price
	"F": "0.00000000",             // Iceberg quantity
	"g": -1,                       // Ignore
	"C": "null",                   // Original client order ID; This is the ID of the order being canceled
	"x": "NEW",                    // Current execution type
	"X": "NEW",                    // Current order status
	"r": "NONE",                   // Order reject reason; will be an error code.
	"i": 4293153,                  // Order ID
	"l": "0.00000000",             // Last executed quantity
	"z": "0.00000000",             // Cumulative filled quantity
	"L": "0.00000000",             // Last executed price
	"n": "0",                      // Commission amount
	"N": null,                     // Commission asset
	"T": 1499405658657,            // Transaction time
	"t": -1,                       // Trade ID
	"I": 8641984,                  // Ignore
	"w": true,                     // Is the order working? Stops will have
	"m": false,                    // Is this trade the maker side?
	"M": false                     // Ignore
}
*/
