export interface BINANCE_API_CancelOrder_Reply
{
	symbol: 'BTCUSDT',
	origClientOrderId: string,
	orderId: number,
	clientOrderId : string
}

/*
{
	"symbol": "BTCUSDT",
	"origClientOrderId": "FYJe312TnBRTdQMsp8v007",
	"orderId": 66800224,
	"clientOrderId": "iRsdmeMxG3yVrPrz2uy0pm"
}*/
