import {Redis} from '../DB/Redis';

export class State
{
	private static _nextId = 100;
	
	static get nextId() {
		Redis.incr('nextId');
		return this._nextId++;
		
	}
	
	static async initIds() {
		
		if (await Redis.get('isEnabled'))
		{
			this._nextId = await Redis.get('nextId');
		}
		else
		{
			await Redis.set('isEnabled', 1);
			await Redis.set('nextId', 100);
		}
		
	}
	
	static async init(host?:string) {
		await Redis.init(host);
		await this.initIds();
		
	}
}
