import {Machine} from '../../entites/Machine';
import {checkCycleRecursive} from './CheckCycleRecursive';
import {Transaction} from '../../entites/Transaction';
import {Cycle} from '../../entites/Cycle';
import {RealTrader} from '../../RealTrader';

export function checkCycle(machine: Machine, cycle: Cycle): Transaction[] {
	
	let
		transactions: Transaction[] = [];
	
	const
		bookA               = cycle.bookA,
		bookB               = cycle.bookB,
		
		accountA            = machine.accountA,
		accountB            = machine.accountB,
		
		Money_balance_A     = accountA.getMoneyBalance(),
		Commodity_balance_A = accountA.getCommodityBalance(),
		
		Money_balance_B     = accountB.getMoneyBalance(),
		Commodity_balance_B = accountB.getCommodityBalance();
	
	//check AB = sell in A, buy in B
	transactions = transactions.concat(checkCycleRecursive(
		{
			sell_book: bookA,
			buy_book : bookB,
			
			sell_account: accountA,
			buy_account : accountB,
			
			sell_balance_C: Commodity_balance_A,
			buy_balance_$ : Money_balance_B,
			
			profit_threshold: machine.AB_profit_threshold,
			machine         : machine,
			cycle           : cycle,
		},
	));
	
	if (!transactions.length)
	{
		// check BA = sell in B, buy in A
		transactions = transactions.concat(checkCycleRecursive(
			{
				sell_book: bookB,
				buy_book : bookA,
				
				sell_account: accountB,
				buy_account : accountA,
				
				sell_balance_C: Commodity_balance_B,
				buy_balance_$ : Money_balance_A,
				
				profit_threshold: machine.BA_profit_threshold,
				machine         : machine,
				cycle           : cycle,
			},
		))
		;
	}

	RealTrader.last_checkCycle_time = Date.now();
	return transactions;
}

