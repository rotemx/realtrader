import {BookEntry} from '../../types/interfaces/Book';
import * as _ from 'lodash';
import {GLOBALS} from '../../GLOBALS';

export function reduceEntry(book: BookEntry[], tx_amount) {
	let new_book = _.cloneDeep(book);
	new_book[0].amount -= tx_amount;
	
	if (new_book[0].amount <= GLOBALS.MIN_AMOUNT)
	{
		new_book = new_book.slice(1);
	}
	return new_book;
}
