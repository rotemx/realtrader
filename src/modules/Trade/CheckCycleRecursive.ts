import {Book} from '../../types/interfaces/Book';
import {Transaction} from '../../entites/Transaction';
import {Order} from '../../entites/Order';
import {OrderTypes} from '../../types/enums/OrderTypes';
import {reduceEntry} from './ReduceEntry';
import {displayTicker} from './DisplayTicker';
import {Log} from '../../utils/Log';
import {fix8} from '../../utils/fix';
import {Account} from '../../entites/Account';
import {Machine} from '../../entites/Machine';
import {Cycle} from '../../entites/Cycle';
import {playChaChing} from '../../utils/playChaChing';
import {GLOBALS} from '../../GLOBALS';

export function checkCycleRecursive({
	                                    sell_book,
	                                    buy_book,
	                                    sell_account,
	                                    buy_account,
	                                    sell_balance_C,
	                                    buy_balance_$,
	                                    profit_threshold,
	                                    machine,
	                                    cycle,
                                    }: {
	sell_book: Book,
	buy_book: Book,
	sell_account: Account,
	buy_account: Account,
	sell_balance_C: number,
	buy_balance_$: number,
	profit_threshold: number,
	machine: Machine,
	cycle: Cycle
	
}) {
	const
		transactions: Transaction[] = [];
	
	if (!buy_book.buy.length || !sell_book.sell.length)
	{
		return [];
	}
	
	const
		
		is_LOCAL_buy                     = buy_account.exchange_id === machine.local_exchange_id,
		local_book_entry                 = is_LOCAL_buy ? buy_book.buy[0] : sell_book.sell[0],
		best_buy                         = buy_book.buy[0],
		best_sell                        = sell_book.sell[0],
		
		//		BUY PRICE
		EXP_buy_price                    = best_buy.price,
		SAFE_buy_price                   = fix8(EXP_buy_price + GLOBALS.SAFE_MARGIN),
		
		//		SELL PRICE
		EXP_sell_price                   = best_sell.price,
		SAFE_sell_price                  = fix8(EXP_sell_price - GLOBALS.SAFE_MARGIN),
		
		//		AMOUNT
		MAX_buy_amount                   = fix8(Math.max(0, buy_balance_$ - GLOBALS.BALANCE_SAFE_GAP_$) * buy_account.exchange.buy_power_ratio / SAFE_buy_price),
		
		MAX_sell_amount                  = Math.max(0, sell_balance_C - GLOBALS.BALANCE_SAFE_GAP_C),
		
		LOCAL_balance_possible_amount    = is_LOCAL_buy ? MAX_buy_amount : MAX_sell_amount,
		
		tx_amount                        = fix8(Math.min(local_book_entry.amount * GLOBALS.AMOUNT_FACTOR, LOCAL_balance_possible_amount)),
		
		//		SELL FEE
		EXP_sell_fee_$                   = fix8(tx_amount * EXP_sell_price * (sell_account.exchange.fee)),
		SAFE_sell_fee_$                  = fix8(tx_amount * SAFE_sell_price * (sell_account.exchange.fee)),
		
		//		BUY FEE
		EXP_buy_fee_$                    = fix8(tx_amount * EXP_buy_price * (buy_account.exchange.fee)),
		SAFE_buy_fee_$                   = fix8(tx_amount * SAFE_buy_price * (buy_account.exchange.fee)),
		
		//		INCOME
		EXP_tx_income_$                  = fix8(tx_amount * EXP_sell_price),
		SAFE_tx_income_$                 = fix8(tx_amount * SAFE_sell_price),
		
		// including fee
		EXP_tx_income_for_balance_$      = fix8(tx_amount * EXP_sell_price * (sell_account.exchange.isFeeSameCurrency ? (1 - sell_account.exchange.fee) : 1)),
		SAFE_tx_income_for_balance_$     = fix8(tx_amount * SAFE_sell_price * (sell_account.exchange.isFeeSameCurrency ? (1 - sell_account.exchange.fee) : 1)),
		
		// for profit calc
		EXP_tx_income_for_profit_calc_$  = fix8(tx_amount * EXP_sell_price - EXP_sell_fee_$),
		SAFE_tx_income_for_profit_calc_$ = fix8(tx_amount * SAFE_sell_price - SAFE_sell_fee_$),
		
		//		COST
		EXP_tx_cost_$                    = fix8(tx_amount * EXP_buy_price),
		SAFE_tx_cost_$                   = fix8(tx_amount * SAFE_buy_price),
		
		// including fee
		EXP_tx_cost_for_balance_$        = fix8(tx_amount * EXP_buy_price * (buy_account.exchange.isFeeSameCurrency ? (1 + buy_account.exchange.fee) : 1)),
		SAFE_tx_cost_for_balance_$       = fix8(tx_amount * SAFE_buy_price * (buy_account.exchange.isFeeSameCurrency ? (1 + buy_account.exchange.fee) : 1)),
		
		// for profit calc
		EXP_tx_cost_for_profit_calc_$    = fix8(tx_amount * EXP_buy_price * (1 + buy_account.exchange.fee)),
		SAFE_tx_cost_for_profit_calc_$   = fix8(tx_amount * SAFE_buy_price * (1 + buy_account.exchange.fee)),
		
		//		PROFIT
		EXP_tx_profit_$                  = fix8(EXP_tx_income_for_profit_calc_$ - EXP_tx_cost_for_profit_calc_$),
		SAFE_tx_profit_$                 = fix8(SAFE_tx_income_for_profit_calc_$ - SAFE_tx_cost_for_profit_calc_$),
		
		EXP_profit_per_Commodity_unit    = fix8(tx_amount > 0 ? EXP_tx_profit_$ / tx_amount : 0),
		SAFE_profit_per_Commodity_unit   = fix8(tx_amount > 0 ? SAFE_tx_profit_$ / tx_amount : 0);
	
	//Dynamic Balance Protocol
	
	//	if (tx_amount > 0)
	{
		displayTicker(EXP_profit_per_Commodity_unit, buy_book, sell_book, cycle.cycleId);
	}
	
	//region WTF checks
	let wrong = false;
	if (is_LOCAL_buy && buy_balance_$ < 0)
	{
		Log(`buy_balance_$ ${buy_balance_$} is negative !`, 'WTF checkCycleRecursive');
		wrong = true;
	}
	if (!is_LOCAL_buy && sell_balance_C < 0)
	{
		Log(`sell_balance_C ${sell_balance_C} is negative !`, 'WTF checkCycleRecursive');
		wrong = true;
	}
	if (tx_amount < 0)
	{
		Log(`tx_amount ${tx_amount} is negative !`, 'WTF checkCycleRecursive');
		wrong = true;
	}
	if (is_LOCAL_buy && SAFE_buy_price * tx_amount > buy_balance_$)
	{
		Log(`BUY at ${buy_account.exchange.exName}: Not enough funds ?! need ${SAFE_buy_price * tx_amount}, have ${buy_balance_$}`);
		wrong = true;
		
	}
	if (!is_LOCAL_buy && tx_amount > sell_balance_C)
	{
		Log(`SELL at ${sell_account.exchange.exName}: Not enough Commodity !! need ${tx_amount}, have ${sell_balance_C}`);
		wrong = true;
	}
	if (wrong)
	{
		Log(`cancelling transaction.. something's wrong`);
		return [];
	}
	//endregion
	
		if (SAFE_profit_per_Commodity_unit >= 0 && tx_amount > 0.01)
		{
			Log(` === possible balancing trade found:
			 SAFE_profit_per_Commodity_unit :   ${SAFE_profit_per_Commodity_unit}
			 EXP_profit_per_Commodity_unit  :   ${EXP_profit_per_Commodity_unit}
			 SAFE_sell_price                :   ${SAFE_sell_price}
			 SAFE_buy_price                 :   ${SAFE_buy_price}
			 tx_amount:                     :   ${tx_amount}
			 SAFE_tx_profit_$:              :   ${SAFE_tx_profit_$}
			 EXP_tx_profit_$ :              :   ${EXP_tx_profit_$}
			 
			 
				buy_account_balance_$  :          ${buy_balance_$}
				sell_account.balance_$ :          ${sell_account.balance_$}
				buy_account.balance_C  :          ${buy_account.balance_C}
				sell_account_balance_C  :         ${sell_balance_C}
			 `, `Almost`, 'almosts');
		}
	
	if (EXP_profit_per_Commodity_unit >= profit_threshold)
	{
		if (
			//				EXP_profit_per_Commodity_unit > 0 && EXP_tx_profit_$ < GLOBALS.MIN_TX_PROFIT) ||
			tx_amount < GLOBALS.MIN_AMOUNT ||
			EXP_tx_cost_$ <= GLOBALS.MIN_ORDER_VALUE ||
			SAFE_tx_income_$ <= GLOBALS.MIN_ORDER_VALUE)
		{
			if (tx_amount)
			{
				/*				Log(`=== threshold is Ok (${EXP_profit_per_Commodity_unit}) but other data is not:
				
									tx_amount < GLOBALS.MIN_AMOUNT                  ${tx_amount < GLOBALS.MIN_AMOUNT }
									EXP_tx_cost_$ <= GLOBALS.MIN_ORDER_VALUE        ${EXP_tx_cost_$ <= GLOBALS.MIN_ORDER_VALUE }
									SAFE_tx_income_$ <= GLOBALS.MIN_ORDER_VALUE     ${SAFE_tx_income_$ <= GLOBALS.MIN_ORDER_VALUE}
				
									EXP_tx_profit_$:                        ${EXP_tx_profit_$}
									tx_amount:                              ${tx_amount}
									EXP_tx_cost_for_balance_$:              ${EXP_tx_cost_for_balance_$}
									EXP_tx_income_for_profit_calc_$:        ${EXP_tx_income_for_profit_calc_$}
									EXP_profit_per_Commodity_unit:          ${EXP_profit_per_Commodity_unit}
									
									buy in:                                 ${buy_account.exchange.exName}
									sell in:                                ${sell_account.exchange.exName}
									
									Balances:
									buy_account_balance_$:                  ${buy_balance_$}
									sell_account_balance_C:                 ${sell_balance_C}
									
									`, 'recursive', 'almosts')*/
			}
			return [];
		}
		
		Log(`[============= V =============] BAMMM !!! TRANSACTION FOUND [============= V =============]`);
		
		let
			buy_order: Order,
			sell_order: Order;
		
		buy_order = new Order({
			// GENERAL
			type                         : OrderTypes.buy,
			amount                       : tx_amount,
			market                       : machine.market,
			Money_coin                   : buy_account.Money_coin,
			Commodity_coin               : buy_account.Commodity_coin,
			
			// PRICE
			EXP_price                    : EXP_buy_price,
			SAFE_price                   : SAFE_buy_price,
			
			// COST
			EXP_cost                     : EXP_tx_cost_$,
			SAFE_cost                    : SAFE_tx_cost_$,
			EXP_cost_for_balance_$       : EXP_tx_cost_for_balance_$,
			SAFE_cost_for_balance_$      : SAFE_tx_cost_for_balance_$,
			EXP_cost_for_profit_calc_$   : EXP_tx_cost_for_profit_calc_$,
			SAFE_cost_for_profit_calc_$  : SAFE_tx_cost_for_profit_calc_$,
			
			// INCOME
			EXP_income                   : tx_amount,
			SAFE_income                  : tx_amount,
			EXP_income_for_balance_$     : tx_amount,
			SAFE_income_for_balance_$    : tx_amount,
			EXP_income_for_profit_calc_$ : null,
			SAFE_income_for_profit_calc_$: null,
			
			// FEE
			EXP_fee                      : EXP_buy_fee_$,
			SAFE_fee                     : SAFE_buy_fee_$,
			
			// RELATIONS
			account_id                   : buy_account.id,
			machine_id                   : machine.id,
			exchange_id                  : buy_account.exchange_id,
		});
		
		sell_order      = new Order({
			// GENERAL
			type                         : OrderTypes.sell,
			amount                       : tx_amount,
			market                       : machine.market,
			Money_coin                   : sell_account.Money_coin,
			Commodity_coin               : sell_account.Commodity_coin,
			
			// PRICE
			EXP_price                    : EXP_sell_price,
			SAFE_price                   : SAFE_sell_price,
			
			// COST
			EXP_cost                     : tx_amount,
			SAFE_cost                    : tx_amount,
			EXP_cost_for_balance_$       : tx_amount,
			SAFE_cost_for_balance_$      : tx_amount,
			EXP_cost_for_profit_calc_$   : null,
			SAFE_cost_for_profit_calc_$  : null,
			
			// INCOME
			EXP_income                   : EXP_tx_income_$,
			SAFE_income                  : SAFE_tx_income_$,
			EXP_income_for_balance_$     : EXP_tx_income_for_balance_$,
			SAFE_income_for_balance_$    : SAFE_tx_income_for_balance_$,
			EXP_income_for_profit_calc_$ : EXP_tx_income_for_profit_calc_$,
			SAFE_income_for_profit_calc_$: SAFE_tx_income_for_profit_calc_$,
			
			// FEE
			EXP_fee                      : EXP_sell_fee_$,
			SAFE_fee                     : SAFE_sell_fee_$,
			
			// RELATIONS
			account_id                   : sell_account.id,
			machine_id                   : machine.id,
			exchange_id                  : sell_account.exchange_id,
		});
		const
			transaction = new Transaction({
				// GENERAL
				amount          : tx_amount,
				previous_tx_time: buy_account.last_tx_time,
				time_delta      : cycle.time_delta,
				time            : buy_book.timestamp,
				cycle_id        : cycle.cycleId,
				
				// PRICE
				EXP_buy_price  : EXP_buy_price,
				SAFE_buy_price : SAFE_buy_price,
				EXP_sell_price : EXP_sell_price,
				SAFE_sell_price: SAFE_sell_price,
				
				// COST
				EXP_cost_for_profit_calc_$ : EXP_tx_cost_for_profit_calc_$,
				SAFE_cost_for_profit_calc_$: SAFE_tx_cost_for_profit_calc_$,
				
				// INCOME
				EXP_income_for_profit_calc_$ : EXP_tx_income_for_profit_calc_$,
				SAFE_income_for_profit_calc_$: SAFE_tx_income_for_profit_calc_$,
				
				// PROFIT
				EXP_profit : EXP_tx_profit_$,
				SAFE_profit: SAFE_tx_profit_$,
				
				// PROFIT PER COMMODITY UNIT
				EXP_profit_per_Commodity_unit : EXP_profit_per_Commodity_unit,
				SAFE_profit_per_Commodity_unit: SAFE_profit_per_Commodity_unit,
				
				// RELATIONS
				sell_order_id  : sell_order.id,
				buy_order_id   : buy_order.id,
				machine_id     : machine.id,
				buy_account_id : buy_account.id,
				sell_account_id: sell_account.id,
			});
		
		Log(`
		==============  [V] Transaction ${transaction.id}   ===========================
		buy in:                             ${buy_book.exName}
		sell in:                            ${sell_book.exName}
		amount:                             ${tx_amount}
		EXP_profit_per_Commodity_unit:      ${EXP_profit_per_Commodity_unit}
		EXP_tx_profit:                      ${EXP_tx_profit_$}
		EXP_tx_income_for_profit_calc_$:    ${EXP_tx_income_for_profit_calc_$}                      ${machine.Money_coin}
		EXP_tx_cost_for_profit_calc_$:      ${EXP_tx_cost_for_profit_calc_$}                        ${machine.Money_coin}
		
		EXP_tx_income_for_balance_$:        ${EXP_tx_income_for_balance_$}                          ${machine.Money_coin}
		EXP_tx_cost_for_balance_$:          ${EXP_tx_cost_for_balance_$}                            ${machine.Money_coin}

		SAFE_tx_income_for_balance_$        ${SAFE_tx_income_for_balance_$}
		SAFE_tx_cost_for_balance_$:         ${SAFE_tx_cost_for_balance_$}
		
		cycle:                              ${cycle.cycleId}
		cycle.time_delta:                   ${cycle.time_delta}
		
		EXP_buy_price:                      ${EXP_buy_price}
		SAFE_buy_price:                     ${SAFE_buy_price}
		
		EXP_sell_price:                     ${EXP_sell_price}
		SAFE_sell_price:                    ${SAFE_sell_price}

		>> Balances (before tx):
		buy_balance_$:                      ${buy_balance_$}
		sell_balance_C:                     ${sell_balance_C}
		`, 'checkCycleRecursive');
		
		transactions.push(transaction);
		
		([buy_order, sell_order]).forEach(order => {
			order.set(
				{
					transaction_id: transaction.id,
				});
		});
		
		buy_account.set({last_tx_time: buy_book.timestamp});
		
		const
			new_buy_book  = {...buy_book, buy: reduceEntry(buy_book.buy, tx_amount)},
			new_sell_book = {...sell_book, sell: reduceEntry(sell_book.sell, tx_amount)},
			result        = checkCycleRecursive(
				{
					buy_book        : new_buy_book,
					sell_book       : new_sell_book,
					sell_account    : sell_account,
					buy_account     : buy_account,
					cycle           : cycle,
					machine         : machine,
					sell_balance_C  : fix8(sell_balance_C - tx_amount),
					buy_balance_$   : fix8(buy_balance_$ - SAFE_tx_cost_for_balance_$),
					profit_threshold: profit_threshold,
				},
			);
		
		playChaChing();
		return [...transactions, ...result];
	}
	return [];
}


























