import {Log} from '../../utils/Log';
import {RealTrader} from '../../RealTrader';
import {Book} from '../../types/interfaces/Book';

export function displayTicker(profit: number, buy_book: Book, sell_book: Book, cycleId: number) {
	
	const
		buyEx         = buy_book.exName,
		localBuy      = buy_book.exName === 'bittrex',
		
		BITT_book     = localBuy ? buy_book : sell_book,
		BINN_book     = localBuy ? sell_book : buy_book,
		
		msg           =
			`BITT [B ${BITT_book.buy[0].price.toFixed(2)} S ${BITT_book.sell[0].price.toFixed(2)}]\tBINN [B ${BINN_book.buy[0].price.toFixed(2)} S ${BINN_book.sell[0].price.toFixed(2)}]\t${buyEx === 'bittrex' ? ' BUY' : 'SELL'}`,
		show_only_new = true,
		is_new        = typeof RealTrader.last_price_ticker[buyEx] === 'undefined' || RealTrader.last_price_ticker[buyEx] !== profit;
	
	if (!show_only_new || is_new)
	{
		let tabs = sell_book.exName === 'binance' ? '' : '\t\t';
		if (isNaN(Number(profit.toFixed(2))))
		{
			Log(`NAN profit: ${profit}`);
		}
		Log(`${tabs}${profit.toFixed(2)}`, `${cycleId} \t ${msg }`, 'MAIN', false);
		RealTrader.last_price_ticker[buyEx] = profit;
	}
	
}
