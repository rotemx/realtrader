import Datastore = require('@google-cloud/datastore');
import {Mongo} from '../src/DB/Mongo';
import {Markets} from '../src/types/enums/Markets';
import {Exchanges} from '../src/types/enums/Exchanges';

const projectId = 'btc-data-1';

let datastore: Datastore;

async function testInit() {
	await Mongo.init();
	
	datastore = new Datastore({
		keyFilename: 'keys/Google_cloud_api_keys.json',
		projectId  : projectId,
	});
}

async function testSend(exName: string, market: Markets) {
	
	let
		limit      = 0,
		i          = 0,
		client     = Mongo.client,
		db         = client.db(exName),
		collection = db.collection(market),
		items      = collection.find({}),
		count      = await items.count()
	;
	
	if (limit)
	{
		items = items.limit(limit);
		count = limit;
	}
	
	i = 0;
	while (await items.hasNext())
	{
		let item = await items.next();
		delete item._id;
		
		if (i % (100) === 0)
		{
			console.log(`${i} records - ${(i / (count) * 100).toFixed(2)}%`);
		}
		
		const kind        = 'BOOK';
		const cycleId     = item.cycleId;
		const key         = datastore.key([kind, `${exName}_${market}_${cycleId}`]);
		const itemToStore = {
			key : key,
			data: item,
		};
		
		try
		{
			await datastore.save(itemToStore);
			//console.log(`Saved ${itemToStore.key.name}: ${itemToStore.data}`);
		} catch (err)
		{
			console.error(err);
		}
		i++;
	}
}

async function testReceive(exName: string, market: Markets) {
	
	const query = datastore.createQuery('BOOK');
	//.filter('done', '=', false)
	//.filter('priority', '>=', 4)
	//		.order('priority', {
	//			descending: true,
	//		});
	
	return datastore.runQuery(query).then(results => {
		
		const tasks = results[0];
		const info  = results[1];
		
		console.log('Tasks:', tasks.length);
		
	});
	
}

//RUN:

(async () => {
	
	console.log('init');
	await testInit();
	
	console.log('send');
	await testSend(Exchanges.binance, Markets.USDT_BTC);
	
//	console.log('receive');
//	await testReceive(Exchanges.binance, Markets.USDT_BTC);
	
	console.log('done');
	Mongo.close();
})();

