import {Redis} from '../src/DB/Redis'
import {Log} from '../src/utils/Log'

(async () => {
	await Redis.init()
	await Redis.flushAll()
	Log('DB has been flushed', 'flush-redis script')
	await Redis.end()
})()
