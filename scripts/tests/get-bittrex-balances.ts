import * as node_bittrex_api from 'node.bittrex.api';
import {Log} from '../../src/utils/Log';
import {BITTREX_API_Get_Balances_Reply} from '../../src/exchanges/Bittrex/types/BITTREX_API_Get_Balances_Reply';
import {fix8} from '../../src/utils/fix';
import {API_Get_Balances_Reply} from '../../src/exchanges/types/API_Get_Balances_Reply';
import {EXCHANGE_CONFIG} from '../../src/exchanges/EXCHANGE_CONFIG';


node_bittrex_api.options({
	apikey   : EXCHANGE_CONFIG.bittrex.key,
	apisecret: EXCHANGE_CONFIG.bittrex.secret,
	cleartext: false,
});

async function getBalances() {
	
	let prom = new Promise<API_Get_Balances_Reply>((resolve, reject) => {
		node_bittrex_api.getbalances(function (data: BITTREX_API_Get_Balances_Reply, err) {
			if (err)
			{
				reject(err);
				return;
			}
			if (data && data.success && data.result)
			{
				let reply: API_Get_Balances_Reply = {};
				
				data.result.forEach(entry => {
					reply[entry['Currency']] = {
						total    : entry.Balance,
						available: entry.Available,
						reserved : fix8(entry.Balance - entry.Available),
					};
				});
				resolve(reply);
			}
			reject('wrong data reply');
		});
	});
	prom.catch(err => {
		Log(err, 'Bittrex/getBalances');
	});
	return prom;
}

setInterval(() => {
	console.time('GET BITTREX BALANCES');
	
	getBalances().then(res => {
		console.timeEnd('GET BITTREX BALANCES');
		//	console.log(res, null, 2 2);
	});
	
}, 300);
