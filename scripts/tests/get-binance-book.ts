import {BookEntry, Book} from '../../src/types/interfaces/Book';
import {Log} from '../../src/utils/Log';
import * as node_binance_api from 'node-binance-api';

import {EXCHANGE_CONFIG} from '../../src/exchanges/EXCHANGE_CONFIG';
//import {Markets} from '../../src/types/enums/Markets';

node_binance_api.options({
	'APIKEY'     : EXCHANGE_CONFIG.binance.key,
	'APISECRET'  : EXCHANGE_CONFIG.binance.secret,
	useServerTime: true,
	reconnect    : false,
	verbose      : false,
	log          : log => {
		Log(log);
	},
});

async function getBalanceBook() {
	
	let prom = new Promise<Book>((resolve, reject) => {
		const market_srt = 'BTCUSDT';
		node_binance_api.depth(market_srt, (error, depth, symbol) => {
			
			if (error || !depth || !depth.bids || !depth.asks)
			{
				Log(error || 'No Binance Depth Data Received');
				return;
			}
			
			const
				buyArray: BookEntry[]  = Object.keys(depth.asks)
				                               .slice(0, 1)
				                               .map(key => ({price: parseFloat(key), amount: depth.asks[key]})),
				sellArray: BookEntry[] = Object.keys(depth.bids)
				                               .slice(0, 1)
				                               .map(key => ({price: parseFloat(key), amount: depth.bids[key]})),
				
				book: Book             = {
					market   : 'USDT_BTC',
					fee      : 0.0005,
					timestamp: Date.now(),
					cycleId  : Date.now(),
					exName   : 'binance',
					buy      : buyArray,
					sell     : sellArray,
					source   : 'ajax',
				};
			
			resolve(book);
			
		});
	});
	prom.catch(err => {
		Log(err, 'Bittrex/TEST-getBalanceBook');
	});
	return prom;
}

setInterval(() => {
	let begin = Date.now();
	
	getBalanceBook()
		.then((book) => {
			
			console.log(`${Date.now() - begin} ms       :    ${book.buy[0].amount}   @    ${book.buy[0].price}$`);
		});
	
}, 50);
