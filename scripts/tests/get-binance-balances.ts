import {Log} from '../../src/utils/Log';
import * as node_binance_api from 'node-binance-api';

import {API_Get_Balances_Reply} from '../../src/exchanges/types/API_Get_Balances_Reply';
import {EXCHANGE_CONFIG} from '../../src/exchanges/EXCHANGE_CONFIG';


node_binance_api.options({
	'APIKEY'     : EXCHANGE_CONFIG.binance.key,
	'APISECRET'  : EXCHANGE_CONFIG.binance.secret,
	useServerTime: true,
	reconnect    : false,
	verbose      : false,
	log          : log => {
		Log(log);
	},
});

async function getBalances() {
	
	let prom = new Promise<API_Get_Balances_Reply>((resolve, reject) => {
		node_binance_api.balance((error, balances) => {
			if (error)
			{
				reject(error);
				return;
			}
			let reply: API_Get_Balances_Reply = {};
			
			Object.keys(balances).forEach(key => {
				reply[key] = {
					total    : parseFloat(balances[key].available) + parseFloat(balances[key].onOrder),
					available: parseFloat(balances[key].available),
					reserved : parseFloat(balances[key].onOrder),
				};
			});
			resolve(reply);
		})

	});
	prom.catch(err => {
		Log(err, 'Bittrex/getBalances');
	});
	return prom;
}

setInterval(() => {
	console.time('GET BINANCE BALANCES');
	
	getBalances().then(res => {
		console.timeEnd('GET BINANCE BALANCES');
		//	console.log(res, null, 2 2);
	});
	
}, 300);
