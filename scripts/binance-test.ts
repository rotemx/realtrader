import {State} from '../src/modules/State';
import {Redis} from '../src/DB/Redis';
import {RealTrader} from '../src/RealTrader';
import {Binance} from '../src/exchanges/Binance/Binance';
import {Bittrex} from '../src/exchanges/Bittrex/Bittrex';

(async () => {
		await State.init();
		
		const
			binance = await Binance.load(),
			bittrex = await Bittrex.load();
		
		//		await Redis.flushAll();
		await RealTrader.loadAllEntites();
		
		await binance.run();
		await bittrex.run();
		
		let bittrex_last_timestamp = 0;
		
//		binance.getOrderStatus('74847543');
		
		/*		bittrex.getBook$(Markets.USDT_BTC)
					   .subscribe(book => {
						   console.log('------------');
					
		//			       console.log('bittrex ' + book.timestamp);
						   console.log('bittrex ' + (book.timestamp - bittrex_last_timestamp));
						   bittrex_last_timestamp = book.timestamp;
					   })
				let binance_last_timestamp = 0;
					   */
		/*
				binance.getBook$(Markets.USDT_BTC)
					   .subscribe(book => {
						   console.log('------------');
		//			       console.log('binance ' + book.timestamp);
						   console.log('binance ' + (book.timestamp - binance_last_timestamp));
						   binance_last_timestamp = book.timestamp;
					   });
		*/
		
		/*
				const
					order = new Order({
						exchange_id     : Binance.id,
						machine_id      : 100,
						account_id      : 102,
						amount          : 0.01,
						type            : OrderTypes.sell,
						market          : Markets.USDT_BTC,
						Commodity_coin  : Coins.BTC,
						Money_coin      : Coins.USDT,
						price           : 8000,
						income_after_fee: 2000,
						fee_$           : 3,
						safe_price      : 8000
					});
		
				await order.execute();
		*/
		
	}
)();

/*const ref = (childCollection,childRefAttr) => (target, propKey) => {
	Object.defineProperty( target, propKey, {
		get: function() {
			return `getting * from ${childCollection} where ${childRefAttr}==${this.id}`
		}
	});
}

class M {
	constructor(public id){
	}
	
	@ref('orders','machineId')
	children;
}

let m = new M(11);
console.log(m.children)*/


