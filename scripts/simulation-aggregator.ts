import {State} from '../src/modules/State';
import {RealTrader} from '../src/RealTrader';
import {Binance} from '../src/exchanges/Binance/Binance';
import {Bittrex} from '../src/exchanges/Bittrex/Bittrex';
import {Mongo} from '../src/DB/Mongo';

//const japan2 = '54.92.121.246';

(async () => {
	await State.init();
	//	await State.init(japan2);
	//	await State.init();
	
/*
	const
		binance: Binance = await Binance.load(),  //!! do NOT create a new instance of exchnages. use ".load()" instead.
		bittrex: Bittrex = await Bittrex.load();
*/
	
/*
	let arr = await RealTrader.loadAllEntites();
	let [
		    machines,
		    transactions,
		    orders,
		    accounts,
	    ]   = arr;
*/
	
	await Mongo.init();
	
	let
		coll    = (await Mongo.getMasterClient())
			.db('simulations')
			.collection('reports'),
		reports = await coll
			.find({})
			.toArray();
	
	let tenM = 100000000;
	let reps = reports
		.filter(rep => rep.info.note === "from 3.5 to 7.5 which is now")
		.filter(a => a && a.summery)
		// .filter(rep=>rep.info.machineParams.AB_profit_threshold <0.00000002)
		.sort((a, b) => a.summery.total_tx_profit_$ > b.summery.total_tx_profit_$ ? 1 : -1)
		.map(r => ({
			profit_$: +r.summery.total_tx_profit_$.toFixed(8),
			AB      : Math.round(r.info.machineParams.AB_profit_threshold.toFixed(8) * tenM),
			BA      : Math.round(r.info.machineParams.BA_profit_threshold.toFixed(8) * tenM),
			tx_count: r.summery.total_tx_count,
			origian : r,
		}));
	
	let actions = (await (await Mongo.getMasterClient())
			    .db('simulations')
			    .collection('actions')
			    .find({simulation_uuid:reps[reps.length-1].origian.simulation_uuid})
			    .toArray());
	
	console.log('reports');
})();

