import {Markets} from '../src/types/enums/Markets';
import {Bittrex} from '../src/exchanges/Bittrex/Bittrex';
import {State} from '../src/modules/State';
import {Mongo} from '../src/DB/Mongo';
import {Exchange} from '../src/exchanges/abstract/Exchange';
import {Binance} from '../src/exchanges/Binance/Binance';
import {Hitbtc} from '../src/exchanges/HitBTC/Hitbtc';
import {Log} from '../src/utils/Log';
import {Redis} from '../src/DB/Redis';

const INTERVAL = 1000;

export class HarvestBooks
{
	//region members
	static active_exchanges: Exchange[];
	
	static bittrex: Bittrex;
	static binance: Binance;
	static hitbtc: Hitbtc;
	
	//endregion
	
	static async init() {
		await State.init();
		await Mongo.init();
		
		this.binance          = await Binance.load();
		this.bittrex          = await Bittrex.load();
		this.hitbtc           = await Hitbtc.load();
		this.active_exchanges = [
			this.bittrex,
			this.binance,
			this.hitbtc,
		];
	}
	
	static async run() {
		let i = 0;
		
		for (let exchange of this.active_exchanges)
		{
			await exchange.initGetBookSnapshot();
		}
		
		setInterval(() => {
			let cycleId = Date.now();
			if (++i === 100)
			{
				Log(new Date(),'harvest/run','HARVESTER');
				i = 0;
			}
			
			for (let exchange of this.active_exchanges)
			{
				for (let market of exchange.activeMarkets)
				{
					this.getCycle(exchange, market, cycleId);
				}
			}
		}, INTERVAL);
	}
	
	static async getCycle(exchange: Exchange, market: Markets, cycleId: number) {
		try
		{
			let book = await exchange.getBookSnapshot(market, cycleId);
			if (book)
			{
				book      = {...book};
				book.buy  = book.buy.slice(0, exchange.book_length);
				book.sell = book.sell.slice(0, exchange.book_length);
				
				book['test'] = 'WTF';
				console.log(`${book.market} in ${book.exName}`);
				console.log('book.buy[0].price');
				console.log(book.buy[0].price);
				console.log('book.sell[0].price');
				console.log(book.sell[0].price);
			await Mongo.saveBook(book);
				
				/*
								console.log(
									`${book.exName} - ${book.market}:\t\t` +
									`best buy\t: ${book.buy[0].price + " : " + book.buy[0].amount} (${book.buy.length} rows) ` +
									`best sell\t: ${book.sell[0].price + " : " + book.sell[0].amount} (${book.sell.length} rows) `,
								);
				*/
				
			}
		} catch (err)
		{
			Log(err, `Harvest/getCycle/${exchange.exName}/${market}`);
		}
	}
}

(async () => {
	await HarvestBooks.init();
	HarvestBooks.run();
	
})();

//region ProcessMGMT
function onExit(err) {
	console.log('[XXX] RealTrader Exit', arguments);
	if (err)
	{
		
		Log(err, 'NODE ERROR >>> ', 'ERRORS');
	}
	else
	{
		Log('Node process - no error.', 'runner');
	}
	Redis.end();
	
	process.exit();
}

const proc: any = process;
proc.on('uncaughtException', onExit);
proc.on('exit', onExit);
proc.on('SIGINT', onExit);
process.on('unhandledRejection', r => {
	Log(r, 'unhandled promise ', 'HARVEST ERROR');
});

//endregion
