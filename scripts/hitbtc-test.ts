import {Markets} from '../src/types/enums/Markets';
import {Bittrex} from '../src/exchanges/Bittrex/Bittrex';
import {State} from '../src/modules/State';
import {Mongo} from '../src/DB/Mongo';
import {Exchange} from '../src/exchanges/abstract/Exchange';
import {Binance} from '../src/exchanges/Binance/Binance';
import {Hitbtc} from '../src/exchanges/HitBTC/Hitbtc';
import {Log} from '../src/utils/Log';
import {Redis} from '../src/DB/Redis';
import {EXCHANGE_CONFIG} from '../src/exchanges/EXCHANGE_CONFIG';
import {hitbtc_SocketApi} from '../src/APIS/hitbtc-market';
import {OrderBookCollector} from '../src/exchanges/HitBTC/OrderBookCollector';

const INTERVAL = 1000;

export class test
{
	//region members
	static active_exchanges: Exchange[];
	
	static bittrex: Bittrex;
	static binance: Binance;
	static hitbtc: Hitbtc;
	
	//endregion
	
	static async init() {
		console.log(`init...`);
		await State.init();
		await Mongo.init();
		
		this.binance          = await Binance.load();
		this.bittrex          = await Bittrex.load();
		this.hitbtc           = await Hitbtc.load();
		this.active_exchanges = [
			// this.bittrex,
			// this.binance,
			this.hitbtc,
		];
		console.log(`...init done`);
	}
	
	static async run() {
		//HarvestBooks.runTest_api();
		test.runTest_orderbookCollector();		
	}
	
	private static runTest_orderbookCollector() {
		let orderbookCollector = new OrderBookCollector(this.hitbtc, {
			verbose: true,
			auth   : {
				key   : EXCHANGE_CONFIG.HitBTC.key,
				secret: EXCHANGE_CONFIG.HitBTC.secret,
			},			
		});
		orderbookCollector.start();
		setInterval(() => {
			let cycleId = Date.now();
			//console.log(`cycleId `, cycleId);
			for (let market of this.hitbtc.activeMarkets)
			{
				let book = orderbookCollector.getBook(market);
				if (book)
				{
					console.log(`
		${book.exName} :: ${book.market}
		best buy  : ${book.buy[0].price} : ${book.buy[0].amount}    / ${book.buy.length} rows
		best sell : ${book.sell[0].price} : ${book.sell[0].amount}    / ${book.sell.length} rows`);
				}
			}
		}, INTERVAL);
	}
	
	private static runTest_api() {
		let api = new hitbtc_SocketApi({
			verbose: false,
			auth   : {
				key   : EXCHANGE_CONFIG.HitBTC.key,
				secret: EXCHANGE_CONFIG.HitBTC.secret,
			},
		});
		api.trade.getTradingBalance()
		   .then(x => x.map(x => `${parseFloat(x.available) > 0 ? "VVV" : "   "}  ${x.currency} : ${x.available} , ${x.reserved} `))
		   .then(x => x.join('\n'))
		   .then(console.log)
		   .catch(console.error);
		api.trade.getOrders()
		   .then(console.log)
		   .catch(console.error);
		api.trade.subscribeReports().subscribe(x => {
			console.log(x);
		}, err => console.error(err));
		
		api.on('open', () => console.log('event: open'));
		api.on('close', (code, reason) => console.log('event: close', code, reason));
		api.on('error', (err) => console.log('event: error', err));
		api.on('login', () => console.log('event: login'));
		
		api.start();
		
		// -- TEST CLOSE SOCKET
		setInterval(() => {
			api.stop();
		}, 10 * 1000);
	}
	
	static async getCycle(exchange: Exchange, market: Markets, cycleId: number) {
		try
		{
			let book = await exchange.getBookSnapshot(market, cycleId);
			if (book)
			{
				book      = {...book};
				book.buy  = book.buy.slice(0, exchange.book_length);
				book.sell = book.sell.slice(0, exchange.book_length);
				
				await Mongo.saveBook(book);
				
				//console.log(`${book.exName} :: ${book.market}
				//best buy  : ${book.buy[0].price + " : " + book.buy[0].amount}    / ${book.buy.length} rows
				//best sell : ${book.sell[0].price + " : " + book.sell[0].amount}    / ${book.sell.length} rows
				//`);
				
			}
		} catch (err)
		{
			Log(err, `Harvest/getCycle/${exchange.exName}/${market}`);
		}
	}
}

(async () => {
	console.log("starting");
	await test.init();
	test.run();
	
})();

//region ProcessMGMT
function onExit(err) {
	console.log('[XXX] RealTrader Exit', arguments);
	if (err)
	{
		
		Log(err, 'NODE ERROR >>> ', 'ERRORS');
	}
	else
	{
		Log('Node process - no error.', 'runner');
	}
	Redis.end();
	
	process.exit();
}

const proc: any = process;
proc.on('uncaughtException', onExit);
proc.on('exit', onExit);
proc.on('SIGINT', onExit);
process.on('unhandledRejection', r => {
	Log(r, 'unhandled promise ', 'HARVEST ERROR');
});

//endregion
