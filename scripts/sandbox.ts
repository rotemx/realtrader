import {State} from '../src/modules/State';
import {RealTrader} from '../src/RealTrader';
import {Binance} from '../src/exchanges/Binance/Binance';
import {Order} from '../src/entites/Order';
import {Bittrex} from '../src/exchanges/Bittrex/Bittrex';
import {Redis} from '../src/DB/Redis';
import {fix2} from '../src/utils/fix';

//const japan2 = '54.92.121.246';
const
	moment  = require('moment'),
	fse     = require('fs-extra'),
	US_WEST = 'uswest';

(async () => {
	//	await State.init();
	await State.init(US_WEST);
	//	await State.init();
	
	const
		binance: Binance = await Binance.load(),  //!! do NOT create a new instance of exchnages. use ".load()" instead.
		//		hitbtc: Hitbtc   = await Hitbtc.load();
		bittrex: Bittrex = await Bittrex.load();
	
	let arr = await RealTrader.loadAllEntites();
	let [
		    machines,
		    transactions,
		    orders,
		    accounts,
	    ]   = arr;
	
	let
		Bittrex_orders: Order[] = orders.filter(o => o.exchange_id === Bittrex.id),
		file_name               = `orders.29.8.delta-10--10.2btc.csv`,
		os                      = Bittrex_orders
			.sort((a, b) => {return a.placed_time > b.placed_time ? -1 : 1;})
			//			.filter(a => a.id > 279)
			.map(o => ({
				id : o.id,
				status: o.status,
				success: !!o.ACTUAL_price,
				EXP_profit_per_Commodity_unit : o.transaction.EXP_profit_per_Commodity_unit,
				EXP_price: o.EXP_price,
				SAFE_price: o.SAFE_price,
				amount: o.amount,
				filled_amount: o.filled_amount,
				ACTUAL_price: o.ACTUAL_price,
				type: o.type,
				time: moment(o.placed_time).format('DD-MM-YY HH:mm:ss:SSS'),
				orig: o
			}));
	
	fse.writeFileSync(file_name, '');
	fse.appendFileSync(file_name, `id, successful, status, time, type, EXP_profit_per_Commodity_unit, EXP_price, SAFE_price, ACTUAL_price, amount, filled_amount, delta \n`);
	
	for (let o of os)
	{
		fse.appendFileSync(file_name, `${o.id},${o.ACTUAL_price > 0},${o.status},${o.time},${o.type},${o.EXP_profit_per_Commodity_unit},${o.EXP_price},${o.SAFE_price},${o.ACTUAL_price},${o.amount},${o.filled_amount},${fix2(o.ACTUAL_price > 0 ? Math.abs(o.EXP_price - o.ACTUAL_price) : 0)} \n`);
	}
	
	console.log('== DONE ==');
	Redis.end()
})();

