import {State} from '../src/modules/State';
import {Mongo} from '../src/DB/Mongo';
import {Exchange} from '../src/exchanges/abstract/Exchange';
import {Log} from '../src/utils/Log';
import {Redis} from '../src/DB/Redis';
import {EXCHANGE_CONFIG} from '../src/exchanges/EXCHANGE_CONFIG';
import {hitbtc_SocketApi} from '../src/APIS/hitbtc-market';
import {Markets} from '../src/types/enums/Markets';
import {Hitbtc} from '../src/exchanges/HitBTC/Hitbtc';
import {getShortUUID} from '../src/utils/getShortUUID';

const INTERVAL = 1000;

export class HitBtcTrade_Test
{
	//region members
	static active_exchanges: Exchange[];
	static hitbtc: Hitbtc;
	
	//endregion
	
	static async init() {
		console.log(`init...`);
		await State.init();
		await Mongo.init();
		
		this.hitbtc = await Hitbtc.load();
		console.log(`...init done`);
	}
	
	static async run() {
		
		let api = new hitbtc_SocketApi({
			verbose: false,
			auth   : {
				key   : EXCHANGE_CONFIG.HitBTC.key,
				secret: EXCHANGE_CONFIG.HitBTC.secret,
			},
		});
		
		api.on('open', () => console.log('event: open'));
		api.on('close', (code, reason) => console.log('event: close', code, reason));
		api.on('error', (err) => console.log('event: error', err));
		api.on('login', () => console.log('event: login'));
		
		api.start();
		
		api.trade.subscribeReports()
		   .subscribe(
			   report => {
				   console.log("-- report ----------------------");
				   console.log(JSON.stringify(report, null, 2));
			   },
			   err => console.error(err),
			   () => console.log('subscription complete'),
		   );
		
		api.trade
		   .newOrder(this.hitbtc.marketToString(Markets.USDT_BTC), 'buy', 2, 0.07991, getShortUUID())
		   .then(x => {
			   console.log(': : : : execOrder : : : ');
			   console.log(JSON.stringify(x, null, 2));
		   }).catch(err => {
			console.log(': : : : EXEC ORDER ERRRRRROOOORRRRRRR: : : ');
			console.log(err);
		});
		
		//		api.trade.getOrders()
		//		   .then(console.log)
		//		   .catch(console.error);
		//		api.trade.subscribeReports().subscribe(x => {
		//			console.log(x);
		//		}, err => console.error(err));
		//
		
		//
		//		api.start();
		//
		//		// -- TEST CLOSE SOCKET
		//		setInterval(() => {
		//			api.stop();
		//		}, 10 * 1000);
	}
	
}

(async () => {
	console.log("starting");
	await HitBtcTrade_Test.init();
	HitBtcTrade_Test.run();
})();

//region ProcessMGMT
function onExit(err) {
	console.log('[XXX] RealTrader Exit', arguments);
	if (err)
	{
		
		Log(err, 'NODE ERROR >>> ', 'ERRORS');
	}
	else
	{
		Log('Node process - no error.', 'runner');
	}
	Redis.end();
	
	process.exit();
}

const proc: any = process;
proc.on('uncaughtException', onExit);
proc.on('exit', onExit);
proc.on('SIGINT', onExit);
process.on('unhandledRejection', r => {
	Log(r, 'unhandled promise ', 'HARVEST ERROR');
});

//endregion
