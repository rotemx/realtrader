import {Bittrex} from '../src/exchanges/Bittrex/Bittrex';
import {Log} from '../src/utils/Log';
import {Binance} from '../src/exchanges/Binance/Binance';
import {Redis} from '../src/DB/Redis';
import {State} from '../src/modules/State';
import {Markets} from '../src/types/enums/Markets';
import {Mongo} from '../src/DB/Mongo';
import {RealTrader} from '../src/RealTrader';
import {AjaxCombinator} from '../src/exchanges/ajax-combinator';

//region ProcessMGMT
function onExit(err) {
	console.log('[XXX] RealTrader Exit', arguments);
	if (err)
	{
		
		Log(err, 'NODE ERROR >>> ', 'ERRORS');
	}
	else
	{
		Log('Node process - no error.', 'runner');
	}
	Redis.end();
	
	process.exit();
}

const proc: any = process;
proc.on('uncaughtException', onExit);
proc.on('exit', onExit);
proc.on('SIGINT', onExit);
process.on('unhandledRejection', r => {
	Log(r, 'unhandled promise ', 'ERROR');
});

//endregion

let
	market = Markets.USDT_BTC;

(async () => {
	await State.init();
	await Mongo.init();
	
	const
		binance: Binance = await Binance.load(),  //!! do NOT create a new instance of exchanges. use ".load()" instead.
		bittrex: Bittrex = await Bittrex.load();
	
	await bittrex.run();
	await binance.run();
	
	let trader = new RealTrader(AjaxCombinator.getCombinedStream$(bittrex, binance, market), bittrex, binance);
	trader.init()
	      .catch(err => {
		      console.log('INIT');
		      Log(err, 'real-trader-hitbtc-runner:init');
	      });
	
})();
