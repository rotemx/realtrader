import {Bittrex} from '../src/exchanges/Bittrex/Bittrex';
import {State} from '../src/modules/State';
import {Markets} from '../src/types/enums/Markets';
import * as node_bittrex_api from 'node.bittrex.api';
import {BITTREX_API_ExecOrder_Reply} from '../src/exchanges/Bittrex/types/BITTREX_API_ExecOrder_Reply';
import {Redis} from '../src/DB/Redis';
import {Log} from '../src/utils/Log';
import {API_Order_Status} from '../src/exchanges/types/API_Order_Status';
import {BITTREX_API_GetOrderStatus_Reply} from '../src/exchanges/Bittrex/types/BITTREX_API_GetOrderStatus_Reply';

(async () => {
		await State.init();
		let bittrex = await Bittrex.load();
//		bittrex.run();
		
		let settings = {
			MarketName   : bittrex.marketToString(Markets.USDT_BTC),
			OrderType    : 'LIMIT',
			Quantity     : 0.001,
			Rate         : 2000,
			TimeInEffect : 'IMMEDIATE_OR_CANCEL',                              // supported options are 'IMMEDIATE_OR_CANCEL', 'GOOD_TIL_CANCELLED', 'FILL_OR_KILL'
			ConditionType: 'NONE',                                      // supported options are 'NONE', 'GREATER_THAN', 'LESS_THAN'
			Target       : 0,                                           // used in conjunction with ConditionType
		};
		
		node_bittrex_api.tradebuy(settings, (res: BITTREX_API_ExecOrder_Reply, err) => {
			console.log('res');
			console.log(res);
			
			console.log('err');
			console.log(err);
			
			node_bittrex_api.getorder({uuid: res.result.OrderId}, (res: BITTREX_API_GetOrderStatus_Reply, err) => {
				if (err || !res || !res.success)
				{
					console.log('err');
					console.log(err);
				}
				console.log(res.result, `bittrex/_getOrderStatus ${res.result.OrderUuid} res`, `bittrex`);
				
				const order_status: API_Order_Status = {
					remaining_amount: res.result.QuantityRemaining,
					filled_amount   : res.result.Quantity - res.result.QuantityRemaining,
					isOpen          : res.result.IsOpen,
					actual_price    : res.result.PricePerUnit,
					total_cost      : res.result.Price,
					fee_paid        : res.result.CommissionPaid,
				};
				console.log(order_status);
			});
			
			
			Redis.end()
		});
		
		// await order.execute()
		
		// await order.cancel()
		
		// let res_open_orders = await bittrex.getOpenOrders(Markets.USDT_BTC)
		
		//		console.log('Done');
		
		// Redis.end()
		// console.log(exec_res)
		
	}
)();

/*const ref = (childCollection,childRefAttr) => (target, propKey) => {
	Object.defineProperty( target, propKey, {
		get: function() {
			return `getting * from ${childCollection} where ${childRefAttr}==${this.id}`
		}
	});
}

class M {
	constructor(public id){
	}
	
	@ref('orders','machineId')
	children;
}

let m = new M(11);
console.log(m.children)*/


