import {Bittrex} from '../src/exchanges/Bittrex/Bittrex';
import {State} from '../src/modules/State';
import {Redis} from '../src/DB/Redis';
import {Binance} from '../src/exchanges/Binance/Binance';
import {Markets} from '../src/types/enums/Markets';

(async () => {
		
				const MarketManager = require('bittrex-market');
				
				const marketManager = new MarketManager(false);
				
				marketManager.market('USDT-BTC', (err, btc_usdt) => {
					
					btc_usdt.on('fills', console.log);
					
					//fires each time changes have been applied to the orderbook, and prints the current state of the orderbook
					btc_usdt.on('orderbookUpdated', () => {
						//print the asks side of the current order book state
						//the format is an array of [ rate, quantity ]
						//i.e. [[ 0.10994992, 4.37637934 ], [ 0.10996992, 10.47637934 ] ...]
						console.log(btc_usdt.asks);
						
						//same thing for the bids side
						console.log(btc_usdt.bids);
					});
					
					//fires each time changes have been applied to the orderbook, and prints the changes only
		/*
					let sides      = ['asks', 'bids'];
					let eventTypes = ['removed', 'inserted', 'updated'];
					
					sides.forEach((side) => {
						eventTypes.forEach((type) => {
							bitcoin.on(`orderbook.diff.${side}.${type}`, (event) => {
								console.log(side, type, event);
							});
						});
					});
		*/
				});
				
		await State.init();
		
		const
//			binance: Binance = await Binance.load(),  //!! do NOT create a new instance of exchnages. use ".load()" instead.
			bittrex: Bittrex = await Bittrex.load();
		
		bittrex.run();
//		binance.run();
		
		
		bittrex.getBook$(Markets.USDT_BTC).subscribe((book)=>{
			console.log(book);
		})
		
	}
)();

