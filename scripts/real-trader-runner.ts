import {Log} from '../src/utils/Log';
import {Bittrex} from '../src/exchanges/Bittrex/Bittrex';
import {Binance} from '../src/exchanges/Binance/Binance';
import {Redis} from '../src/DB/Redis';
import {State} from '../src/modules/State';
import {Markets} from '../src/types/enums/Markets';
import {Mongo} from '../src/DB/Mongo';
import {Book} from '../src/types/interfaces/Book';
import {Cycle} from '../src/entites/Cycle';
import {RealTrader} from '../src/RealTrader';
import {assertBookData} from '../src/utils/AssertBookData';
import { Observable } from "rxjs";

//region ProcessMGMT
function onExit(err) {
	console.log('[XXX] RealTrader Exit', arguments);
	if (err)
	{
		
		Log(err, 'NODE ERROR >>> ', 'ERRORS');
	}
	else
	{
		Log('Node process - no error.', 'runner');
	}
	Redis.end();
	
	process.exit();
}

const proc: any = process;
proc.on('uncaughtException', onExit);
proc.on('exit', onExit);
proc.on('SIGINT', onExit);
process.on('unhandledRejection', r => {
	Log(r, 'unhandled promise ', 'ERROR');
});

//endregion

let
	market = Markets.BTC_XVG;

(async () => {
	await State.init();
	await Mongo.init();
	//	await Redis.flushAll();  //TODO : remove before flight !!
	
	const
		binance: Binance                   = await Binance.load(),  //!! do NOT create a new instance of exchnages. use ".load()" instead.
		bittrex: Bittrex                   = await Bittrex.load(),
		
		bittrex_OBS: Observable<Book>      = bittrex.getBook$(market)
		                                            .filter(assertBookData),
		//		                                            .do(() => console.log('BITTREX Socket')),
		bittrex_Ajax_OBS: Observable<Book> = bittrex.getBookAjax$(market)
		                                            .filter(assertBookData),
		//		                                            .do(() => console.log('BITTREX Ajax')),
		
		binance_OBS: Observable<Book>      = binance.getBook$(market)
		                                            .filter(assertBookData),
		//		                                            .do(() => console.log('Binance Socket')),
		binance_Ajax_OBS: Observable<Book> = binance.getBookAjax$(market)
		                                            .filter(assertBookData);
	//		                                            .do(() => console.log('Binance Ajax'));
	
	bittrex.run();
	binance.run();
	
	const
		cycles$: Observable<Cycle> =
			Observable.combineLatest(
				binance_OBS.startWith(null),
				bittrex_OBS.startWith(null),
				bittrex_Ajax_OBS.startWith(null),
				binance_Ajax_OBS.startWith(null)
			)
				.filter(([binance_book, bittrex_book, bittrex_Ajax_book, binance_Ajax_book]: Book[]) => !!((binance_book || binance_Ajax_book) && (bittrex_book || bittrex_Ajax_book)))
				.map(([binance_book, bittrex_book, bittrex_Ajax_book, binance_Ajax_book]: Book[]) => {
					
					const
						selected_bittrex_book = (bittrex_Ajax_book && bittrex_Ajax_book.timestamp) > (bittrex_book && bittrex_book.timestamp) ? bittrex_Ajax_book : bittrex_book,
						selected_binance_book = (binance_Ajax_book && binance_Ajax_book.timestamp) > (binance_book && binance_book.timestamp) ? binance_Ajax_book : binance_book,
						bittrex_src           = (bittrex_Ajax_book && bittrex_Ajax_book.timestamp) > (bittrex_book && bittrex_book.timestamp) ? 'ajax' : 'socket',
						binance_src           = (binance_Ajax_book && binance_Ajax_book.timestamp) > (binance_book && binance_book.timestamp) ? 'ajax' : 'socket',
						time_delta            = Math.abs(selected_binance_book.timestamp - selected_bittrex_book.timestamp);
					
/*
					if (time_delta < 400)
					{
						Log(`[-]    Bittrex ${ bittrex_Ajax_book && bittrex_Ajax_book.timestamp > (bittrex_book && bittrex_book.timestamp) ? 'AJAX  ' : 'SOCKET'}     Binance ${binance_Ajax_book && binance_Ajax_book.timestamp > (binance_book && binance_book.timestamp) ? 'AJAX  ' : 'SOCKET'}      @${time_delta}`, '', '', false);
					}
*/
					
					return new Cycle({
						time_delta: time_delta,
						bookA     : selected_binance_book,
						bookB     : selected_bittrex_book,
						cycleId   : Date.now(),
						timestamp : Date.now(),
						sourceA   : binance_src,
						sourceB   : bittrex_src
					});
				});
	
	let trader = new RealTrader(cycles$, binance, bittrex);
	trader.init();
	
	/*
		const deltas = [];
		binance_OBS.subscribe(book => {
			console.log('BIN');
		});
		bittrex_OBS.subscribe(book => {
			console.log('BITT');
		});
	cycles$.subscribe(cycle => {
		const delta = cycle.bookA.timestamp - cycle.bookB.timestamp;
		//	deltas.push(delta);
		console.log(delta);
	});
	*/
	
})();
