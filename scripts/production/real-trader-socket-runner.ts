//region ProcessMGMT
import {Bittrex} from '../../src/exchanges/Bittrex/Bittrex';
import {Log} from '../../src/utils/Log';
import {Binance} from '../../src/exchanges/Binance/Binance';
import {Redis} from '../../src/DB/Redis';
import {State} from '../../src/modules/State';
import {Markets} from '../../src/types/enums/Markets';
import {Mongo} from '../../src/DB/Mongo';
import {Observable} from 'rxjs';
import {Book} from '../../src/types/interfaces/Book';
import {assertBookData} from '../../src/utils/AssertBookData';
import {Cycle} from '../../src/entites/Cycle';
import {RealTrader} from '../../src/RealTrader';

function onExit(err) {
	console.log('[XXX] RealTrader Exit', arguments);
	if (err)
	{
		
		Log(err, 'NODE ERROR >>> ', 'ERRORS');
	}
	else
	{
		Log('Node process - no error.', 'runner');
	}
	Redis.end();
	
	process.exit();
}

const proc: any = process;
proc.on('uncaughtException', onExit);
proc.on('exit', onExit);
proc.on('SIGINT', onExit);
process.on('unhandledRejection', r => {
	Log(r, 'unhandled promise real-reader-socket-runner ', 'ERROR');
	process.exit();
});

//endregion

let
	market = Markets.USDT_BTC;

(async () => {
	await State.init();
	await Mongo.init();
	
	const
		binance: Binance              = await Binance.load(),  //!! do NOT create a new instance of exchnages. use ".load()" instead.
		bittrex: Bittrex              = await Bittrex.load(),
		
		bittrex_OBS: Observable<Book> = bittrex.getBook$(market)
		                                       .filter(assertBookData),
		binance_OBS: Observable<Book> = binance.getBook$(market)
		                                       .filter(assertBookData);
	
	bittrex.run();
	binance.run();
	
	const
		cycles$: Observable<Cycle> =
			Observable.combineLatest(
				binance_OBS.startWith(null),
				bittrex_OBS.startWith(null),
			          )
			          .filter(([binance_book, bittrex_book]: Book[]) => !!(binance_book && bittrex_book))
			          .map(([binance_book, bittrex_book]: Book[]) => {
				
				          /*
											  if (time_delta < 400)
											  {
												  Log(`[-]    Bittrex ${ bittrex_Ajax_book && bittrex_Ajax_book.timestamp > (bittrex_book && bittrex_book.timestamp) ? 'AJAX  ' : 'SOCKET'}     Binance ${binance_Ajax_book && binance_Ajax_book.timestamp > (binance_book && binance_book.timestamp) ? 'AJAX  ' : 'SOCKET'}      @${time_delta}`, '', '', false);
											  }
						  */
				
				          return new Cycle({
					          time_delta: -1,
					          bookA     : bittrex_book,
					          bookB     : binance_book,
					          cycleId   : Date.now(),
					          timestamp : Date.now(),
					          sourceA   : 'socket',
					          sourceB   : 'socket',
				          });
			          });
	
	let trader = new RealTrader(cycles$, bittrex, binance);
	trader.init();
	
	/*
		const deltas = [];
		binance_OBS.subscribe(book => {
			console.log('BIN');
		});
		bittrex_OBS.subscribe(book => {
			console.log('BITT');
		});
	cycles$.subscribe(cycle => {
		const delta = cycle.bookA.timestamp - cycle.bookB.timestamp;
		//	deltas.push(delta);
		console.log(delta);
	});
	*/
	
})();
