import {Mongo} from '../../src/DB/Mongo';
import {Markets} from '../../src/types/enums/Markets';
import {Exchanges} from '../../src/types/enums/Exchanges';
import {CycleSource_mongo} from '../Virtual_Machines/cycleSources/CycleSource_mongo';
import {MYSQL} from '../../src/APIS/MySQL/MySQL';
import moment = require('moment');

run()
	.then(() => console.log('done'))
	.catch(console.error);

async function run() {
	try
	{
		await MYSQL.init('');
		await Mongo.init();
		
		await exportDataToMySQL(
			Markets.USDT_BTC,
			[Exchanges.bittrex, Exchanges.binance]);
	} catch (err)
	{
		console.error('error in stream', err.message || err);
	}
	
	await Mongo.client.close().catch(err => console.error(err.message || err));
}

export async function exportDataToMySQL(market: Markets, exchanges: Exchanges[], overwrite = true,
) {
	const dbName = `${exchanges[0]}_${exchanges[1]}_${market}`;
	console.log(`----- database ${dbName} --------`);
	
	if (!overwrite)
	{
		let databases = await MYSQL.sequelize.query(`show databases like '${dbName}'`);
		if (databases[0].length > 0)
		{
			return;
		}
	}
	await MYSQL.sequelize.query(`create database if not exists ${dbName}`);
	await MYSQL.sequelize.query(`drop table if exists ${dbName}.records`);
	await MYSQL.init(dbName);
	
	await MYSQL.Record.sync();
	
	let source  = CycleSource_mongo(
		Mongo.client,
		market,
		exchanges,
		{
			logProgress  : true,
			progressStep : 0.01,
			onlyFullMatch:
				true,
		},
	);
	let records = [];
	
	await source.forEach(async data => {
		
		try
		{
			const
				book_A = data.books[0],
				book_B = data.books[1],
				record = {
					cycleId      : +data.cycleId,
					time         : moment(data.cycleId).format('DD-MM-YY HH:mm:ss'),
					A_buy_price  : book_A.buy[0].price,
					A_buy_amount : book_A.buy[0].amount,
					A_sell_price : book_A.sell[0].price,
					A_sell_amount: book_A.sell[0].amount,
					B_buy_price  : book_B.buy[0].price,
					B_buy_amount : book_B.buy[0].amount,
					B_sell_price : book_B.sell[0].price,
					B_sell_amount: book_B.sell[0].amount,
					AB_Delta     : book_A.sell[0].price * (1 - book_A.fee) - book_B.buy[0].price * (1 + book_B.fee),
					BA_Delta     : book_B.sell[0].price * (1 - book_B.fee) - book_A.buy[0].price * (1 + book_A.fee),
				};
			
			records.push(record);
			
			if (records.length == 10000)
			{
				await MYSQL.sequelize
				           .getQueryInterface()
				           .bulkInsert(`records`, records, {})
				           .catch(err => {
					           console.log(err);
				           });
				
				MYSQL.sequelize
				     .getQueryInterface();
				records = [];
			}
		} catch (e)
		{
			console.log(`error ${e}`);
		}
	});
}
