import {Mongo} from '../../src/DB/Mongo';
import {Markets} from '../../src/types/enums/Markets';
import {MYSQL} from '../../src/APIS/MySQL/MySQL';
import * as moment from 'moment';
import {Exchanges} from '../../src/types/enums/Exchanges';
import {Log} from '../../src/utils/Log';
import {Redis} from '../../src/DB/Redis';

interface SqlExchnage
{
	name: Exchanges,
	fee: number,
}

async function digest(ex_A: SqlExchnage, ex_B: SqlExchnage, overwrite = true, market: string, limit = 0, title = '') {
	
	try
	{
		console.time('data-conversion');
		
		await MYSQL.init('');
		
		const dbName = `${ex_A.name}_${ex_B.name}_${market}`;
		console.log(`----- database ${dbName} --------`);
		
		if (!overwrite)
		{
			let databases = await MYSQL.sequelize.query(`show databases like '${dbName}'`);
			if (databases[0].length > 0)
			{
				return;
			}
		}
		
		await MYSQL.sequelize.query(`create database if not exists ${dbName}`);
		await MYSQL.sequelize.query(`drop table if exists ${dbName}.records`);
		
		await MYSQL.init(dbName);
		
		let
			client      = Mongo.client,
			dbA         = client.db(ex_A.name),
			dbB         = client.db(ex_B.name),
			
			collectionA = dbA.collection(market, {readPreference: 'secondaryPreferred'}, () => {}),
			collectionB = dbB.collection(market, {readPreference: 'secondaryPreferred'}, () => {}),
			
			rowsA       = await collectionA.find({}).sort({cycleId:1}).toArray(),
			rowsB       = await collectionB.find({}).sort({cycleId:1}).toArray(),
			
			countA      = rowsA.length,
			countB      = rowsB.length,
			Record      = MYSQL.Record
		;
		
		if (limit)
		{
			rowsA = rowsA.slice(0, limit);
			rowsB = rowsB.slice(0, limit);
		}
		
		await Record.sync();
		
		//A
		
		const SKIP_ROWS = 10;
		
		for (let i = 0; i < countA; i += SKIP_ROWS)
		{
			
			let row = rowsA[i];
			
			if (i % ((limit / 100) || 1000) === 0)
			{
				console.log(`${title } : ${dbName} - A: ${i} records - ${(i / (limit || countA) * 100).toFixed(2)}%`);
			}
			
			await Record.create({
				            cycleId      : +row.cycleId,
				            time         : moment(row.cycleId).format('DD-MM-YY HH:mm:ss'),
				            A_buy_price  : row.book.buy[0].price,
				            A_buy_amount : row.book.buy[0].amount,
				            A_sell_price : row.book.sell[0].price,
				            A_sell_amount: row.book.sell[0].amount,
				            B_buy_price  : -1,
				            B_buy_amount : -1,
				            B_sell_price : -1,
				            B_sell_amount: -1,
				            AB_Delta     : -1,
				            BA_Delta     : -1,
			            })
			            .catch(err => {
				            console.log(err);
			            });
		}
		
		//B
		for (let i = 0; i < countB; i++)
		{
			let row = rowsB[i];
			
			if (i % (limit / 100 || 1000) === 0)
			{
				console.log(`${title } : ${dbName} B: ${(i / (limit || countB) * 100).toFixed(2)} %`);
			}
			
			let other: any = await  Record.findOne({
				where: {
					cycleId: +row.cycleId,
				},
			});
			if (!other)
			{
				continue;
			}
			
			await Record.update({
					B_buy_price  : row.book.buy[0].price,
					B_buy_amount : row.book.buy[0].amount,
					B_sell_price : row.book.sell[0].price,
					B_sell_amount: row.book.sell[0].amount,
					AB_Delta     : other.A_sell_price * (1 - ex_A.fee) - row.book.buy[0].price * (1 + ex_B.fee),
					BA_Delta     : row.book.sell[0].price * (1 - ex_B.fee) - other.A_buy_price * (1 + ex_A.fee),
				},
				{
					where: {
						cycleId: row.cycleId,
					},
				});
		}
		
		await Record.destroy({
			where: {
				b_buy_price: -1,
			},
		});
		
		console.log('ALL DONE');
	} catch (e)
	{
		console.error(e);
	}
	
	console.timeEnd('data-conversion');
}

(async () => {
	
	await Mongo.init();
	
	let exchangesPool = [
		{
			name: Exchanges.bittrex,
			fee : 0.0025,
		},
		{
			name: Exchanges.binance,
			fee : 0.0005,
		},
		//		{
		//			name: Exchanges.bittrex,
		//			fee : 0.0025,
		//		},
	];
	
	/*
		let count = 0;
		
		for (let i = 0; i < exchangesPool.length; i++)
		{
			for (let j = i + 1; j < exchangesPool.length; j++)
			{
				for (let market in Markets)
				{
					count++;
					
					const ex_a = exchangesPool[i],
						  ex_b = exchangesPool[j]
					;
					
					await digest(
						ex_a,
						ex_b,
						true,
						market,
						0,
						`${count} of ${fact(exchangesPool.length) * market.length}`);
					//await digest(ex_a, ex_b, false, market, 1000);
				}
			}
		}
	*/
	
	const market = Markets.USDT_ETH;
	
	await digest(
		exchangesPool[0],
		exchangesPool[1],
		true,
		market,
		0,
		`BITT_BIN_${market}`,
	);
	
	console.log('[V] ALL ALL ALL DONE');
	
	Mongo.close();
	
})();

//region ProcessMGMT
function onExit(err) {
	console.log('[XXX] RealTrader Exit', arguments);
	if (err)
	{
		
		Log(err, 'NODE ERROR >>> ', 'ERRORS');
	}
	else
	{
		Log('Node process - no error.', 'runner');
	}
	Redis.end();
	
	process.exit();
}

const proc: any = process;
proc.on('uncaughtException', onExit);
proc.on('exit', onExit);
proc.on('SIGINT', onExit);
process.on('unhandledRejection', r => {
	Log(r, 'unhandled promise ', 'HARVEST ERROR');
});

//endregion
