import * as fse from 'fs-extra';
import {Mongo} from '../../src/DB/Mongo';

const moment = require('moment');

async function parsePM2Logs(input_filename: string) {
	
	console.time('parsePM2Logs');
	
	let
		regexp_buy  = /^[0-9]{2}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}:[0-9]{3} \t[0-9]{13} \t BITT \[B [0-9]{4}.[0-9]{2} S [0-9]{4}.[0-9]{2}\]\tBINN \[B [0-9]{4}.[0-9]{2} S [0-9]{4}.[0-9]{2}\]\t BUY:\t(-|)[0-9]{1,4}.[0-9]{2}/,
		regexp_sell = /^[0-9]{2}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}:[0-9]{3} \t[0-9]{13} \t BITT \[B [0-9]{4}.[0-9]{2} S [0-9]{4}.[0-9]{2}\]\tBINN \[B [0-9]{4}.[0-9]{2} S [0-9]{4}.[0-9]{2}\]\tSELL:\t\t\t(-|)[0-9]{1,4}.[0-9]{2}/,
		
		data        = fse.readFileSync(input_filename, 'utf8')
		                 .split('\n')
		                 //		                 .slice(0, 1000)
		                 .filter(str => regexp_buy.test(str) || regexp_sell.test(str))
		                 .map((line, i, arr) => {
			
			                 const
				                 data      = line.split('\t'),
				                 BITT_buy  = +data[2].split('\t')[0].split('BITT [B')[1].trim().split(' ')[0],
				                 BITT_sell = +data[2].split('\t')[0].split('BITT [B')[1].trim().split(' ')[2].replace(']', ''),
				                 BINN_buy  = +data[3].split('BINN [B ')[1].split(' S ')[0],
				                 BINN_sell = +data[3].split('BINN [B ')[1].split(' S ')[1].replace(']', ''),
				                 res       = {
					                 time     : new Date(+data[1]),
					                 cycleId  : +data[1],
					                 BITT_buy : BITT_buy,
					                 BITT_sell: BITT_sell,
					
					                 BINN_buy  : BINN_buy,
					                 BINN_sell : BINN_sell,
					                 
					                 delta_buy : BINN_sell * 0.9995 - BITT_buy * 1.0025,
					                 delta_sell: BITT_sell * 0.9975 - BINN_buy * 1.0005,
				                 };
			
			                 if (!(i % 1000))
			                 {
				                 console.log(`${(i / arr.length * 100).toFixed()} %`);
			                 }
			                 return res;
		                 });
	
	let
		BITT_buy_arr   = data.map((d: any) => ({t: d.time, y: d.BITT_buy})),
		BITT_sell_arr  = data.map((d: any) => ({t: d.time, y: d.BITT_sell})),
		BINN_buy_arr   = data.map((d: any) => ({t: d.time, y: d.BINN_buy})),
		BINN_sell_arr  = data.map((d: any) => ({t: d.time, y: d.BINN_sell})),
		delta_sell_arr = data.map((d: any) => ({t: d.time, y: d.delta_sell})),
		delta_buy_arr  = data.map((d: any) => ({t: d.time, y: d.delta_buy}));
	
	await Promise.all([
		BITT_buy.insertMany(BITT_buy_arr),
		BITT_sell.insertMany(BITT_sell_arr),
		BINN_buy.insertMany(BINN_buy_arr),
		BINN_sell.insertMany(BINN_sell_arr),
		delta_buy.insertMany(delta_buy_arr),
		delta_sell.insertMany(delta_sell_arr),
	]);
	
	console.timeEnd('parsePM2Logs');
	console.log(`=== DONE === proccessed ${BITT_buy_arr.length} records`);
}

let
	BITT_buy,
	BITT_sell,
	BINN_buy,
	BINN_sell,
	delta_buy,
	delta_sell,
	client;

(async () => {
	Mongo.init();
	client = await Mongo.getMasterClient();
	
	await client.db('cycleDigests').dropDatabase();
	
	BITT_buy   = await client.db('cycleDigests').collection('BITT_buy');
	BITT_sell  = await client.db('cycleDigests').collection('BITT_sell');
	BINN_buy   = await client.db('cycleDigests').collection('BINN_buy');
	BINN_sell  = await client.db('cycleDigests').collection('BINN_sell');
	delta_buy  = await client.db('cycleDigests').collection('delta_buy');
	delta_sell = await client.db('cycleDigests').collection('delta_sell');
	
	const input_filename = 'logs/_from_server/node-out-1-27.9.2018.log';
	await parsePM2Logs(input_filename);
	Mongo.close();
})();






















