const COLS = [
	'BITT_buy',
	'BITT_sell',
	'BINN_buy',
	'BINN_sell',
	'delta_buy',
	'delta_sell',
];

export async function getChartData(client, fromTime?, toTime?) {
	
	const
		promises  = [],
		chartData = {};
	
	for (let col of COLS)
	{
		promises.push(client.db('cycleDigests')
		                    .collection(col)
		                    .aggregate([
			                    {
			                    	$match : {
					                    "t": {
						                    $gte: new Date("2018-08-28T00:00:00.000Z"),
						                    $lte: new Date('2018-08-30T00:00:00.000Z')
					                    },
				                    }
			                    },
		                    	{
			                    $bucketAuto: {
				                    groupBy: "$t",
				                    buckets: 10000, // number of buckets
				                    output : {
					                    y: {$avg: "$y"},
					                    t: {$max: "$t"},
					                    "count": { $sum: 1 },
				                    },
			                    },
		                    }],
			                    {allowDiskUse: true}
			                    )
		                    .toArray());
	}
	
	let arr = await Promise.all(promises);
	console.log(`Result count ${arr[1].length}`);
	return arr.reduce((pre, curr, i) => {
		pre[COLS[i]] = curr;
		return pre;
	}, {});
	
}
