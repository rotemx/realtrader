"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const get_chart_data_1 = require("./get-chart-data");
const Mongo_1 = require("../../src/DB/Mongo");
const express = require('express'), app = express();
let client;
//app.get('/', (req, res) => res.send('Hello World!'));
app.get('/chartData', async (req, res) => {
    let { fromTime, toTime } = req.query;
    res.send(await get_chart_data_1.getChartData(client, fromTime, toTime));
});
app.use(express.static(__dirname + '/public'));
Mongo_1.Mongo.init()
    .then(async () => {
    client = await Mongo_1.Mongo.getMasterClient();
    app.listen(3000, () => console.log('Graph Server listening on port 3000!'));
});
//# sourceMappingURL=graph-server.js.map