window.chartColors = {
	red   : 'rgb(255, 99, 132)',
	orange: 'rgb(255, 159, 64)',
	yellow: 'rgb(255, 205, 86)',
	green : 'rgb(75, 192, 192)',
	blue  : 'rgb(54, 162, 235)',
	purple: 'rgb(153, 102, 255)',
	grey  : 'rgb(201, 203, 207)'
};


function setConfig() {
	
	config = {

		type   : 'line',
		data   : {
			labels  : window.chartData.BITT_buy.map(a => a.t),
			datasets: [
				{
					borderColor: window.chartColors.blue,
					fill       : false,
					label      : 'BITT_buy',
					data       : window.chartData.BITT_buy,
					pointRadius: 1,
					borderWidth: 1,
					yAxisID    : 'usdAxis'
					
				},
				{
					borderColor: window.chartColors.orange,
					fill       : false,
					label      : 'BINN_buy',
					data       : window.chartData.BINN_buy,
					pointRadius: 0.75,
					borderWidth: 1,
					yAxisID    : 'usdAxis'
				},
				
				{
					borderColor: window.chartColors.green,
					fill       : false,
					label      : 'delta_buy',
					data       : window.chartData.delta_buy,
					pointRadius: 0.75,
					borderWidth: 1,
					yAxisID    : 'deltaAxis',
					showLine   : false
				},
				{
					borderColor: window.chartColors.red,
					fill       : false,
					label      : 'delta_sell',
					data       : window.chartData.delta_sell,
					pointRadius: 1.25,
					borderWidth: 1,
					yAxisID    : 'deltaAxis',
					showLine   : false
					
				},
			],
		},
		options: {
			animation : false,
			// showLines : false,
			elements  : {
				line: {
					tension: 0
				}
			},
			scales    : {
				xAxes: [{
					type        : 'time',
					distribution: 'linear',
					ticks       : {
						source: 'auto'
					},
					time        : {
						unit          : 'minute',
						displayFormats: {
							minute: 'DD-MM-YY h:mm:ss:SSS'
						},
						
					},
					
				}],
				yAxes: [{
					type     : 'linear', // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
					display  : true,
					position : 'left',
					id       : 'usdAxis',
					gridLines: {
						zeroLineColor: "rgba(0,255,0,1)"
					},
					ticks    : {
						stepSize: 10
					},
				}, {
					type     : 'linear', // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
					display  : true,
					position : 'right',
					id       : 'deltaAxis',
					// grid line settings
					gridLines: {
						drawOnChartArea: false, // only want the grid lines for one axis to show up
						zeroLineColor  : "rgba(0,0,0,1)",
						zeroLineWidth  : 0.5
						
					},
				}],
			},
			annotation: {
				annotations: [
					{
						drawTime   : 'afterDraw', // overrides annotation.drawTime if set
						id         : 'a-line-1', // optional
						type       : 'line',
						mode       : 'horizontal',
						scaleID    : 'deltaAxis',
						value      : '2',
						borderColor: 'red',
						borderWidth: 1,
					},
					{
						drawTime   : 'afterDraw', // overrides annotation.drawTime if set
						id         : 'a-line-2', // optional
						type       : 'line',
						mode       : 'horizontal',
						scaleID    : 'deltaAxis',
						value      : '-10',
						borderColor: 'orange',
						borderWidth: 1,
					},
					{
						drawTime   : 'afterDraw', // overrides annotation.drawTime if set
						id         : 'a-line-3',
						type       : 'line',
						mode       : 'horizontal',
						scaleID    : 'deltaAxis',
						value      : '0',
						borderColor: 'black',
						borderWidth: 1,
					},
				]
			}
		}
	};
}
