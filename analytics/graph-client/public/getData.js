async function getData() {
	window.chartData = {
		BITT_buy  : [],
		BITT_sell : [],
		BINN_buy  : [],
		BINN_sell : [],
		delta_buy : [],
		delta_sell: [],
	};
	
	return (
		await fetch(`/chartData`))
		.json()
		.then(data => {
			window.chartData = data;
		});
	
}
