function onRefresh() {
	let
		from_datetime = moment((
			$('#fromdate').val()) + ' ' + $('#fromtime').val()),
		to_datetime = moment((
			$('#todate').val()) + ' ' + $('#totime').val());
	
	
	for (let key of Object.keys(cData))
	{
		cData[key] = cData[key].filter(a => moment(a.t).isBetween(from_datetime, to_datetime));
		
	}
	
	for (let dataset of config.data.datasets)
	{
		dataset.data = cData[dataset.label];
	}
	config.data.labels = cData.BITT_buy.map(a => a.t);
	myLineChart.update(config);
}
