import {getChartData} from './get-chart-data';
import {Mongo} from '../../src/DB/Mongo';

const
	express = require('express'),
	app     = express();
let client;

//app.get('/', (req, res) => res.send('Hello World!'));

app.get('/chartData', async (req, res) => {
	let {fromTime, toTime} = req.query;
	res.send(await getChartData(client, fromTime, toTime));
});

app.use(express.static(__dirname + '/public'));

Mongo.init()
     .then(async () => {
	     client = await Mongo.getMasterClient();
	
	     app.listen(3000, () => console.log('Graph Server listening on port 3000!'));
     });
