import { Observable, Subject } from 'rxjs';
import { SimulatorReportInfo, SimulatorReportAction, SimulatorReportSummery, IterableStreamTeardown, IterableStream } from '.';

export interface ReportContainer
{
    type: 'SimulatorReportInfo' | 'SimulatorReportSummery' | 'SimulatorReportAction',
    simulation_uuid: string,
    report: any,
}

export interface ReportsConsumer
{
    get_reportInfo(): SimulatorReportInfo | Promise<SimulatorReportInfo>,
    get_reportAction(): SimulatorReportAction | Promise<SimulatorReportAction>,
    get_reportSummery(): SimulatorReportSummery | Promise<SimulatorReportSummery>,
}

export function ReportSourceFactory(
    factory: (consumer: ReportsConsumer) => (void | IterableStreamTeardown | Promise<IterableStreamTeardown>),
)
{
    return IterableStream.createFromFactory<ReportContainer>(async (consumer) =>
    {
        //state
        let firedAllInfos = false;
        let firedAllActions = false;
        let firedAllSummeries = false;

        //exec child factory and inits
        let reportConsumer: ReportsConsumer = {
            get_reportInfo: () => null,
            get_reportAction: () => null,
            get_reportSummery: () => null,
        }
        let teardown = await factory(reportConsumer);
        teardown = teardown || (() => { });

        consumer.onNext_value = async () =>
        {
            if (!firedAllInfos)
            {
                let info = await reportConsumer.get_reportInfo();
	
	            if (info)
	            {
		            let event: ReportContainer = {
			            type: 'SimulatorReportInfo',
			            simulation_uuid: info.simulation_uuid,
			            report: info,
		            };
		            return event;
	            }
            }
	        firedAllInfos = true;

            if (!firedAllActions)
            {
                let action = await reportConsumer.get_reportAction();
                if (action)
                {
                    let event: ReportContainer = {
                        type: 'SimulatorReportAction',
                        simulation_uuid: action.simulation_uuid,
                        report: action,
                    };
                    return event;
                }
            }
            firedAllActions = true;

            if (!firedAllSummeries)
            {
                let summery = await reportConsumer.get_reportSummery();
                if (summery)
                {
                    let event: ReportContainer = {
                        type: 'SimulatorReportSummery',
                        simulation_uuid: summery.simulation_uuid,
                        report: summery,
                    };
                    return event;
                }
            }
	        firedAllSummeries = true;

            //done.
            return null;
        }

        return teardown;
    });
}
