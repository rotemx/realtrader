export interface StreamIterator<S>
{
    next(): Promise<IteratorResult<S>>,
    close(): Promise<void>,
}
export interface IterableConsumer<S>
{
    onNext_iterator?: () => (IteratorResult<S> | Promise<IteratorResult<S>>),
    onNext_value?: () => (S | Promise<S> | Promise<null | undefined>),
}
export type IterableStreamFactory<S> = (consumer: IterableConsumer<S>) => (void | IterableStreamTeardown | Promise<IterableStreamTeardown>)
export type IterableStreamTeardown = () => (any | Promise<any>);

export class IterableStream<S>
{
    constructor(private factory: IterableStreamFactory<S>)
    {
    }

    getIterator(): StreamIterator<S>
    {
        const assertStupidDummy = (() => { throw 'no, dummy... you don\'t invoke this! you provide the consumer with a onNext_iterator() or onNext_value() callback function, that returns a value when he requests one' });

        let _done = false;
        let _initialized = false;

        let factory = this.factory;
        let consumer: IterableConsumer<S> = {
            onNext_iterator: assertStupidDummy,
            onNext_value: assertStupidDummy,
        };

        let fnOnNextIterator: () => (IteratorResult<S> | Promise<IteratorResult<S>>) = () => null;
        let fnTearDown: IterableStreamTeardown = () => { };

        return {
            next: async (): Promise<IteratorResult<S>> =>
            {
                if (_done)
                {
                    return iteratorDone();
                }

                try
                {
                    if (!_initialized)
                    {
                        let consumerTeardown = await factory(consumer);
                        fnTearDown = consumerTeardown || fnTearDown;

                        let found_onNext_iterator = consumer.onNext_iterator && consumer.onNext_iterator != assertStupidDummy;
                        let found_onNext_value = consumer.onNext_value && consumer.onNext_value != assertStupidDummy;

                        if (found_onNext_iterator && found_onNext_value)
                        {
                            throw Error('you can either supply an onNext_iterator() or onNext_value() callback function. not both!');
                        }

                        if (found_onNext_iterator)
                        {
                            fnOnNextIterator = consumer.onNext_iterator;
                        } else if (found_onNext_value)
                        {
                            fnOnNextIterator = async () =>
                            {
                                let value = await consumer.onNext_value();
                                return (value != null)
                                    ? iteratorStep(value)
                                    : iteratorDone<S>()
                            }
                        } else
                        {
                            fnOnNextIterator = () => iteratorDone();
                        }


                        _initialized = true;
                    }

                    let step = await fnOnNextIterator();
                    if (step.done)
                    {
                        _done = true;
                        await fnTearDown();
                    }
                    return step;

                } catch (err)
                {
                    _done = true;
                    await fnTearDown();
                    throw err;
                }
            },

            close: async (): Promise<void> =>
            {
                if (_done)
                {
                    return;
                }

                _done = true;
                await fnTearDown();
            },
        };
    }

    //-------------------------------------------------------    
    //ops:

    async forEach(
        callback: (value: S, i: number) => (void | Promise<void>),
        halt?: () => (boolean | Promise<boolean>)
    )
    {
        const source = this.getIterator();

        halt = halt || (() => false); //default halter (never , sync)
        let i = 0;
        while (!(await halt()))
        {
            let { done, value } = await source.next();
            if (done)
            {
                break;
            }
            await callback(value, i++);
        }
    }

    map<T>(callback: (value: S) => (T | Promise<T>))
    {
        return new IterableStream<T>(async (consumer) =>
        {
            const source = this.getIterator();
            consumer.onNext_iterator = (async () =>
            {
                let step = await source.next();
                return (step.done)
                    ? iteratorDone<T>()
                    : iteratorStep(await callback(step.value));
            });

            return () => source.close();
        });
    }

    filter(callback: (value: S) => (boolean | Promise<boolean>))
    {
        return new IterableStream<S>(async (consumer) =>
        {
            const source = this.getIterator();

            consumer.onNext_iterator = (async () =>
            {
                while (true)
                {
                    let step = await source.next();
                    if (step.done || await callback(step.value))
                    {
                        return step;
                    }
                }
            });

            return () => source.close();
        });
    }

    //-------------------------------------------------------

    static createFromFactory<S>(factory: IterableStreamFactory<S>): IterableStream<S>
    {
        return new IterableStream(factory);
    }

    static createFromMethods<S>(
        functions: {
            nextFn_value?: () => S | Promise<S>,
            nextFn_iterator?: () => IteratorResult<S> | Promise<IteratorResult<S>>,
            initFn?: () => void | Promise<void>
            disposeFn?: () => void | Promise<void>
        }
    ): IterableStream<S>
    {
        return new IterableStream<S>(async (consumer) => 
        {
            let { disposeFn, initFn, nextFn_iterator, nextFn_value } = functions;

            if (nextFn_iterator)
            {
                consumer.onNext_iterator = nextFn_iterator;
            }

            if (nextFn_value)
            {
                consumer.onNext_value = nextFn_value;
            }

            await initFn();

            return disposeFn;
        });
    }
}

//-------------------------------------------------------
// HELPERS

function iteratorDone<T>(): IteratorResult<T>
{
    return {
        done: true,
        value: undefined,
    }
}

function iteratorStep<T>(value: T): IteratorResult<T>
{
    return {
        done: false,
        value: value,
    }
}
