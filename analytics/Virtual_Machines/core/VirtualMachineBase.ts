import { EventEmitter } from "events";
import * as _ from 'lodash';
import { MultipleBooksCycle } from "../../../src/types/interfaces/MultipleBooksCycle";
import { Log } from "../../../src/utils/Log";
import { Book } from "../../../src/types/interfaces/Book";
import { Exchanges } from "../../../src/types/enums/Exchanges";
import { VirtualTransaction, VirtualAccount, VirtualOrder } from ".";

const EVENT_TRANSACTION_SINGLE = 'transaction';
type EVENT_TRANSACTION_SINGLE = 'transaction';

const EVENT_TRANSACTION_BATCH = 'transaction_batch';
type EVENT_TRANSACTION_BATCH = 'transaction_batch';

const EVENT_STATE_CHANGED = 'state_change';
type EVENT_STATE_CHANGED = 'state_change';

export type listener_transactionSingle = (
    machine: IVirtualMachine,
    cycle: MultipleBooksCycle,
    transaction: VirtualTransaction,
    transaction_batch: VirtualTransaction[],
    batch_index: number,
    accountsBefore: VirtualAccount[],
    accountsAfter: VirtualAccount[]
) => void | Promise<void>;

export type Listener_transactionBatch = (
    machine: IVirtualMachine,
    cycle: MultipleBooksCycle,
    transactions: VirtualTransaction[],
    accountsBefore: VirtualAccount[],
    accountsAfter: VirtualAccount[]
) => void | Promise<void>;

export type listener_stateChanged = (
    machine: IVirtualMachine,
    stateBefore: any,
    stateAfter: any
) => void | Promise<void>;

export interface IVirtualMachine
{
    machineType: string;
    update(cycle: MultipleBooksCycle);
    accounts: VirtualAccount[],
    params: any,
    state?: any,
}

export abstract class VirtualMachineBase<T_PARAMS, T_STATE> extends EventEmitter implements IVirtualMachine
{
    TAG = VirtualMachineBase.name;

    get machineType() { return this.constructor.name; }

    constructor(
        public accounts: VirtualAccount[],
        public params: T_PARAMS,
        public state?: T_STATE,
    )
    {
        super();
    }

    on(event: EVENT_TRANSACTION_SINGLE, listener: listener_transactionSingle): this;
    on(event: EVENT_TRANSACTION_BATCH, listener: Listener_transactionBatch): this;
    on(event: EVENT_STATE_CHANGED, listener: listener_stateChanged): this;
    on(event: string | symbol, listener: (...args: any[]) => void): this
    {
        return super.on(event, listener);
    }

    protected abstract _updateInternalState(cycle: MultipleBooksCycle): boolean;
    protected abstract _getTransactions(cycle: MultipleBooksCycle): VirtualTransaction[];

    protected getAccountForBook(book: Book)
    {
        return this.getAccountForExchange(book.exName);
    }

    protected getAccountForExchange(exchange: Exchanges | string)
    {
        return this.accounts.find(acc => acc.exchange === exchange);
    }

    async update(cycle: MultipleBooksCycle)
    {
        let singleTransactionListeners = this.listeners(EVENT_TRANSACTION_SINGLE) as listener_transactionSingle[];
        let batchTransactionListeners = this.listeners(EVENT_TRANSACTION_BATCH) as Listener_transactionBatch[];
        let stateListeners = this.listeners(EVENT_STATE_CHANGED) as listener_stateChanged[];

        //update internal state
        let stateBefore = _.cloneDeep(this.state);
        let didStateChanged = this._updateInternalState(cycle);
        if (didStateChanged)
        {
            if (stateListeners.length)
            {
                let stateAfter = _.cloneDeep(this.state);
                await Promise.all(stateListeners.map(listener =>
                    Promise
                        .resolve(listener(this, stateBefore, stateAfter))
                        .catch((err) => console.log('error in machine listener ', err))
                ));
            }
        }

        //perform trades        
        let transactions = this._getTransactions(cycle);
        if (transactions && transactions.length)
        {
            let accountsBefore = _.cloneDeep(this.accounts);
            let accountsAfter: VirtualAccount[] = null;

            for (const [i, transaction] of transactions.entries())            
            {
                if (transaction.buy_order == null && transaction.sell_order == null)
                {
                    Log(`WTF - ${this.TAG}::update - transaction buy AND sell order are null together ?! why you say you have transaction if both empty ?!`);
                    return;
                }

                this.execBuy(transaction.buy_order);
                this.execSell(transaction.sell_order);

                if (singleTransactionListeners.length)
                {
                    accountsAfter = _.cloneDeep(this.accounts);
                    await Promise.all(singleTransactionListeners.map(listener =>
                        Promise
                            .resolve(listener(this, cycle, transaction, transactions, i, accountsBefore, accountsAfter))
                            .catch((err) => console.log('error in machine listener ', err))
                    ));
                }
            }

            if (batchTransactionListeners)
            {
                accountsAfter = accountsAfter || _.cloneDeep(this.accounts);

                await Promise.all(batchTransactionListeners.map(listener =>
                    Promise
                        .resolve(listener(this, cycle, transactions, accountsBefore, accountsAfter))
                        .catch((err) => console.log('error in machine listener ', err))
                ));
            }
        }
    }

    private execBuy(buy_order: VirtualOrder)
    {
        if (!buy_order)
        {
            return;
        }

        let account = this.getAccountForExchange(buy_order.exchange);

        if (!account)
        {
            Log(`WTF - ${this.TAG}::execBuy - want to execute order with ${buy_order.exchange} account but no have matching account in machine?!`);
            return;
        }

        account.balance_$ -= buy_order.amount * buy_order.price * (1 + account.fee);
        account.balance_C += buy_order.amount;

    }

    private execSell(sell_order: VirtualOrder)
    {
        if (!sell_order)
        {
            return;
        }

        let account = this.getAccountForExchange(sell_order.exchange);

        if (!account)
        {
            Log(`WTF - ${this.TAG}::execSell - want to execute order with account ${sell_order.exchange} but no have matching account in machine?!`);
            return;
        }

        account.balance_$ += sell_order.amount * sell_order.price * (1 - account.fee);
        account.balance_C -= sell_order.amount;
    }
}