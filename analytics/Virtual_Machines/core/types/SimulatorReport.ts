import {VirtualAccount, VirtualTransaction} from ".";
import {MultipleBooksCycle, MultipleBooksCycleSummery} from "../../../../src/types/interfaces/MultipleBooksCycle";
import {Exchanges} from "../../../../src/types/enums/Exchanges";

export interface SimulatorReportInfo
{
	simulation_uuid: string,
	simulation_batch_uuid: string,
	executionDate: number,
	note: string,
	machineType: string,
	machineParams: any,
	exchanges: Exchanges[],
	accounts: VirtualAccount[]
}

export interface SimulatorReportSummery
{
	simulation_uuid: string,
	simulation_batch_uuid:string,
	cycleIdStart: number,
	cycleIdEnd: number,
	
	machineStateStart: any,
	machineStateEnd: any,
	
	accountsStart: VirtualAccount[],
	accountsEnd: VirtualAccount[],
	
	total_tx_batches_count: number,
	total_tx_count: number,
	total_tx_amount_C: number,
	total_tx_profit_$: number,
	average_tx_depth: number,
	
}

export interface SimulatorReport
{
	simulation_uuid: string,
	simulation_batch_uuid:string,
	info: SimulatorReportInfo,
	summery: SimulatorReportSummery,
}

export interface SimulatorReportAction
{
	simulation_uuid: string,
	simulation_batch_uuid:string,
	cycleId: number,
	
	cycleFull: MultipleBooksCycle,
	cycleSummery: MultipleBooksCycleSummery,
	
	transaction: VirtualTransaction,
	transactionBatch_count: number;
	transactionBatch_index: number;
	accountsBefore: VirtualAccount[],
	accountsAfter: VirtualAccount[],
}
