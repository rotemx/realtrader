import { Exchanges } from "../../../../src/types/enums/Exchanges";
import { Markets } from "../../../../src/types/enums/Markets";
import { Book } from "../../../../src/types/interfaces/Book";

export interface VirtualOrder
{
    type: 'buy' | 'sell';
    price: number,
    amount: number,
    exchange: Exchanges,
    //account: VirtualAccount
}

export interface VirtualTransaction
{
    buy_order: VirtualOrder,
    sell_order: VirtualOrder,
    amount_C: number,
    tx_profit_$: number,
    profit_per_C: number,
}

export interface VirtualAccount
{
    exchange: Exchanges,
    market: Markets,
    balance_$: number,
    balance_C: number,
    fee: number,
}