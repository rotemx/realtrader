import {SimulatorReportInfo, SimulatorReportSummery, SimulatorReportAction} from ".";

export interface ISimulationLogger
{
	logSimulationStart(simulation_uuid: string, info: SimulatorReportInfo): any | Promise<any>;
	
	logSimulationEnd(error: any, simulation_uuid: string, summery: SimulatorReportSummery, info: SimulatorReportInfo): any | Promise<any>;
	
	logSimulationAction(simulation_uuid: string, action: SimulatorReportAction, info: SimulatorReportInfo): any | Promise<any>;
	
	logBatchStart(isBatch: boolean, simulation_batch_uuid: string): any | Promise<any>;
	
	logBatchEnd(isBatch: boolean, simulation_batch_uuid: string): any | Promise<any>;
}
