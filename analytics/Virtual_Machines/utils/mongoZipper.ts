import { Cursor } from "mongodb";
import { IterableStream } from "../core/iterableStreams/IterableStream";


export interface mongoZipperProgress
{
    progress: number,
    timeStart: number,
    timeDelta: number,

    totalCount: number,
    totalRead: number,
    totalMatched: number,
    totalFullMatched: number,
    infos: {
        count: number,
        reads: number,
    }[]
}


export interface mongoZipperResult<T>
{
    key: number,
    match: T[],
    isFullMatch: boolean,
    matchCount: number,
    missCount: number,
}

export interface mongoZipperOptions
{
    duplicateValues?: 'useFirst' | 'useAll',
    logDuplicates?: boolean,
    logProgress?: boolean,
    progressStep?: number,
    logger?: (progress: mongoZipperProgress) => void | Promise<void>
}


interface itemContainer<T>
{
    value: T,
    cursor: Cursor<T>,
    open: boolean,
}

type Query<T> = (() => Promise<Cursor<T>>) | (() => Cursor<T>) | Promise<Cursor<T>> | Cursor<T>;

//-------------------------------------------------------

export function mongoZipper<T_DB_ENTITY>(
    queries: Query<T_DB_ENTITY>[],
    matchBy: string | ((item: T_DB_ENTITY) => number),
    options?: mongoZipperOptions,
): IterableStream<mongoZipperResult<T_DB_ENTITY>>
{
    const TAG = mongoZipper.name;

    options = {
        duplicateValues: 'useFirst',
        logDuplicates: true,
        logProgress: false,
        progressStep: 0.01,
        logger: defaultLogger,
        ...options,
    }

    let getField: ((item: T_DB_ENTITY) => number);

    if (typeof (matchBy) === 'string')
    {
        const fieldName = matchBy;
        getField = (item => item[fieldName] as number);
    } else
    {
        getField = matchBy;
    }

    const updateProgress = async (progress: mongoZipperProgress, forceLog = false) =>
    {
        let { logger, logProgress, progressStep } = options;
        if (logProgress)
        {
            let newProgress = progress.totalRead / progress.totalCount;
            if (
                forceLog ||
                progress.progress < 0 ||
                newProgress >= progress.progress + progressStep
            )
            {
                progress.progress = newProgress;
                progress.timeDelta = Date.now() - progress.timeStart;
                try
                {
                    await logger(progress);
                } catch (err)
                {
                    console.error("error with progress logger", err);
                }
            }
        }
    }

    return IterableStream.createFromFactory<mongoZipperResult<T_DB_ENTITY>>(async (consumer) =>
    {
        //-- inits:
        let running = true;
        let items: itemContainer<T_DB_ENTITY>[] = [];
        let progress: mongoZipperProgress;

        let cursors = await executeQueries(queries);
        for (const cursor of cursors)
        {
            items.push({
                value: null,
                cursor: cursor,
                open: true,
            });
        }

        let counts = await Promise.all(cursors.map(c => c.count(true)));
        let totalCount = counts.reduce((a, b) => a + b, 0);

        progress = {
            timeStart: Date.now(),
            timeDelta: 0,
            progress: -1,
            totalRead: 0,
            totalCount: totalCount,
            totalMatched: 0,
            totalFullMatched: 0,
            infos: items.map((item, i) => ({
                count: counts[i],
                reads: 0,
            }))
        };

        let lastVal: (number | null)[] = items.map((item) => null);
        let totalDuplicates = items.map((item) => 0);

        //-- on next value:
        consumer.onNext_value = async () =>
        {
            if (!running)
            {
                return null;
            }

            // -- fill all holes:
            let emptyIdx = -1;
            while ((emptyIdx = items.findIndex((item, i) => item.open && item.value == null)) > -1)
            {
                let empty = items[emptyIdx];
                if (await empty.cursor.hasNext())
                {
                    let val = (await empty.cursor.next());

                    //duplicates check
                    let lastField = lastVal[emptyIdx]
                    let newField = getField(val);

                    if (lastField != null && lastField === newField)
                    {
                        (totalDuplicates[emptyIdx])++;

                        if (options.logDuplicates)
                        {
                            let msg = `same value on input #${emptyIdx} : ${newField} `
                            switch (options.duplicateValues)
                            {
                                case 'useAll':
                                    msg += (`- using all values`);
                                    break;
                                case 'useFirst':
                                    msg += (`- skipping (using only first occurrence)`);
                                    break;
                            }
                            console.warn(msg);
                        }
                        if (options.duplicateValues === 'useAll')
                        {
                            empty.value = val;
                        } else
                        {
                            //don't
                        }
                    } else
                    {
                        empty.value = val;
                    }

                    lastVal[emptyIdx] = newField;
                    progress.infos[emptyIdx].reads++;
                    progress.totalRead++;
                } else
                {
                    empty.open = false;
                }
            }

            // -- update running state:
            let numActives = items.filter(x => x.open).length;
            if (numActives == 0)
            {
                running = false;
                return null;
            }

            // -- match:
            //find smallest item
            let itemsWithValues = items.filter(x => !!x.value).map(x => getField(x.value));
            let smallestField = itemsWithValues.reduce((winner, current) =>
                (current < winner) ? current : winner,
                itemsWithValues[0]);

            //match against smallest item
            let matchCount = 0;
            let matchRow = [];
            items.forEach((item) =>
            {
                let matched = (item.value && getField(item.value) === smallestField);
                if (matched)
                {
                    matchRow.push(item.value);
                    item.value = null;
                    matchCount++;
                } else
                {
                    matchRow.push(null)
                }
            });

            let isFullMatch = matchCount === matchRow.length;
            if (isFullMatch)
            {
                progress.totalFullMatched++;
            }
            progress.totalMatched++;

            let res: mongoZipperResult<T_DB_ENTITY> = {
                key: smallestField,
                match: matchRow,
                isFullMatch: isFullMatch,
                matchCount: matchCount,
                missCount: matchRow.length - matchCount,
            };

            await updateProgress(progress);

            return res;

        };

        //--teardown:
        return async () =>
        {
            await Promise.all(
                items.map(item =>
                    item.cursor.close()
                        .catch(err => { console.warn('error closing cursor') })
                )
            );

            await updateProgress(progress, true);

            if (options.logDuplicates)
            {
                let msg = `total duplicate values encountered : [ ${totalDuplicates.join(" ")}]`
                switch (options.duplicateValues)
                {
                    case 'useAll':
                        msg += (`  - mode: using all values`);
                        break;
                    case 'useFirst':
                        msg += (`  - mode: skipping (using only first occurrence)`);
                        break;
                }
                console.log(msg);
            }
        }
    });
}

////////////////////////////////////////////////////////////////////////////////////////////

// this will check if something is a promise!

function isPromise(something)
{
    return (Promise.resolve(something) == something)
}

async function executeQueries<T>(queries: Query<T>[])
{
    let res: Cursor<T>[] = [];
    for (const query of queries)
    {
        let cursor: Cursor<T>;
        if (typeof query == 'function')
        {
            cursor = await query();
        } else if (isPromise(query))
        {
            cursor = await query;
            cursor = cursor.clone();
            cursor.rewind();
        } else
        {
            cursor = query as Cursor;
            cursor = cursor.clone();
            cursor.rewind();
        }

        res.push(cursor);
    }
    return res;
}

function defaultLogger(progress: mongoZipperProgress)
{
    let childProgresses = progress.infos.map((info, i) => padNumber(info.reads / info.count * 100, 3, 0) + "% ").join('')
    let totalsMsg = `${progress.totalRead} reads, ${progress.totalMatched} matches (${progress.totalFullMatched} full matched)`;

    console.log(`\
${padNumber(progress.progress * 100, 3, 0)}% : \
[ ${childProgresses} ]  \
${totalsMsg}  \
${padNumber((progress.timeDelta / 1000), 4, 0)} seconds\
`
    );
}

function padNumber(number: number, padding: number, precision: number)
{
    let str = (number).toFixed(precision);
    if (String.prototype['padStart'])
    {
        return str['padStart'](padding);
    } else
    {
        return str;
    }
}
