
export interface DEXP_params
{
    smoothing?: number;
    correction?: number;
    prediction?: number;
    jitterRad?: number;
    maxDeviation?: number;
}

export interface DEXP_overrides
{
    interpolateSample?: (x0: number, x1: number, w: number) => number;
    interpolateTrend?: (x0: number, x1: number, w: number) => number;
    calcTrend?: (x0: number, x1: number) => number;
    addTrend?: (sample: number, trend: number) => number;
}

export class DEXP_filter
{
    private params: DEXP_params = {
        smoothing: 0.5,
        correction: 0.5,
        prediction: 0.5,
        jitterRad: 0,
        maxDeviation: Infinity,
    }

    private overrides: DEXP_overrides = {
        interpolateSample: default_interpolateSample,
        interpolateTrend: default_interpolateTrend,
        calcTrend: default_calcTrend,
        addTrend: default_addTrend,
    }

    private history = 0;
    private sample_0 = 0;
    private filtered_0 = 0;
    private trend_0 = 0;

    private _current = 0;

    constructor(params: DEXP_params, overrides?: DEXP_overrides)
    {
        this.params = { ...this.params, ...params };
        this.overrides = { ...this.overrides, ...overrides };
    }

    get current(): number
    {
        return this._current;
    }

    reset(initial_sample?: number, initial_trend?: number)
    {
        this.history = 0;
        this.sample_0 = 0;
        this.filtered_0 = 0;
        this.trend_0 = 0;
        this._current = 0;

        if (initial_sample != null)
        {
            this.sample_0 = initial_sample;
            this.filtered_0 = initial_sample;
            this._current = initial_sample;
            this.history = 1;
        }

        if (initial_trend != null)
        {
            this.trend_0 = initial_trend;
            this.history = 2;
        }
    }
    
    update(sample: number): number
    {
        let interpolateSample = this.overrides.interpolateSample;
        let interpolateTrend = this.overrides.interpolateTrend;
        let calcTrend = this.overrides.calcTrend;
        let addTrend = this.overrides.addTrend;

        let params = this.params;

        let sample_0 = this.sample_0;
        let filtered_0 = this.filtered_0;
        let trend_0 = this.filtered_0;

        let sample_1 = sample;
        let filtered_1;
        let trend_1;
        let prediction;

        if (this.history == 0)
        {
            filtered_1 = sample_1;
            trend_1 = trend_0;
            this.history++;
        } else if (this.history == 1)
        {
            filtered_1 = interpolateSample(sample_0, sample_1, 0.5);
            trend_1 = calcTrend(filtered_1, filtered_0);
            trend_1 = interpolateTrend(trend_0, trend_1, params.correction);

            this.history++;
        } else
        {
            let diff;
            diff = sample_1 - filtered_0;
            if (diff < params.jitterRad)
            {
                sample_1 = interpolateSample(filtered_0, sample, diff / params.jitterRad)
            }

            filtered_1 = addTrend(filtered_0, trend_1);
            filtered_1 = interpolateSample(sample_1, filtered_1, params.smoothing);

            trend_1 = calcTrend(filtered_0, filtered_1);
            trend_1 = interpolateTrend(trend_0, trend_1, params.correction);
        }

        prediction = addTrend(filtered_1, trend_1);
        prediction = interpolateSample(filtered_1, prediction, params.prediction);

        let diff = prediction - sample_1;
        if (diff > params.maxDeviation)
        {
            prediction = interpolateSample(sample_1, prediction, diff / params.maxDeviation);
        }

        //store history:
        this.sample_0 = sample;
        this.filtered_0 = filtered_1;
        this.trend_0 = trend_1;
        this._current = prediction;

        return this.current;
    }
}

function default_interpolateSample(x0, x1, w)
{
    return x0 + w * (x1 - x0);
}

function default_interpolateTrend(x0, x1, w)
{
    return x0 + w * (x1 - x0);
}

function default_calcTrend(x0, x1)
{
    return (x1 - x0);
}

function default_addTrend(x0, x1)
{
    return (x1 - x0);
}

