import { DEXP_params, DEXP_filter, DEXP_overrides } from "./DEXP_filter";



export class DEXP_filterSigma
{
    private dexpAverage: DEXP_filter;
    private dexpVariance: DEXP_filter;

    private _currentAverage = 0;
    private _currentSigma = 0;

    constructor(
        averageSettings: { params: DEXP_params, overrides?: DEXP_overrides },
        sigmaSettings: { params: DEXP_params, overrides?: DEXP_overrides }
    )
    {
        this.dexpAverage = new DEXP_filter(
            averageSettings.params,
            averageSettings.overrides);

        this.dexpVariance = new DEXP_filter(
            sigmaSettings.params,
            sigmaSettings.overrides);
    }

    get current()
    {
        return {
            average: this._currentAverage,
            sigma: this._currentSigma
        };
    }

    resetAverage(initial_sample?: number, initial_trend?: number)
    {
        this.dexpAverage.reset(initial_sample, initial_trend);
        this._currentAverage = this.dexpAverage.current;
    }

    resetSigma(initial_sigma?: number, initial_sigma_trend?: number)
    {
        let initial_variance = null;
        if (initial_sigma != null)
        {
            initial_variance = initial_sigma ** 2;
        }

        let initial_variance_trend = null;
        if (initial_sigma_trend != null)
        {
            initial_variance_trend = initial_sigma_trend ** 2;
        }

        this.dexpVariance.reset(initial_variance, initial_variance_trend);
        this._currentSigma = Math.sqrt(this.dexpVariance.current);
    }

    update(sample: number)
    {
        let average = this.dexpAverage.update(sample);
        let sampleVariance = (sample - average) ** 2;
        let variance = this.dexpVariance.update(sampleVariance);
        let sigma = Math.sqrt(variance);

        this._currentAverage = average;
        this._currentSigma = sigma;

        return this.current;
    }
}



/* ------------------------
// example:

let a = new DEXP_filterSigma(
    {
        params: {
            smoothing: 0.5,
            correction: 0.5,
            prediction: 0.5,
            jitterRad: 0,
            maxDeviation: Infinity,
        }
    },
    {
        params: {
            smoothing: 0.5,
            correction: 0.5,
            prediction: 0.5,
            jitterRad: 0,
            maxDeviation: Infinity,
        }
    });

a.resetAverage(1000);
a.resetSigma(50);

let sigma, average;
a.update(1001);
average = a.current.average;
sigma = a.current.sigma;

a.update(1002);
average = a.current.average;
sigma = a.current.sigma;

a.update(1003);
average = a.current.average;
sigma = a.current.sigma;

a.update(1004);
average = a.current.average;
sigma = a.current.sigma;

a.update(1005);
average = a.current.average;
sigma = a.current.sigma;

a.update(1006);
average = a.current.average;
sigma = a.current.sigma;

a.update(1007);
average = a.current.average;
sigma = a.current.sigma;

a.update(1008);
average = a.current.average;
sigma = a.current.sigma;

a.update(1009);
average = a.current.average;
sigma = a.current.sigma;

*/