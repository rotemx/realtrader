import { MultipleBooksCycle } from "../../../src/types/interfaces/MultipleBooksCycle";
import { MongoClient } from "mongodb";
import { Markets } from "../../../src/types/enums/Markets";
import { Exchanges } from "../../../src/types/enums/Exchanges";
import { SingleBookCycle } from "../../../src/types/interfaces/SingleBookCycle";
import { mongoZipper, mongoZipperOptions } from "../utils/mongoZipper";

export interface CycleSource_mongoOptions extends mongoZipperOptions
{
    limit?: number,
    fromCycleId?: number,
    toCycleId?: number,
    onlyFullMatch?: boolean,
}

export function CycleSource_mongo(
    client: MongoClient,
    market: Markets,
    exchanges: Exchanges[],
    options?: CycleSource_mongoOptions
)
{
    options = {
        limit: 0,
        fromCycleId: 0,
        toCycleId: 0,
        onlyFullMatch: false,
        ...options
    };

    let source = mongoZipper<SingleBookCycle>(
        exchanges.map(ex => () => getQuery(ex)),
        item => item.cycleId,
        options,
    );

    if (options.onlyFullMatch)
    {
        source = source.filter(result => result.isFullMatch);
    }

    return source.map<MultipleBooksCycle>(result =>
        ({
            books: result.match.map(value => value ? value.book : null),
            cycleId: result.key,
            timestamp: result.key
        })
    );

    function getQuery(exchange: Exchanges) 
    {
        // optional query range:
        let query: {};
        if (options.fromCycleId && options.toCycleId)
        {
            query = { cycleId: { $gte: options.fromCycleId, $lte: options.toCycleId } }
        } else if (options.fromCycleId)
        {
            query = { cycleId: { $gte: options.fromCycleId } }
        } else if (options.toCycleId)
        {
            query = { cycleId: { $lte: options.toCycleId } }
        }

        let c = client
            .db(exchange)
            .collection(market, { readPreference: 'secondaryPreferred' },
                (err, collection) =>
                {
                    if (err)
                    {
                        console.error(`error getting collection ${exchange}.${market}`, err);
                    }
                }
            )
            .find<SingleBookCycle>(query);

        if (options.limit > 0)
        {
            c = c.limit(options.limit);
        }
	
	    let sort = [ ["cycleId", 1] ];
        return c.sort(sort);
    }
}
