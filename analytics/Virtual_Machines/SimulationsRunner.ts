import {VirtualMachineBase, IVirtualMachine} from "./core/VirtualMachineBase";
import {MultipleBooksCycle} from "../../src/types/interfaces/MultipleBooksCycle";
import {VirtualTransaction, VirtualAccount, SimulatorReportInfo, SimulatorReportSummery, SimulatorReportAction} from "./core/types";
import {ISimulationLogger} from "./core/ISimulationLogger";
import {summarizeMultipleBooksCycle} from "../../src/utils/summarizeBook";
import * as _ from 'lodash';
import {getShortUUID} from "../../src/utils/getShortUUID";
import {IterableStream} from "./core/iterableStreams/IterableStream";
import {fix2} from '../../src/utils/fix';
import {ReportsTranslatorRunner} from './ReportsTranslatorRunner';
import {ReportSourceFactory_mongo} from './report-sources/TranslatorSource_mongo';
import {SimulationLogger_aggregate} from './Loggers/SimulationLogger_aggregate';

interface executionBag
{
	isActive: boolean,
	simulation_uuid: string,
	summery: SimulatorReportSummery,
	info: SimulatorReportInfo,
}

export class SimulationsRunner
{
	private executionBags = new Map<IVirtualMachine, executionBag>();
	private simulation_batch_uuid:string;
	constructor(
		private cyclesSource: IterableStream<MultipleBooksCycle>,
		private machines: VirtualMachineBase<any, any>[],
		private type: 'parallel' | 'serial' = 'parallel',
		private loggers: ISimulationLogger[],
		private note?: string,
		simulation_batch_uuid ?: string,
	) {
		this.simulation_batch_uuid = simulation_batch_uuid || getShortUUID();
	}
	
	async run() {
		let on_transaction_bound      = this.on_transaction.bind(this);
		let on_transactionBatch_bound = this.on_transactionBatch.bind(this);
		let on_stateChanged_bound     = this.on_stateChanged.bind(this);
		
		let setupMachines = () => {
			this.machines.forEach(m => {
				m.on('transaction', on_transaction_bound);
				m.on('transaction_batch', on_transactionBatch_bound);
				m.on('state_change', on_stateChanged_bound);
			});
		};
		
		let cleanUpMachines = () => {
			this.machines.forEach(m => {
				m.removeListener('transaction', on_transaction_bound);
				m.removeListener('transaction_batch', on_transactionBatch_bound);
				m.removeListener('state_change', on_stateChanged_bound);
			});
		};
		
		try
		{
			setupMachines();
			await Promise.all(this.machines.map(m =>
				this.on_start(m),
			));
			
			await
				this.cyclesSource
				.forEach(async cycle => {
					//update cycle id range
					for (const bag of this.executionBags.values())
					{
						if (bag.isActive)
						{
							const summery        = bag.summery;
							summery.cycleIdStart = summery.cycleIdStart <= 0 ? cycle.cycleId : summery.cycleIdStart;
							summery.cycleIdEnd   = cycle.cycleId;
						}
					}
					
					await Promise.all(this.machines.map(m =>
						m.update(cycle),
					));
				});
			
			await Promise.all(this.machines.map(m =>
				this.on_end(null, m),
			));
			
			cleanUpMachines();
			
			// all ended
			//reports:
			console.log(`\nSAVING REPORTS FOR BATCH : ${this.simulation_batch_uuid}`);
			
			let
				reportsSource = ReportSourceFactory_mongo({simulation_batch_uuid: this.simulation_batch_uuid}),
				loggers       = [
					new SimulationLogger_aggregate(),
					//new SimulationLogger_JSON(__dirname, { mode: 'pretty' }),
					//new SimulationLogger_mongo(await Mongo.getMasterClient()),
					//new SimulationLogger_console(),
					//new SimulationLogger_csv(),
				],
				translator    = new ReportsTranslatorRunner(reportsSource, loggers, true);
			
			await translator.run();
			console.log(`\nDONE SAVING REPORTS`);
			
		} catch (err)
		{
			await Promise.all(this.machines.map(m =>
				this.on_end(err, m),
			));
			cleanUpMachines();
			throw err;
		}
	}
	
	//machine run events
	
	private async on_start(machine: IVirtualMachine) {
		//init execution bags
		let simulation_uuid = getShortUUID();
		
		let summery: SimulatorReportSummery = {
			simulation_uuid       : simulation_uuid,
			simulation_batch_uuid : this.simulation_batch_uuid,
			cycleIdStart          : -1,
			cycleIdEnd            : -1,
			machineStateStart     : machine.state,
			machineStateEnd       : null,
			accountsStart         : _.cloneDeep(machine.accounts),
			accountsEnd           : null,
			total_tx_batches_count: 0,
			total_tx_count        : 0,
			total_tx_amount_C     : 0,
			total_tx_profit_$     : 0,
			average_tx_depth      : 0,
		};
		
		let info: SimulatorReportInfo = {
			simulation_uuid      : simulation_uuid,
			simulation_batch_uuid: this.simulation_batch_uuid,
			note                 : this.note,
			exchanges            : machine.accounts.map(acc => acc.exchange),
			executionDate        : Date.now(),
			machineType          : machine.machineType,
			machineParams        : machine.params,
			accounts             : machine.accounts,
		};
		
		this.executionBags.set(machine, {
			isActive: true,
			simulation_uuid,
			info,
			summery,
		});
		
		// listeners
		await Promise.all(this.loggers.map(logger =>
			Promise
				.resolve(logger.logSimulationStart(simulation_uuid, info))
				.catch((err) => console.log('error in logger (on_start)', err)),
		));
	}
	
	private async on_end(error: any, machine: IVirtualMachine) {
		let
			bag                              = this.executionBags.get(machine),
			{summery, info, simulation_uuid} = bag;
		
		//update
		bag.isActive = false;
		
		summery.average_tx_depth = fix2(summery.average_tx_depth / summery.total_tx_batches_count);
		summery.accountsEnd      = _.cloneDeep(machine.accounts);
		summery.machineStateEnd  = _.cloneDeep(machine.state);
		
		//await runTestSimulator(simulation_uuid);
		
		// listeners
		await Promise.all(this.loggers.map(logger =>
			Promise
				.resolve(logger.logSimulationEnd(error, simulation_uuid, summery, info))
				.catch((err) => console.log('error in logger (on_end)', err)),
		));
	}
	
	private async on_transaction(
		machine: IVirtualMachine,
		cycle: MultipleBooksCycle,
		transaction: VirtualTransaction,
		transaction_batch: VirtualTransaction[],
		batch_index: number,
		accountsBefore: VirtualAccount[],
		accountsAfter: VirtualAccount[],
	) {
		let {summery, info, simulation_uuid} = this.executionBags.get(machine);
		
		//update
		summery.total_tx_count++;
		summery.total_tx_amount_C += transaction.amount_C;
		summery.total_tx_profit_$ += transaction.tx_profit_$;
		
		let action: SimulatorReportAction = {
			simulation_uuid       : simulation_uuid,
			simulation_batch_uuid : this.simulation_batch_uuid,
			cycleId               : cycle.cycleId,
			cycleFull             : cycle,
			cycleSummery          : summarizeMultipleBooksCycle(cycle),
			transaction           : transaction,
			transactionBatch_count: transaction_batch.length,
			transactionBatch_index: batch_index,
			accountsBefore        : accountsAfter,
			accountsAfter         : accountsAfter,
		};
		
		// listeners
		await Promise.all(this.loggers.map(logger =>
			Promise
				.resolve(logger.logSimulationAction(simulation_uuid, action, info))
				.catch((err) => console.log('error in logger (on_transaction)', err)),
		));
	}
	
	private async on_transactionBatch(
		machine: IVirtualMachine,
		cycle: MultipleBooksCycle,
		transactions: VirtualTransaction[],
		accountsBefore: VirtualAccount[],
		accountsAfter: VirtualAccount[],
	) {
		let {summery, info, simulation_uuid} = this.executionBags.get(machine);
		
		//update summery
		summery.total_tx_batches_count++;
		summery.average_tx_depth += transactions.length; //in the end divide by "total_tx_batches_count"
	}
	
	private async on_stateChanged(
		machine: IVirtualMachine,
		stateBefore: any,
		stateAfter: any,
	) {
		//nothing
	}
}
