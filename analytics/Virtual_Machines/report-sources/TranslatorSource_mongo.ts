import {Mongo} from "../../../src/DB/Mongo";
import {ReportSourceFactory, SimulatorReport, SimulatorReportAction} from "../core";
import {Cursor} from 'mongodb';

export function ReportSourceFactory_mongo(options: { simulation_uuid?: string, simulation_batch_uuid?: string }) {
	return ReportSourceFactory(async (consumer) => {
		// -- init :
		let
			reports: Cursor<SimulatorReport>,
			summeries: Cursor<SimulatorReport>,
		    actions: Cursor<SimulatorReportAction>;
		
		
		if (options.simulation_uuid)
		{
			reports = await Mongo.client
			                     .db('simulations')
			                     .collection('reports', {readPreference: 'secondaryPreferred'}, () => { })
			                     .find<SimulatorReport>({simulation_uuid: options.simulation_uuid}, {projection: {_id: 0}});
			
			summeries= await Mongo.client
			                     .db('simulations')
			                     .collection('reports', {readPreference: 'secondaryPreferred'}, () => { })
			                     .find<SimulatorReport>({simulation_uuid: options.simulation_uuid}, {projection: {_id: 0}});
			
			actions = await Mongo.client
			                     .db('simulations')
			                     .collection('actions', {readPreference: 'secondaryPreferred'}, () => { })
			                     .find<SimulatorReportAction>({simulation_uuid: options.simulation_uuid}, {projection: {_id: 0}});
			
			
		}
		else if (options.simulation_batch_uuid)
		{
			reports = await Mongo.client
			                     .db('simulations')
			                     .collection('reports', {readPreference: 'secondaryPreferred'}, () => { })
			                     .find<SimulatorReport>({simulation_batch_uuid: options.simulation_batch_uuid}, {projection: {_id: 0}});
			
			summeries= await Mongo.client
			                      .db('simulations')
			                      .collection('reports', {readPreference: 'secondaryPreferred'}, () => { })
			                      .find<SimulatorReport>({simulation_batch_uuid: options.simulation_batch_uuid}, {projection: {_id: 0}});
			
			actions = await Mongo.client
			                     .db('simulations')
			                     .collection('actions', {readPreference: 'secondaryPreferred'}, () => { })
			                     .find<SimulatorReportAction>({simulation_batch_uuid: options.simulation_batch_uuid}, {projection: {_id: 0}});
		}
		
		if (!reports || (await reports.count())===0)
		{
			throw ('simulation(s) not found');
		}
		
		//-- next values callbacks:
		consumer.get_reportInfo = async () => {
			if (await reports.hasNext())
			{
				return (await reports.next()).info;
			}
		};
		
		consumer.get_reportAction = async () => {
			if (await actions.hasNext())
			{
				return await actions.next();
			}
		};
		
		consumer.get_reportSummery = async () => {
			if (await summeries.hasNext())
			{
				return (await summeries.next()).summery;
			}
		};
		
		// --teardown:
		return async () => {
			if (actions)
			{
				await actions.close();
			}
		};
		
	});
}
