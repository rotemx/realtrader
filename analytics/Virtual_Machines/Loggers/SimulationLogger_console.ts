import { ISimulationLogger, SimulatorReportInfo, SimulatorReportSummery, SimulatorReportAction, SimulatorReport } from "../core";

export class SimulationLogger_console implements ISimulationLogger
{
	async logBatchEnd(isBatch: boolean, simulation_batch_uuid: string){
	
	}
	
	async logBatchStart(isBatch: boolean, simulation_batch_uuid: string){
	
	}
	
    logSimulationStart(simulation_uuid: string, info: SimulatorReportInfo)
    {
        let { machineType, machineParams, } = info;

        console.log(
            `--- running machine ${machineType}\n` +
            `--- params:\n${nicefier(machineParams)}\n`
        );
    }

    logSimulationEnd(error: any, simulation_uuid: string, summery: SimulatorReportSummery, info: SimulatorReportInfo)
    {
        if (error)
        {
            console.log("logger errror: ", error);
            return;
        }
        let { accountsStart, accountsEnd } = summery;

        console.log(
            `----initial accounts ---\n` +
            `${nicefier(accountsStart)}\n` +
            `----final accounts ---\n` +
            `${nicefier(accountsEnd)}\n` +
            `------------------------------------`
        );
        console.log(
            `---- report info   ---\n` +
            `${nicefier(info)}\n` +
            `---- report summery   ---\n` +
            `${nicefier(summery)}\n` +
            `------------------------------------`
        );

    }

    logSimulationAction(simulation_uuid: string, action: SimulatorReportAction, info: SimulatorReportInfo)
    {
        let {
            transaction,
            transactionBatch_count,
            transactionBatch_index,
            cycleId
        } = action;

        if (transactionBatch_count > 1 && transactionBatch_index == 0)
        {
            console.log(
                `===================================\n` +
                `multiple transaction BATCH start! ${transactionBatch_count}\n`
            );
        }

        console.log(`***************** transaction! [${transactionBatch_index + 1} / ${transactionBatch_count}]`);
        console.log(`** cycle ${cycleId}`);
        let { buy_order, sell_order, tx_profit_$, profit_per_C, amount_C } = transaction;
        console.log(
            `**  buy in ${buy_order.exchange} : ${buy_order.amount.toFixed(8)} C @ ${buy_order.price.toFixed(8)}\n` +
            `**  sell in ${sell_order.exchange} : ${sell_order.amount.toFixed(8)} C @ ${sell_order.price.toFixed(8)}\n` +
            `**  profit ${tx_profit_$.toFixed(8)}$ [${profit_per_C.toFixed(8)}$/C]\n`
        );

        if (transactionBatch_count > 1 && transactionBatch_index + 1 == transactionBatch_count)
        {
            console.log(
                `multiple transaction BATCH end! ${transactionBatch_count}\n` +
                `===================================\n`
            );
        }
    }
}

function nicefier(obj: any, spaces = 2)
{
    return JSON.stringify(
        obj,
        ((key, value) =>
            (typeof (value) === 'number')
                ? ((Math.abs(value) < 2) ? value.toFixed(8) : value.toFixed(2))
                : value),
        spaces
    );
}
