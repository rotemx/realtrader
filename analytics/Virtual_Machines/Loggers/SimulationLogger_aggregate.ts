import {ISimulationLogger, SimulatorReportInfo, SimulatorReportSummery, SimulatorReportAction} from "../core";
import * as fs from 'fs-extra';
import * as path from 'path';
import {fix8} from '../../../src/utils/fix';
import * as _ from 'lodash';
import moment = require('moment');

export class SimulationLogger_aggregate implements ISimulationLogger
{
	run_bags = new Map<string, {
		firstAction: boolean,
		actionsFilename: string,
		reportFilename: string,
		info: SimulatorReportInfo,
		summery: SimulatorReportSummery,
	}>();
	
	constructor() {
	
	}
	
	async logBatchStart(isBatch: boolean, simulation_batch_uuid: string) {
		
	}
	
	async logBatchEnd(isBatch: boolean, simulation_batch_uuid: string) {
		
		if (isBatch)
		{
			let file_path = path.join(process.cwd(), 'analytics', 'data', moment(Date.now()).format('DD-MM-YY-HH-mm-ss') + '_' + simulation_batch_uuid + '.json');
			let json      = {};
			if (fs.existsSync(file_path))
			{
				json = fs.readJSONSync(file_path);
			}
			
			for (const [key, value] of this.run_bags.entries())
			{
				json[key] = {
					info   : value.info,
					summery: value.summery,
				};
			}
			
			console.log(`saving simulations jons : ${file_path}`);
			fs.ensureFileSync(file_path);
			fs.writeJSONSync(file_path, json, {spaces: 2});
		}
	}
	
	async logSimulationStart(simulation_uuid: string, info: SimulatorReportInfo) {
		
		let
			time         = moment(Date.now()).format('DD-MM-YY-HH-mm'),
			market_names = (_.uniq(info.accounts.map(acc => acc.market))).join("_"),
			ex_names     = (_.uniq(info.exchanges)).join("_"),
			ab           = info.machineParams.AB_profit_threshold.toFixed(2),
			ba           = info.machineParams.BA_profit_threshold.toFixed(2),
			folder_name  = `${time}_${market_names}_${ex_names}`,
			file_name    = `${simulation_uuid}_${ab}_${ba}`,
			file_path    = path.join(process.cwd(), 'analytics', 'data', folder_name, file_name + '.csv');
		
		console.log(`saving report to : ${file_path}`);
		
		let
			actionsFilename = file_path,
			reportFilename  = ''
		;
		
		this.run_bags.set(info.simulation_uuid, {
			firstAction    : true,
			actionsFilename: actionsFilename,
			reportFilename : reportFilename,
			info           : info,
			summery        : null,
		});
		
		let row = [
			"cycleId",
			
			"binance_buy_price",
			"binance_sell_price",
			
			"bittrex_buy_price",
			"bittrex_sell_price",
			
			"buy in",
			"buy_order.price",
			"sell in",
			"sell_order.price",
			"amount_C",
			"profit_per_C",
			"tx_profit_$",
			info.exchanges[0] + " balance_$",
			info.exchanges[0] + " balance_C",
			info.exchanges[1] + " balance_$",
			info.exchanges[1] + " balance_C",
			
			info.exchanges[0] + "_holdings_$",
			info.exchanges[1] + "_holdings_$",
			
			info.exchanges[0] + "_holdings_C",
			info.exchanges[1] + "_holdings_C",
//			"monkey",
//			"double_monkey",
		].join(',');
		
		this.run_bags.get(info.simulation_uuid).firstAction = false;
		
		fs.ensureFileSync(actionsFilename);
		fs.writeFileSync(actionsFilename, row + '\n', 'utf8');
	}
	
	logSimulationAction(simulation_uuid: string, action: SimulatorReportAction, info: SimulatorReportInfo) {
		
		action = {...action};
		//		delete action.cycleFull;
		
		let
			{firstAction, actionsFilename, reportFilename, summery} = this.run_bags.get(info.simulation_uuid),
			INIT_BTC_PRICE                                          = 9260;
		
		let row = [
			action.cycleId,
			
			action.cycleFull.books[0].buy[0].price,
			action.cycleFull.books[0].sell[0].price,
			
			action.cycleFull.books[1].buy[0].price,
			action.cycleFull.books[1].sell[0].price,
			
			action.transaction.buy_order && action.transaction.buy_order.exchange,
			action.transaction.buy_order && action.transaction.buy_order.price,
			
			action.transaction.sell_order && action.transaction.sell_order.exchange,
			action.transaction.sell_order && action.transaction.sell_order.price,
			
			action.transaction.amount_C,
			action.transaction.profit_per_C,
			action.transaction.tx_profit_$,
			
			action.accountsAfter[0].balance_$,
			action.accountsAfter[0].balance_C,
			action.accountsAfter[1].balance_$,
			action.accountsAfter[1].balance_C,
			
			fix8(action.accountsAfter[0].balance_$ + action.accountsAfter[0].balance_C * action.cycleFull.books[0].sell[0].price),
			fix8(action.accountsAfter[1].balance_$ + action.accountsAfter[1].balance_C * action.cycleFull.books[1].sell[0].price),
			
			fix8(action.accountsAfter[0].balance_C + action.accountsAfter[0].balance_$ / action.cycleFull.books[0].buy[0].price),
			fix8(action.accountsAfter[1].balance_C + action.accountsAfter[1].balance_$ / action.cycleFull.books[1].buy[0].price),
			
//			fix8((INIT_BTC_PRICE + action.cycleFull.books[0].sell[0].price) * summery.accountsStart[1].balance_C),
//			fix8((2 * action.cycleFull.books[0].sell[0].price) * summery.accountsStart[1].balance_C),
		].join(',');
		
		this.run_bags.get(info.simulation_uuid).firstAction = false;
		
		fs.appendFileSync(actionsFilename, row + '\n', 'utf8');
		
	}
	
	logSimulationEnd(error: any, simulation_uuid: string, summery: SimulatorReportSummery, info: SimulatorReportInfo) {
		let bag                               = this.run_bags.get(info.simulation_uuid),
		    {actionsFilename, reportFilename} = bag;
		
		if (error)
		{
			console.error(error);
			return Promise.all([
				fs.unlink(actionsFilename),
				//				fs.unlink(reportFilename),
			]);
		}
		
		bag.summery = summery;
	}
	
}
