import * as fs from 'fs-extra';
import * as path from 'path';
import { Mongo } from "../../../src/DB/Mongo";
import { Db, Collection } from "mongodb";
import { ISimulationLogger, SimulatorReportInfo, SimulatorReportSummery, SimulatorReportAction, SimulatorReport } from "../core";

interface SimulationLogger_JSON_options
{
    mode?: 'pretty' | 'newLineDelimited' | 'ugly',
    newlineDelimited?: boolean,
}

export class SimulationLogger_JSON implements ISimulationLogger
{
    options: SimulationLogger_JSON_options;
    files = new Map<string, {
        firstAction: boolean,
        actionsFilename: string,
        reportFilename: string,
        actionsFileStream: fs.WriteStream,
    }>();

    constructor(private baseDir: string, options?: SimulationLogger_JSON_options)
    {
        this.options = {
            mode: 'pretty',
            ...options
        };
    }
	
	async logBatchEnd(isBatch: boolean, simulation_batch_uuid: string){
	
	}
	
	async logBatchStart(isBatch: boolean, simulation_batch_uuid: string){
	
	}
	
    logSimulationStart(simulation_uuid: string, info: SimulatorReportInfo)
    {
        let filebase = path.join(this.baseDir, info.simulation_uuid);
        let actionsFileStream = fs.createWriteStream(filebase + '.actions.json');
        this.files.set(info.simulation_uuid, {
            firstAction: true,
            actionsFilename: filebase + '.actions.json',
            reportFilename: filebase + '.report.json',
            actionsFileStream: actionsFileStream,
        });

        switch (this.options.mode)
        {
            case 'pretty':
                actionsFileStream.write('[\n')
                break;
            case 'ugly':
                actionsFileStream.write('[')
                break;
            case 'newLineDelimited':
                break;
        }
    }

    logSimulationEnd(error: any, simulation_uuid: string, summery: SimulatorReportSummery, info: SimulatorReportInfo)
    {
        let { actionsFilename, actionsFileStream, reportFilename } = this.files.get(info.simulation_uuid);

        if (error)
        {
            actionsFileStream.close();
            return Promise.all([
                fs.unlink(actionsFilename),
                fs.unlink(reportFilename),
            ]);
        }

        switch (this.options.mode)
        {
            case 'pretty':
                actionsFileStream.write('\n]')
                break;
            case 'ugly':
                actionsFileStream.write(']')
                break;
            case 'newLineDelimited':
                actionsFileStream.write('\n')
                break;
        }
        actionsFileStream.close();

        let report: SimulatorReport = {
            simulation_uuid: info.simulation_uuid,
	        simulation_batch_uuid : info.simulation_batch_uuid,
            info: info,
            summery: summery,
        }

        return fs.writeJson(reportFilename, report, { encoding: 'utf8', spaces: 2 });
    }

    logSimulationAction(simulation_uuid: string, action: SimulatorReportAction, info: SimulatorReportInfo)
    {
        action = { ...action };
        delete action.cycleFull;

        let { firstAction, actionsFilename, actionsFileStream, reportFilename } = this.files.get(info.simulation_uuid);

        let chunk = '';
        switch (this.options.mode)
        {
            case 'pretty':
                chunk += firstAction ? '' : ',\n';
                chunk += '  ' + JSON.stringify(action, null, 2);
                break;
            case 'ugly':
                chunk += firstAction ? '' : ',';
                chunk += JSON.stringify(action);
                break;
            case 'newLineDelimited':
                chunk += JSON.stringify(action);
                chunk += '\n';
                break;
        }
        this.files.get(info.simulation_uuid).firstAction = false;

        actionsFileStream.write(chunk, 'utf8');
    }
}
