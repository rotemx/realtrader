import {Db, Collection, MongoClient} from "mongodb";
import {ISimulationLogger, SimulatorReportInfo, SimulatorReportSummery, SimulatorReportAction, SimulatorReport} from "../core";

export class SimulationLogger_mongo implements ISimulationLogger
{
	private db: Db;
	private collection_actions: Collection<SimulatorReportAction>;
	private collection_reports: Collection<SimulatorReport>;
	
	constructor(private client: MongoClient) {
		this.db                 = this.client.db('simulations');
		this.collection_reports = this.db.collection<SimulatorReport>("reports", {readPreference: 'secondaryPreferred'}, () => { });
		
		this.collection_actions = this.db.collection<SimulatorReportAction>("actions", {readPreference: 'secondaryPreferred'}, () => { });
	}
	
	async logBatchEnd(isBatch: boolean, simulation_batch_uuid: string) {
	
	}
	
	async logBatchStart(isBatch: boolean, simulation_batch_uuid: string) {
	
	}
	
	logSimulationStart(simulation_uuid: string, info: SimulatorReportInfo) {
		let report: SimulatorReport = {
			simulation_uuid      : info.simulation_uuid,
			simulation_batch_uuid: info.simulation_batch_uuid,
			info                 : info,
			summery              : null,
		};
		return this.collection_reports.insertOne(report);
	}
	
	logSimulationEnd(error: any, simulation_uuid: string, summery: SimulatorReportSummery, info: SimulatorReportInfo) {
		if (error)
		{
			console.log('mongo logger ended with error...', error);
			return Promise.all([
				this.collection_reports.deleteOne({simulation_uuid: info.simulation_uuid}),
				this.collection_actions.deleteMany({simulation_uuid: info.simulation_uuid}),
			]);
		}
		let report: SimulatorReport = {
			simulation_uuid      : info.simulation_uuid,
			simulation_batch_uuid: info.simulation_batch_uuid,
			info                 : info,
			summery              : summery,
		};
		return this.collection_reports.replaceOne({simulation_uuid: info.simulation_uuid}, report);
	}
	
	logSimulationAction(simulation_uuid: string, action: SimulatorReportAction, info: SimulatorReportInfo) {
		return this.collection_actions.insertOne(action);
		
	}
}
