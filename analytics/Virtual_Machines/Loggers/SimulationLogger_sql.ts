import { ISimulationLogger, SimulatorReportInfo, SimulatorReportSummery, SimulatorReportAction, SimulatorReport } from "../core";

export class SimulationLogger_sql implements ISimulationLogger
{
	async logBatchEnd(isBatch: boolean, simulation_batch_uuid: string){
	
	}
	
	async logBatchStart(isBatch: boolean, simulation_batch_uuid: string){
	
	}
	
    logSimulationStart(simulation_uuid: string, info: SimulatorReportInfo)
    {

    }

    logSimulationEnd(error: any, simulation_uuid: string, summery: SimulatorReportSummery, info: SimulatorReportInfo)
    {

    }

    logSimulationAction(simulation_uuid: string, action: SimulatorReportAction, info: SimulatorReportInfo)
    {

    }
}
