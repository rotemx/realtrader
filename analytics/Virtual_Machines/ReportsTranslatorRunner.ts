import {SimulatorReportInfo, SimulatorReportSummery, SimulatorReportAction} from "./core/types";
import {ISimulationLogger} from "./core/ISimulationLogger";
import * as _ from 'lodash';
import {getShortUUID} from "../../src/utils/getShortUUID";
import {Log} from "../../src/utils/Log";
import {ReportContainer, IterableStream} from "./core";

interface executionBag
{
	simulation_uuid_orig: string,
	simulation_uuid_target: string,
	info: SimulatorReportInfo,
	summery: SimulatorReportSummery,
	running: boolean;
}

export class ReportsTranslatorRunner
{
	private TAG = ReportsTranslatorRunner.name;
	
	private executionBags = new Map<string, executionBag>();
	
	private simulation_batch_uuid;
	
	constructor(
		private source: IterableStream<ReportContainer>,
		private targets: ISimulationLogger[],
		private keepSourceUUID: boolean,
	) { }
	
	async run() {
		try
		{
			await this.logBatchStart();
			await this.source.forEach(async reportContainer => {
				let {report, simulation_uuid, type} = reportContainer;
				switch (type)
				{
					case 'SimulatorReportInfo':
						await this.logInfo(simulation_uuid, report as SimulatorReportInfo);
						break;
					case 'SimulatorReportSummery':
						await this.logSummery(simulation_uuid, report as SimulatorReportSummery);
						break;
					case 'SimulatorReportAction':
						await this.logAction(simulation_uuid, report as SimulatorReportAction);
						break;
				}
			});
			
			await this.validateCompleteAll();
			await this.logBatchEnd();
			
		} catch (err)
		{
			await this.logErrorAll(err);
			throw err;
		}
	}
	
	private async logInfo(orig_uuid: string, report: SimulatorReportInfo) {
		
		if (this.simulation_batch_uuid && report.simulation_batch_uuid != this.simulation_batch_uuid)
		{
			Log('WTF - simulation batch id not same as all batch?! ', "ReportsTranslatorRunner::logInfo()");
		}
		if (report.simulation_batch_uuid)
		{
			this.simulation_batch_uuid = report.simulation_batch_uuid;
		}
		
		let bag = this.executionBags.get(orig_uuid);
		if (bag && bag.running)
		{
			Log(`WTF - ${this.TAG}::logInfo() - info report after simulator already started ?!`);
			throw 'got info report for an already started simulation...';
			return;
		}
		
		let target_uuid = this.keepSourceUUID ? orig_uuid : getShortUUID();
		let info        = clone_and_update_uuid(report, target_uuid);
		
		this.executionBags.set(orig_uuid, {
			info                  : info,
			summery               : null,
			simulation_uuid_orig  : orig_uuid,
			simulation_uuid_target: target_uuid,
			running               : true,
		});
		
		await Promise.all(this.targets.map(logger =>
			Promise
				.resolve(logger.logSimulationStart(target_uuid, info))
				.catch((err) => console.log('error in logger logInfo', err)),
		));
	}
	
	private async logBatchStart() {
		await Promise.all(this.targets.map(logger =>
			Promise
				.resolve(logger.logBatchStart(!!this.simulation_batch_uuid,this.simulation_batch_uuid))
				.catch((err) => console.log('error in logger logBatchStart', err)),
		));
	}
	
	private async logBatchEnd() {
		await Promise.all(this.targets.map(logger =>
			Promise
				.resolve(logger.logBatchEnd(!!this.simulation_batch_uuid,this.simulation_batch_uuid))
				.catch((err) => console.log('error in logger logBatchEnd', err)),
		));
	}
	
	private async logSummery(orig_uuid: string, report: SimulatorReportSummery) {
		let bag                                                           = this.executionBags.get(orig_uuid);
		let {info, simulation_uuid_orig, simulation_uuid_target, running} = bag;
		
		let summery = clone_and_update_uuid(report, simulation_uuid_target);
		
		await Promise.all(this.targets.map(logger =>
			Promise
				.resolve(logger.logSimulationEnd(null, simulation_uuid_target, summery, info))
				.catch((err) => console.log('error in logger logSummery', err)),
		));
		
		bag.summery = summery;
		bag.running = false;
	}
	
	private async logAction(orig_uuid: string, report: SimulatorReportAction) {
		let bag                                                           = this.executionBags.get(orig_uuid);
		let {info, simulation_uuid_orig, simulation_uuid_target, running} = bag;
		
		let summery = clone_and_update_uuid(report, simulation_uuid_target);
		
		await Promise.all(this.targets.map(logger =>
			Promise
				.resolve(logger.logSimulationAction(simulation_uuid_target, summery, info))
				.catch((err) => console.log('error in logger logAction', err)),
		));
	}
	
	private async logErrorAll(error: any) {
		for (const bag of this.executionBags.values())
		{
			let {info, simulation_uuid_orig, simulation_uuid_target, running} = bag;
			
			if (!running)
			{
				return;
			}
			
			await Promise.all(this.targets.map(logger =>
				Promise
					.resolve(logger.logSimulationEnd(error, simulation_uuid_target, null, info))
					.catch((err) => console.log('error in logger logErrorAll', err)),
			));
			
			bag.running = false;
		}
	}
	
	private async validateCompleteAll() {
		for (const bag of this.executionBags.values())
		{
			let {info, simulation_uuid_orig, simulation_uuid_target, running} = bag;
			
			if (!running)
			{
				return;
			}
			
			await Promise.all(this.targets.map(logger =>
				Promise
					.resolve(logger.logSimulationEnd('source finished without summery report!', simulation_uuid_target, null, info))
					.catch((err) => console.log('error in logger validateCompleteAll', err)),
			));
			
			bag.running = false;
		}
	}
}

//-------------------------------------------------------
//-- helpers

function clone_and_update_uuid<T>(report: T, target_uuid: string): T {
	return _.cloneDeepWith(report,
		(value, key, object, stack) => {
			if (key === 'simulation_uuid')
			{
				return target_uuid;
			}
		},
	);
}
