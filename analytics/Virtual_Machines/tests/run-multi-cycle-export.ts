import * as fs from 'fs-extra';
import * as path from 'path';
import { Mongo } from '../../../src/DB/Mongo';
import { Markets } from '../../../src/types/enums/Markets';
import { Exchanges } from '../../../src/types/enums/Exchanges';
import { CycleSource_mongo } from '../cycleSources/CycleSource_mongo';
import { fix8 } from '../../../src/utils/fix';

run()
    .then(() => console.log('done'))
    .catch(console.error);

    
async function run()
{
    try
    {
        await Mongo.init();
        await dataExportCSV(
            path.join('.', 'hitbtc_binance_BTC_XVG.csv'),
            Markets.BTC_XVG,
            [Exchanges.hitbtc, Exchanges.binance]);
    } catch (err)
    {
        console.error('error in stream', err.message || err);
    }

    await Mongo.client.close().catch(err => console.error(err.message || err));
}

export async function dataExportCSV(filename: string, market: Markets, exchanges: Exchanges[])
{
    let file = fs.createWriteStream(filename, { encoding: 'utf8' });

    //headers
    let row: string[] = [];
    row.push('cycleId');
    exchanges.forEach(ex =>
        row.push(
            ex + '_best_buy',
            ex + '_best_sell',
        ));
    row.push('AB');
    row.push('BA');
    file.write(row.join(',') + '\n');

    let source = CycleSource_mongo(
        Mongo.client,
        market,
        exchanges,
        {
            logProgress: true,
            progressStep: 0.01,
            onlyFullMatch:
                true,
        }
    );

    await source.forEach(data =>		
    {
        //write SCV row
        let row: string[] = [];
        row.push(data.cycleId + "");
        data.books.forEach(ex =>
            row.push(
                fix8(ex.buy[0].price) + "",
                fix8(ex.sell[0].price) + ""
            )
        );
        row.push("" + fix8(data.books[0].sell[0].price * (1 - data.books[0].fee) - data.books[1].buy[0].price * (1 + data.books[1].fee)));
        file.write(row.join(',') + '\n');

    }).then(() =>
    {
        file.close();
    }).catch(err =>
    {
        file.close();
        throw (err);
    });
}