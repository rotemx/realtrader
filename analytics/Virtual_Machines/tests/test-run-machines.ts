import {Mongo} from "../../../src/DB/Mongo";
import {Markets} from "../../../src/types/enums/Markets";
import {Exchanges} from "../../../src/types/enums/Exchanges";
import {CycleSource_mongo} from "../cycleSources/CycleSource_mongo";
import {SimulationsRunner} from "../SimulationsRunner";
import {VirtualMachineBase} from "../core";
import * as _ from 'lodash';
import {SimulationLogger_mongo} from '../Loggers/SimulationLogger_mongo';
import {getShortUUID} from '../../../src/utils/getShortUUID';
import {OneSided_VirtualMachine} from '../machineTypes/one-sided-virtual-machine';

(async () => {
	try
	{
		await Mongo.init();
		await run();
	}
	catch (err)
	{
		console.log('error!', err);
	}
	await Mongo.close();
	console.log('DONE');
})();

async function run() {
	
	const
		machines: VirtualMachineBase<any, any> [] = [],
		simulation_batch_uuid                     = getShortUUID(),
		note                                      = "TESTING DAY BY DAY",
		testThresholds_AB                         = [4],
		//				testThresholds_AB                         = [4],
		testThresholds_BA                         = [-3],
		
		//			testThresholds_BA                         = [-2];
		
		//		first_cycle                               = (await Mongo.client.db('bittrex').collection('USDT_BTC', {readPreference: 'secondaryPreferred'}, () => { }).find({}).sort({cycleId: +1}).limit(1).next()).cycleId,
		//		last_cycle                                = (await Mongo.client.db('bittrex').collection('USDT_BTC', {readPreference: 'secondaryPreferred'}, () => { }).find({}).sort({cycleId: -1}).limit(1).next()).cycleId,
		//		cycle_range                               = last_cycle - first_cycle;
		timeRanges                                = [
			{fromCycleId: Number(new Date('05-01-18')), toCycleId: Number(new Date('05-02-18'))},
			{fromCycleId: Number(new Date('05-02-18')), toCycleId: Number(new Date('05-03-18'))},
			{fromCycleId: Number(new Date('05-03-18')), toCycleId: Number(new Date('05-04-18'))},
			{fromCycleId: Number(new Date('05-04-18')), toCycleId: Number(new Date('05-05-18'))},
			{fromCycleId: Number(new Date('05-05-18')), toCycleId: Number(new Date('05-06-18'))},
			{fromCycleId: Number(new Date('05-06-18')), toCycleId: Number(new Date('05-07-18'))},
			{fromCycleId: Number(new Date('05-07-18')), toCycleId: Number(new Date('05-08-18'))},
			{fromCycleId: Number(new Date('05-08-18')), toCycleId: Number(new Date('05-09-18'))},
			{fromCycleId: Number(new Date('05-10-18')), toCycleId: Number(new Date('05-11-18'))},
			{fromCycleId: Number(new Date('05-11-18')), toCycleId: Number(new Date('05-12-18'))},
			{fromCycleId: Number(new Date('05-12-18')), toCycleId: Number(new Date('05-13-18'))},
			{fromCycleId: Number(new Date('05-13-18')), toCycleId: Number(new Date('05-14-18'))},
			{fromCycleId: Number(new Date('05-14-18')), toCycleId: Number(new Date('05-15-18'))},
			{fromCycleId: Number(new Date('05-15-18')), toCycleId: Number(new Date('05-16-18'))},
			{fromCycleId: Number(new Date('05-16-18')), toCycleId: Number(new Date('05-17-18'))},
		];
	
	const
		market         = Markets.USDT_BTC,
		init_balance_$ = 0,
		init_balance_C = 0.6,
		//base_machine   = new Linear_VirtualMachine(
		base_machine   = new OneSided_VirtualMachine(
			[
				{
					balance_$: init_balance_$,
					balance_C: init_balance_C,
					exchange : Exchanges.bittrex,
					fee      : 0.0025,
					market   : market,
				},
				{
					balance_$: init_balance_$,
					balance_C: init_balance_C,
					exchange : Exchanges.binance,
					fee      : 0.0005,
					market   : market,
				},
			],
			{
				min_order_C        : 0.01,
				AB_profit_threshold: 1,
				BA_profit_threshold: 1,
			},
		);
	
	let source;
	
//	for (let range of timeRanges)
	{
		source = CycleSource_mongo(
			Mongo.client,
			market,
			base_machine.accounts.map(x => x.exchange),
			{
				logProgress  : true,
				onlyFullMatch: true,
				progressStep : 0.01,
				fromCycleId  : Number(new Date('05-09-18')),
				toCycleId    : Number(new Date('05-20-18')),
				//						limit:100,
			},
		);
		
		testThresholds_AB.forEach(AB => {
			testThresholds_BA.forEach(BA => {
				let m                        = _.cloneDeep(base_machine);
				m.params.AB_profit_threshold = AB;
				m.params.BA_profit_threshold = BA;
				machines.push(m);
			});
		});
	}
	
	let loggers = [
		new SimulationLogger_mongo(await Mongo.getMasterClient()),
		//new SimulationLogger_JSON(__dirname, { mode: 'pretty' }),
		//new SimulationLogger_console(),
		//new SimulationLogger_csv(),
	];
	let runner  = new SimulationsRunner(source, machines, 'parallel', loggers, note, simulation_batch_uuid);
	await runner.run();
}
