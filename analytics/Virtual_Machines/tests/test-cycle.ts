import { Mongo } from "../../../src/DB/Mongo";
import { Markets } from "../../../src/types/enums/Markets";
import { Exchanges } from "../../../src/types/enums/Exchanges";
import { mongoZipper } from "../utils/mongoZipper";
import { CycleSource_mongo } from "../cycleSources/CycleSource_mongo";

runTest().then(() =>
{
    console.log('DONE');
});


async function runTest()
{
    try
    {
        await Mongo.init();

        let source = CycleSource_mongo(
            Mongo.client,
            Markets.USDT_BTC,
            [
                Exchanges.binance,
                Exchanges.hitbtc
            ],
            {
                logProgress: true,
                progressStep: 0.01,
            }
        );

        await source.forEach(item =>
        {
            //
        });
    } catch (error)
    {
        console.error(error)
    }

    await Mongo.close();
}