import {Mongo} from "../../../src/DB/Mongo";
import {Markets} from "../../../src/types/enums/Markets";
import {Exchanges} from "../../../src/types/enums/Exchanges";
import {SATOSHI} from "../../../src/GLOBALS";
import {Linear_VirtualMachine} from "../machineTypes/Linear_VirtualMachine";
import {SimulationLogger_mongo} from "../Loggers/SimulationLogger_mongo";
import {CycleSource_mongo} from "../cycleSources/CycleSource_mongo";
import {SimulationsRunner} from "../SimulationsRunner";
import {SimulationLogger_console} from '../Loggers/SimulationLogger_console';

(async function () {
	try
	{
		await Mongo.init();
		await run();
	} catch (err)
	{
		console.log('error!', err);
	}
	await Mongo.close();
	console.log('DONE');
})();

async function run() {
	let market = Markets.BTC_XVG;
	let source = CycleSource_mongo(
		Mongo.client,
		market,
		[Exchanges.hitbtc,Exchanges.binance],
		{
			logProgress  : true,
			onlyFullMatch: true,
			progressStep : 0.05,
			// fromCycleId:1524114330784.0,
			// toCycleId:  1524114340784.0,
			// limit:1000,
		},
	);
	let NO=0;
	let lastId = 0;
	await source.forEach(cycle=>{
		if (cycle.cycleId-lastId <0){
			NO++;
			console.log(cycle.cycleId + "              "    + (cycle.cycleId-lastId) );
		}
		
		lastId=cycle.cycleId;
	});
	
	console.log('nos',NO);
}
