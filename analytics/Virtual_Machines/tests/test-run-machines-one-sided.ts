// import {Mongo} from "../../../src/DB/Mongo";
// import {Markets} from "../../../src/types/enums/Markets";
// import {Exchanges} from "../../../src/types/enums/Exchanges";
// import {SATOSHI} from "../../../src/GLOBALS";
// import {Linear_VirtualMachine} from "../machineTypes/Linear_VirtualMachine";
// import {SimulationLogger_mongo} from "../Loggers/SimulationLogger_mongo";
// import {CycleSource_mongo} from "../cycleSources/CycleSource_mongo";
// import {SimulationsRunner} from "../SimulationsRunner";
// import {SimulationLogger_console} from '../Loggers/SimulationLogger_console';
// import {OneSided_VirtualMachine} from '../machineTypes/OneSided_VirtualMachine';
//
// (async function () {
//	try
//	{
//		await Mongo.init();
//		await run();
//	} catch (err)
//	{
//		console.log('error!', err);
//	}
//	await Mongo.close();
//	console.log('DONE');
//  })();
//
//  async function run() {
//	let market = Markets.BTC_XVG;
//	let m      = new OneSided_VirtualMachine(
//		[
//			{
//				balance_$: 1,
//				balance_C: 117000,
//				exchange : Exchanges.hitbtc,
//				fee      : 0.001,
//				market   : market,
//			},
//			{
//				balance_$: 1,
//				balance_C: 117000,
//				exchange : Exchanges.binance,
//				fee      : 0.0005,
//				market   : market,
//			},
//		],
//		{
//			min_order_C        : 0.01,
//			AB_profit_threshold: 5 * SATOSHI,
//			BA_profit_threshold: 1 * SATOSHI,
//		},
//	);
//	let source = CycleSource_mongo(
//		Mongo.client,
//		market,
//		m.accounts.map(x => x.exchange),
//		{
//			logProgress  : true,
//			onlyFullMatch: true,
//			progressStep : 0.01,
//			// fromCycleId:1524114330784.0,
//			// toCycleId:  1524114340784.0,
//			// limit:1000,
//		},
//	);
//
//	let ms = [m];
//
//
//	let loggers = [
//		//new SimulationLogger_JSON(__dirname, { mode: 'pretty' }),
//		new SimulationLogger_mongo(await Mongo.getMasterClient()),
//		//new SimulationLogger_console(),
//		//new SimulationLogger_csv(),
//	];
//	let runner  = new SimulationsRunner(source, ms, 'parallel', loggers, "one sided");
//	return runner.run();
//}
