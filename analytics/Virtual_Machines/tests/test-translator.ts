import {Mongo} from "../../../src/DB/Mongo";
import {ReportsTranslatorRunner} from "../ReportsTranslatorRunner";
import {ReportSourceFactory_mongo} from "../report-sources/TranslatorSource_mongo";
import {SimulationLogger_aggregate} from '../Loggers/SimulationLogger_aggregate';

(async function () {
	
	const simulation_uuid = "oRFuSkknn9PqV3BG31dQ9H";
	
	try
	{
		await Mongo.init();
		await runTestSimulator(simulation_uuid);
	} catch (err)
	{
		console.log('error!', err);
	}
	await Mongo.close();
	console.log('DONE');
})();

export async function runTestSimulator(simulation_uuid: string) {
	let source = ReportSourceFactory_mongo({simulation_uuid});
	
	let loggers = [
		new SimulationLogger_aggregate(),
		//new SimulationLogger_JSON(__dirname, { mode: 'pretty' }),
		//new SimulationLogger_mongo(await Mongo.getMasterClient()),
		//new SimulationLogger_console(),
		//new SimulationLogger_csv(),
	];
	
	let translator = new ReportsTranslatorRunner(source, loggers, true);
	return translator.run();
}


