import { MultipleBooksCycle } from "../../../src/types/interfaces/MultipleBooksCycle";
import { BookEntry, Book } from "../../../src/types/interfaces/Book";
import { Log } from "../../../src/utils/Log";
import { fix8 } from "../../../src/utils/fix";
import * as _ from 'lodash';
import { VirtualMachineBase, VirtualTransaction, VirtualAccount } from "../core";

export interface Linear_VirtualMachine_PARAMS
{
    BA_profit_threshold: number,
    AB_profit_threshold: number,
    min_order_C: number,
}

export class Linear_VirtualMachine extends VirtualMachineBase<Linear_VirtualMachine_PARAMS, void>
{
    get accountA() { return this.accounts[0]; }
    get accountB() { return this.accounts[1]; }

    protected _updateInternalState(cycle: MultipleBooksCycle): boolean
    {
        return false;
    }

    protected _getTransactions(cycle: MultipleBooksCycle): VirtualTransaction[]
    {
        let arr = [];

        let
            book_A = cycle.books[0],
            book_B = cycle.books[1],
            AB = this.checkCycle(book_B.buy, book_A.sell, this.accountB, this.accountA, this.params.AB_profit_threshold),
            BA = this.checkCycle(book_A.buy, book_B.sell, this.accountA, this.accountB, this.params.BA_profit_threshold);

        if (AB.length && BA.length)
        {
//            Log(`WTF IS THIS SHIT AB AND BA TOGETHER WTF IS THIS SHIT MAN`);
            return
        }

        return arr.concat(AB).concat(BA);
    }


    checkCycle(buyBook: BookEntry[], sellBook: BookEntry[], buyAccount: VirtualAccount, sellAccount: VirtualAccount, profit_threshold: number): VirtualTransaction[]
    {
        buyBook = _.cloneDeep(buyBook);
        sellBook = _.cloneDeep(sellBook);
        buyAccount = _.cloneDeep(buyAccount);
        sellAccount = _.cloneDeep(sellAccount);

        return this.checkCycle_recursiveStep(buyBook, sellBook, buyAccount, sellAccount, profit_threshold);
    }

    checkCycle_recursiveStep(buyBook: BookEntry[], sellBook: BookEntry[], buyAccount: VirtualAccount, sellAccount: VirtualAccount, profit_threshold: number): VirtualTransaction[]
    {
        if (buyBook.length == 0 || sellBook.length == 0)
        {
            return [];
        }

        let res: VirtualTransaction[] = [];

        let buyRow = buyBook[0];
        let sellRow = sellBook[0];

        const
            amount_C = fix8(Math.min(buyRow.amount, sellRow.amount, buyAccount.balance_$ / buyRow.price, sellAccount.balance_C)),
            income_$ = fix8(sellRow.price * (1 - sellAccount.fee) * amount_C),
            cost_$ = fix8(buyRow.price * (1 + buyAccount.fee) * amount_C),
            tx_profit_$ = fix8(income_$ - cost_$),
            profit_per_C = fix8(tx_profit_$ / amount_C)
            ;
        if (amount_C > this.params.min_order_C && profit_per_C >= profit_threshold)
        {
            res = [
                {
                    amount_C: amount_C,
                    tx_profit_$: tx_profit_$,
                    profit_per_C: profit_per_C,
                    buy_order: {
                        type: 'buy',
                        exchange: buyAccount.exchange,
                        price: buyRow.price,
                        amount: amount_C,
                    },
                    sell_order: {
                        type: 'sell',
                        exchange: sellAccount.exchange,
                        price: sellRow.price,
                        amount: amount_C,
                    },
                },
            ];

            //update state and call recursively:

            buyAccount.balance_$ -= cost_$;
            buyAccount.balance_C += amount_C;

            sellAccount.balance_$ += income_$;
            sellAccount.balance_C -= amount_C;

            buyRow.amount -= amount_C;
            sellRow.amount -= amount_C;

            if (buyRow.amount <= this.params.min_order_C)
            {
                buyBook.splice(0, 1);
            }
            if (sellRow.amount <= this.params.min_order_C)
            {
                sellBook.splice(0, 1);
            }

            res = res.concat(this.checkCycle_recursiveStep(
                buyBook,
                sellBook,
                buyAccount,
                sellAccount,
                profit_threshold,
            ));
        }

        return res;
    }
}
