import {MultipleBooksCycle} from "../../../src/types/interfaces/MultipleBooksCycle";
import {BookEntry} from "../../../src/types/interfaces/Book";
import {fix8} from "../../../src/utils/fix";
import * as _ from 'lodash';
import {VirtualMachineBase, VirtualTransaction, VirtualAccount} from "../core";
import {Log} from '../../../src/utils/Log';

export interface Linear_VirtualMachine_PARAMS
{
	BA_profit_threshold: number,
	AB_profit_threshold: number,
	min_order_C: number,
}

export class OneSided_VirtualMachine extends VirtualMachineBase<Linear_VirtualMachine_PARAMS, void>
{
	first = true;
	get accountA() { return this.accounts[0]; }
	
	get accountB() { return this.accounts[1]; }
	
	protected _updateInternalState(cycle: MultipleBooksCycle): boolean {
		return false;
	}
	
	protected _getTransactions(cycle: MultipleBooksCycle): VirtualTransaction[] {
		let arr = [];
		
		let
			book_A = cycle.books[0],
			book_B = cycle.books[1],
			AB     = this.checkCycle(book_B.buy, book_A.sell, this.accountB, this.accountA, this.params.AB_profit_threshold),
			BA     = this.checkCycle(book_A.buy, book_B.sell, this.accountA, this.accountB, this.params.BA_profit_threshold);
		
		if (AB.length && BA.length)
		{
			//            Log(`WTF IS THIS SHIT AB AND BA TOGETHER WTF IS THIS SHIT MAN`);
			return;
		}
		
		return arr.concat(AB).concat(BA);
	}
	
	checkCycle(buyBook: BookEntry[], sellBook: BookEntry[], buyAccount: VirtualAccount, sellAccount: VirtualAccount, profit_threshold: number): VirtualTransaction[] {
		buyBook     = _.cloneDeep(buyBook);
		sellBook    = _.cloneDeep(sellBook);
		buyAccount  = _.cloneDeep(buyAccount);
		sellAccount = _.cloneDeep(sellAccount);
		
		return this.checkCycle_recursiveStep(buyBook, sellBook, buyAccount, sellAccount, profit_threshold);
	}
	
	checkCycle_recursiveStep(buyBook: BookEntry[], sellBook: BookEntry[], buyAccount: VirtualAccount, sellAccount: VirtualAccount, price_delta_threshold: number): VirtualTransaction[] {
		
		if (buyBook.length == 0 || sellBook.length == 0)
		{
			return [];
		}
		
		
		let
			isLocalBuy                = buyAccount.exchange === this.accountA.exchange,
			res: VirtualTransaction[] = [],
			
			buyRow                    = buyBook[0],
			sellRow                   = sellBook[0],
			
			localRow                  = isLocalBuy ? buyRow : sellRow,
			localBook                 = isLocalBuy ? buyBook : sellBook,
			localAccount              = isLocalBuy ? buyAccount : sellAccount,
			
			remoteRow                 = isLocalBuy ? sellRow : buyRow,
			remoteBook                = isLocalBuy ? sellBook : buyBook,
			remoteAccount             = isLocalBuy ? sellAccount : buyAccount,
			
			maxAmount                 = isLocalBuy ? localAccount.balance_$ / localRow.price : localAccount.balance_C,
			amount_C                  = fix8(Math.min(localRow.amount, maxAmount)),
			
			income_$                  = fix8(sellRow.price * (1 - sellAccount.fee) * amount_C),
			cost_$                    = fix8(buyRow.price * (1 + buyAccount.fee) * amount_C),
			
			tx_profit_$               = fix8(income_$ - cost_$),
			price_delta               = fix8(tx_profit_$ / amount_C)
		;
		
		if (this.first)
		{
			Log(buyRow,'FIRST CYCLE BUY');
			Log(sellRow,'FIRST CYCLE SELL');
			console.log();
			this.first = false;
		}
		
		
		if (amount_C > this.params.min_order_C && price_delta >= price_delta_threshold)
		{
			res = [
				{
					amount_C    : amount_C,
					tx_profit_$ : 0,
					profit_per_C: price_delta,
					
					buy_order:
						isLocalBuy
							? {
								type    : 'buy',
								exchange: buyAccount.exchange,
								price   : buyRow.price,
								amount  : amount_C,
							}
							: null,
					
					sell_order:
						isLocalBuy
							? null
							: {
								type    : 'sell',
								exchange: sellAccount.exchange,
								price   : sellRow.price,
								amount  : amount_C,
							},
				},
			];
			
			//update state and call recursively:
			
			if (isLocalBuy)
			{
				localAccount.balance_$ -= cost_$;
				localAccount.balance_C += amount_C;
			}
			else //sell
			{
				localAccount.balance_$ += income_$;
				localAccount.balance_C -= amount_C;
			}
			
			//update local book:
			localRow.amount -= amount_C;
			
			if (localRow.amount <= 0)
			{
				localBook.splice(0, 1);
			}
			
			//update other book:
			let amountToTrunc = amount_C;
			
			while (amountToTrunc > 0 && remoteRow)
			{
				let C = Math.min(amountToTrunc, remoteRow.amount);
				amountToTrunc -= C;
				remoteRow.amount -= C;
				
				if (remoteRow.amount <= 0)
				{
					remoteBook.splice(0, 1);
					remoteRow = remoteBook[0];
				}
			}
			
			res = res.concat(this.checkCycle_recursiveStep(
				buyBook,
				sellBook,
				buyAccount,
				sellAccount,
				price_delta_threshold,
			));
		}
		
		return res;
	}
}
