import * as fse from 'fs-extra';
import * as moment from 'moment';

const csv = 'analytics/csv/log3.csv';
fse.ensureFileSync(csv);
fse.writeFileSync(csv, '');
fse.appendFileSync(csv, `time, price\n`);

let
	data = fse.readFileSync('analytics/data/hitbin-out-2.log', 'utf8'),
	
	arr  = data
		.split('\n')
		//		.slice(0, 1000)
		.filter(line => {
			return line.includes('BUY in');
		})
		.forEach((line, i, ar) => {
			let
				price_idx  = line.lastIndexOf('\t') + 1,
				time_idx   = line.indexOf('152'),
				price      = line.substring(price_idx, line.length),
				time       = +line.substring(time_idx, time_idx + 13),
				excel_time = moment(time).format('DD-MM-YY HH:mm:ss');
			
			const restult_line = `${time},${price},${excel_time}\n`;
			
			if (restult_line.indexOf('NaN') === -1)
			{
				fse.appendFileSync(csv, restult_line);
			}
			
			if (i % 1000 == 0)
			{
				console.log(`${i}/${ar.length}`);
			}
		});

console.log('done');

