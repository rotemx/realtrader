import {Mongo} from '../../src/DB/Mongo';
import {runTestSimulator} from '../Virtual_Machines/tests/test-translator';

Mongo.init().then(() => {
	
	(async () => {
		
		let docs = await (await Mongo
			.client)
			.db('simulations')
			.collection('reports', {readPreference: 'secondaryPreferred'}, ()=>{})
			.find({})
			.toArray();
		
		docs
			.forEach(async s => {
				await runTestSimulator(s.simulation_uuid);
			});
		console.log('done');
	})();
	
});
